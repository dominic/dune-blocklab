#include"config.h"

/** This is the main app that allows to access all of BlockLab's core
 *  capabilities through one executable. Of course, it takes horribly
 *  long to compile. For downstream projects, it makes much more sense
 *  to provide their own app, that follows the implementation patterns
 *  used in this app, but only includes the necessary grid provider,
 *  vector providers and blocks.
 */

#include<dune/blocklab/app.hh>
#include<dune/blocklab/events.hh>
#include<dune/blocklab/grids.hh>
#include<dune/blocklab/init.hh>
#include<dune/blocklab/vectors.hh>

#include<memory>
#include<tuple>


int main(int argc, char** argv)
{
  // This should be the first line of any application - it correctly initializes
  // dependencies like MPI and Python
  auto init = Dune::BlockLab::initBlockLab(argc, argv);

  // The grid provider for this application
  using GridProvider = Dune::BlockLab::StructuredSimplexGridProvider<Dune::UGGrid<2>>;
  auto grid = [](const auto& c)
    {
      return std::make_shared<GridProvider>(c);
    };

  // The vector provider(s) for this application
  auto vectors = std::make_tuple(
    std::make_tuple("Solution", "P1 Lagrange Element", [](auto gp)
    {
      return std::make_shared<Dune::BlockLab::PkFemVectorProvider<GridProvider, 1>>(gp);
    })
  );

  // This lambda described how the construction context object of the solver
  // is populated with blocks. In fact, this parameter of instantiate_combinatorics
  // can be omitted and it default to only registering the default blocks. However
  // it is shown here to exemplify how the set of available blocks is customized
  // (both to extend functionality by adding some and to reduce compile times by removing)
  auto reg = [](auto& ctx){
    // This registers all blocks provided by dune-blocklab
    Dune::BlockLab::registerBuiltinBlocks(ctx);

    // This registers all operators provided by dune-blocklab
    Dune::BlockLab::registerBuiltinOperators(ctx);

    // The following line would register a custom block
    // ctx.template registerBlock<MyBlock>("myblock");
  };

  // Instantiate a BlockLabApp. It holds the above speficied arguments and dispatches
  // to commands which are registered to the app.
  Dune::BlockLab::BlockLabApp app(init, grid, vectors, reg, std::tuple<>{});

  // This is the main run command
  app.addDefaultRunner();

  // Add an auto-generated help message when called with command 'help'
  app.addHelpMessage();

  // Add an auto-generated frontend specification when called with command 'frontend <filename>'
  app.addFrontendExporter();

  // Actually run the thing!
  app.run();

  return 0;
}
