
find_package(yaml-cpp 0.6 REQUIRED)
dune_register_package_flags(LIBRARIES yaml-cpp)

# Check for the muparser library
find_package(muparser REQUIRED)
dune_register_package_flags(LIBRARIES muparser::muparser)

find_package(PythonLibs REQUIRED)
dune_register_package_flags(LIBRARIES "${PYTHON_LIBRARIES}"
                            INCLUDE_DIRS "${PYTHON_INCLUDE_DIRS}"
                            COMPILE_DEFINITIONS "HAVE_PYTHON"
                            )

dune_python_find_package(PACKAGE pyaml
                         REQUIRED)

include(BlocklabAppSetup)
