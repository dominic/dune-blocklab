#
# Set up a blocklab app from a downstream module
#

add_custom_target(frontend ALL)
set(BLOCKLAB_FRONTEND_EMPTY TRUE)

dune_module_path(MODULE dune-blocklab
                 RESULT modpath
                 CMAKE_MODULES)

dune_module_path(MODULE dune-blocklab
                 RESULT buildpath
                 BUILD_DIR)

add_custom_target(frontend_run_script_init
                  COMMAND ${CMAKE_COMMAND} -DAPPEND=FALSE -DFILENAME="${CMAKE_BINARY_DIR}/frontend.sh" -P ${modpath}/WriteFrontendRun.cmake -- "\\#!/bin/bash")

add_custom_target(frontend_run_script_finalize
                  COMMAND ${CMAKE_COMMAND} -DAPPEND=TRUE -DFILENAME="${CMAKE_BINARY_DIR}/frontend.sh" -P ${modpath}/WriteFrontendRun.cmake -- " "
                  COMMAND ${CMAKE_COMMAND} -DAPPEND=TRUE -DFILENAME="${CMAKE_BINARY_DIR}/frontend.sh" -P ${modpath}/WriteFrontendRun.cmake -- "cd ${buildpath}/frontend"
                  COMMAND ${CMAKE_COMMAND} -DAPPEND=TRUE -DFILENAME="${CMAKE_BINARY_DIR}/frontend.sh" -P ${modpath}/WriteFrontendRun.cmake -- "./run.sh"
                  )

add_dependencies(frontend frontend_run_script_finalize)
add_dependencies(frontend_run_script_finalize frontend_run_script_init)

function(blocklab_add_app_to_frontend)
  set(OPTION)
  set(SINGLE TARGET ADDITIONAL_FRONTEND_FILE)
  set(MULTI)
  cmake_parse_arguments(FRONTEND "${OPTION}" "${SINGLE}" "${MULTI}" ${ARGN})

  if(NOT FRONTEND_TARGET)
    message(FATAL_ERROR "blocklab_add_app_to_frontend: TARGET parameter is required!")
  endif()

  set(newfile ${CMAKE_CURRENT_BINARY_DIR}/${FRONTEND_TARGET}_frontend.yml)
  set(DOLLAR_LITERAL "$")
  add_custom_target(${FRONTEND_TARGET}_frontendyml
                    COMMAND ${FRONTEND_TARGET} frontend ${newfile} ${FRONTEND_ADDITIONAL_FRONTEND_FILE}
                    COMMAND ${CMAKE_COMMAND} -DCMAKE_BINARY_DIR="${CMAKE_BINARY_DIR}" -DAPPEND=TRUE -DFILENAME="${CMAKE_BINARY_DIR}/frontend.sh" -P ${modpath}/WriteFrontendRun.cmake -- "export BLOCKLAB_FRONTEND_FILE=DOLLAR_LITERAL{BLOCKLAB_FRONTEND_FILE}DOLLAR_LITERAL{BLOCKLAB_FRONTEND_FILE:+:\\}${newfile}"
  )
  add_dependencies(${FRONTEND_TARGET}_frontendyml frontend_run_script_init)
  add_dependencies(frontend_run_script_finalize ${FRONTEND_TARGET}_frontendyml)
endfunction()

