# In order to have CMake write an executable script, we need to write
# to a temporary and then copy it, as file(WRITE) has not FILE_PERMISSIONS
# parameter.

get_filename_component(fname "${FILENAME}" NAME)
get_filename_component(dir "${FILENAME}" DIRECTORY)
file(MAKE_DIRECTORY "${dir}/tmp")
set(tmpfile "${dir}/tmp/${fname}")

# The last argument is what should be written to the file
math(EXPR last "${CMAKE_ARGC}-1")
set(COMMAND APPEND)
if(NOT APPEND)
  set(COMMAND WRITE)
endif()
set(content "${CMAKE_ARGV${last}}\n")
string(REPLACE DOLLAR_LITERAL "$" realcontent "${content}")
file(${COMMAND} "${tmpfile}" "${realcontent}")

# This removal is necessary to circumvent https://gitlab.kitware.com/cmake/cmake/-/issues/21363
file(REMOVE "${FILENAME}")
file(COPY "${tmpfile}"
     DESTINATION "${dir}"
     FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
     )
