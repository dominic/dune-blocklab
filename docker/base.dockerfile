ARG TOOLCHAIN="gcc-9-17"
FROM registry.dune-project.org/dominic/dune-blocklab/requirements:debian-11-${TOOLCHAIN}

RUN duneci-install-module --recursive https://gitlab.dune-project.org/dominic/dune-blocklab.git
