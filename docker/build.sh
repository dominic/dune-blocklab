#!/bin/bash

#
# Build the docker images from this repository and deploy them to Gitlab.
# This is only meant for internal development purposes until we build
# Docker images in Gitlab CI. Any arguments passed to this script are
# forwarded to any docker build commands. This can be e.g. used to pass
# --no-cache.
#

# We want the script to halt on errors
set -e

docker login registry.dune-project.org

for toolchain in gcc-9-17
do
  # Build and push the 'requirements' image
  image=registry.dune-project.org/dominic/dune-blocklab/requirements:debian-11-$toolchain
  docker build --pull "$@" --build-arg TOOLCHAIN=$toolchain -t $image - < requirements.dockerfile
  docker push $image

  # Build and push the 'base' image
  image=registry.dune-project.org/dominic/dune-blocklab/base:debian-11-$toolchain
  docker build "$@" --build-arg TOOLCHAIN=$toolchain -t $image - < base.dockerfile
  docker push $image
done

# Build and push the 'frontend' image
image=registry.dune-project.org/dominic/dune-blocklab/frontend
docker build "$@" -t $image - < frontend.dockerfile
docker push $image

docker logout registry.dune-project.org
