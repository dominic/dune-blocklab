FROM registry.dune-project.org/docker/ci/debian:11

# Switch to the ROOT user in order to install more software
USER root
WORKDIR /

RUN export DEBIAN_FRONTEND=noninteractive; \
  apt-get update && apt-get dist-upgrade --no-install-recommends --yes \
  && apt-get install --no-install-recommends --yes \
  gmsh \  
  libyaml-cpp-dev \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

RUN python3 -m pip install pyaml

# Switch back to the duneci user as used in the upstream image
USER duneci
WORKDIR /duneci

ARG TOOLCHAIN="gcc-9-17"
RUN ln -s /duneci/toolchains/${TOOLCHAIN} /duneci/toolchain

RUN echo 'CMAKE_FLAGS+=" -DDUNE_PYTHON_VIRTUALENV_SETUP=1 -DDUNE_PYTHON_VIRTUALENV_PATH=/duneci/modules/dune-python-venv"' >> /duneci/cmake-flags/enable_virtualenv

RUN duneci-install-module -b master https://gitlab.dune-project.org/core/dune-common.git
RUN duneci-install-module -b master https://gitlab.dune-project.org/core/dune-geometry.git
RUN duneci-install-module -b master https://gitlab.dune-project.org/staging/dune-uggrid.git
RUN duneci-install-module -b master https://gitlab.dune-project.org/core/dune-grid.git
RUN duneci-install-module -b master https://gitlab.dune-project.org/core/dune-istl.git
RUN duneci-install-module -b master https://gitlab.dune-project.org/core/dune-localfunctions.git
RUN duneci-install-module -b master https://gitlab.dune-project.org/quality/dune-testtools.git
RUN duneci-install-module -b master https://gitlab.dune-project.org/staging/dune-typetree.git
RUN duneci-install-module -b master https://gitlab.dune-project.org/staging/dune-functions.git
RUN duneci-install-module -b master https://gitlab.dune-project.org/pdelab/dune-pdelab.git
RUN duneci-install-module -b master https://gitlab.dune-project.org/extensions/dune-alugrid.git
RUN duneci-install-module -b master --recursive https://gitlab.dune-project.org/extensions/dune-codegen.git
