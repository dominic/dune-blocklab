FROM registry.dune-project.org/dominic/dune-blocklab/base:debian-11-gcc-9-17

# Interesting input to consider:
# https://pythonspeed.com/articles/gunicorn-in-docker/

# Install all prerequisites
USER root
RUN python3 -m pip install -r /duneci/modules/dune-blocklab/frontend/requirements.txt
USER duneci

# We need to expose gunicorns port
EXPOSE 5000

# Set the working directory
WORKDIR /duneci/modules/dune-blocklab/build-cmake

# This will create the frontend specification
RUN make frontend

# Start gunicorn
CMD ["/duneci/modules/dune-blocklab/build-cmake/frontend.sh"]
