#ifndef DUNE_BLOCKLAB_APP_APPCLASS_HH
#define DUNE_BLOCKLAB_APP_APPCLASS_HH

#include <dune/common/exceptions.hh>

#include<dune/blocklab/app/frontend.hh>
#include<dune/blocklab/app/help.hh>
#include<dune/blocklab/app/run.hh>
#include<dune/blocklab/init.hh>
#include<dune/blocklab/utilities/yaml.hh>

#include<functional>
#include<map>
#include<string>


namespace Dune::BlockLab {

  /** @brief The main class for a BlockLab application
   *
   * This class manages the entrypoint of the application. It parses
   * the command line arguments and dispatches to a registered command.
   * Several default commands are available.
   *
   * @tparam PTuple An @c std::tuple of parameters to use in the solver's parameter system (on top of the default ones)
   * @tparam GP A lambda that describe how the grid provider is built from a YAML::Node (should be deducable)
   * @tparam VP A @c std::tuple of <tt>std::tuple<std::string, std::string, lambda></tt>. Each tuple entry is a vector provider (should be deducable)
   * @tparam RegisterFunction A lambda that describes how blocks are registered to the @ref ConstructionContext
   *
   * @ingroup app
   */
  template<typename PTuple, typename GP, typename VP, typename RegisterFunction>
  class BlockLabApp
  {
    public:
    using ParameterTuple = PTuple;

    /** @brief Construct an instance of this app
     * 
     * @param state The initialization state as put out by @ref initBlockLab
     * @param gp The lambda that describes how the grid provider is built from YAML::Node
     * @param vp The tuple that describes the construction of the vector providers
     * @param registerFunction The lambda describes how a @ref ConstructionContext is populated with blocks
     */
    BlockLabApp(const InitState& state,
                const GP& gp,
                const VP& vp,
                const RegisterFunction& registerFunction,
                const PTuple& ptuple)
      : title("BlockLab Application")
      , init(state)
      , gp(gp)
      , vp(vp)
      , registerFunction(registerFunction)
    {
      // Check that a command has been specified
      if (state.argc < 2) {
        DUNE_THROW(Dune::IOError,
                   "BlockLabApp requires a command as second argument!");
      }

      // The command to execute is always the first positional argument
      command = state.argv[1];

      for(int i = 2; i<state.argc; ++i)
        args.push_back(state.argv[i]);
    }

    //! Export the number of finite element containers in this app
    static constexpr int num_vectors = std::tuple_size_v<VP>;

    /** @brief Register a custom command to this app
     * 
     * @tparam Command The type of the command lambda (can be deduced)
     * @param name the command name
     * @param command A lambda that given an instance to an app, executes the command
     * 
     * This registers a command, so that a command line call to
     * <tt>./app <command> <args></tt>
     * will call the registered lambda.
     */
    template<typename Command>
    void registerCommand(std::string name, Command&& command)
    {
      mapping[name] = std::forward<Command>(command);
    }

    /** @brief register the default @c run command
     * 
     * This is a small shortcut for using @ref runApplication.
     */
    void addDefaultRunner()
    {
      mapping["run"] = [](auto& app){ return runApplication(app); };
    }

    /** @brief register the default @c help command
     * 
     * This is a small shortcut for using @ref helpMessage
     */
    void addHelpMessage()
    {
      mapping["help"] = [](auto& app){ return helpMessage(app); };
    }

    /** @brief register the default @c frontend command
     * 
     * This is a small shortcut for using @ref frontendExport
     */
    void addFrontendExporter()
    {
      mapping["frontend"] = [](auto& app){ return frontendExport(app); };
    }

    void setTitle(const std::string& title_)
    {
      title = title_;
    }

    /** @brief Run the app!
     * 
     * This dispatches on the correct command. You need to call this in order
     * for your app to actually do something.
     */
    void run()
    {
      const auto it = mapping.find(command);
      if (it == mapping.end()) {
        DUNE_THROW(Dune::IOError, "Command not registered: '" + command + "'");
      }
      it->second(*this);
    }

    //! Get the init state
    const InitState& getInitState()
    {
      return init;
    }

    //! Get the grid provider
    auto getGridProvider()
    {
      return gp(getConfig()["solver"]["grid"]);
    }

    //! Get a tuple of vector provider
    template<typename Grid>
    auto getVectorProviders(const Grid& grid)
    {
      return std::apply(
        [&grid](auto... v)
        {
          return std::make_tuple(std::get<2>(v)(grid)...);
        },
        vp
      );
    }

    //! Get a tuple of vector names to use
    auto getVectorNames()
    {
      return std::apply(
        [](auto... v)
        {
          return std::array{std::get<0>(v)...};
        },
        vp
      );
    }

    //! Get a tuple of descriptive vector titles to use
    auto getVectorTitles()
    {
      return std::apply(
        [](auto... v)
        {
          return std::array{std::get<1>(v)...};
        },
        vp
      );
    }

    //! Get the function that registers blocks with the @ref ConstructionContext
    const RegisterFunction& getRegisterFunction()
    {
      return registerFunction;
    }

    //! Get the root config YAML::Node
    const YAML::Node& getConfig()
    {
      // Lazy parsing of the yaml input
      if (config.Type() == YAML::NodeType::Null) {
        // Check if argument is present
        const auto config_path = getArgument(0);
        if (config_path.empty()) {
          DUNE_THROW(
            Dune::IOError,
            "BlockLabApp requires path to configuration file as argument!");
        }

        // Parse the file
        config = YAML::LoadFile(config_path);
      }

      return config;
    }

    //! Get the app title
    const std::string& getTitle()
    {
      return title;
    }

    /** @brief Get a command line argument
     * 
     * @param i the index of the argument
     * 
     * @returns the argument or @c "" if it was not present
     * 
     * Note that the indexing here is shifted in comparison to @c argv :
     * The arguments following the command are accessible with 0-based indexing
     */
    std::string getArgument(std::size_t i)
    {
      if(i < args.size())
        return args[i];
      else
        return "";
    }

    private:
    // Some fields that can be set on the app class
    std::string title;

    // Preserve command line arguments
    std::vector<std::string> args;

    // The parameters we parsed from the input
    std::string command;
    YAML::Node config;

    // The state that we get from the outside
    const InitState& init;
    const GP& gp;
    const VP& vp;
    const RegisterFunction& registerFunction;

    // The command mapping
    std::map<std::string, std::function<void(BlockLabApp<ParameterTuple, GP, VP, RegisterFunction>&)>> mapping;
  };

} // namespace Dune::BlockLab

#endif
