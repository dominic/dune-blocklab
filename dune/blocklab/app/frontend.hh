#ifndef DUNE_BLOCKLAB_APP_FRONTEND_HH
#define DUNE_BLOCKLAB_APP_FRONTEND_HH

#include<dune/blocklab/utilities/locate.hh>
#include<dune/blocklab/utilities/python.hh>
#include<dune/common/hybridutilities.hh>

#include<map>
#include<string>
#include<tuple>
#include<vector>


namespace Dune::BlockLab {

  namespace impl {

    template<typename... VectorProviders>
    class DummyContext
    {
      // Construct a tuple of all vector types in the given providers
      using V = std::tuple<typename VectorProviders::Vector...>;

      // and a tuple of all parameter types
      using P = std::tuple<typename VectorProviders::Parameter...>;

      public:
      template<typename Block>
      void registerBlock(std::string identifier)
      {
        // TODO: If this is inherited from DisabledBlock, we should skip this part!
        auto bdata = Block::blockData();
        if(bdata.size())
          data[identifier] = Block::blockData();
      }

      template<template<typename, typename, std::size_t, typename...> typename Block>
      void registerBlock(std::string identifier)
      {
        Dune::Hybrid::forEach(Dune::Hybrid::integralRange(std::integral_constant<std::size_t, 0>{},
                                                          std::integral_constant<std::size_t, std::tuple_size<V>::value>{}),
          [this, identifier](auto i)
          {
            this->template registerBlock<Block<P, V, i>>(identifier);
          }
        );
      }

      template<template<typename, typename, typename...> typename Block>
      void registerBlock(std::string identifier)
      {
        this->template registerBlock<Block<P, V>>(identifier);
      }

      std::map<std::string, std::vector<std::string>> data;
    };

    void update_data(PythonContext& pyctx, std::vector<std::string> data, std::string key = "")
    {
      using namespace std::string_literals;
      pyctx.run("tmpdata = {}");
      for(auto line : data)
      {
        pyctx.run("line = '''"s + line + "'''"s);
        pyctx.run("tmpdata = merge_dicts(tmpdata, remove_underscore_keys(yaml.safe_load(line)))"s);
      }
      pyctx.run("data" + key + " = tmpdata");
    }

  } // namespace impl

  /** @brief Implement the frontend command
   * 
   * This implements a command that outputs a yaml file which can be used as input
   * for the BlockLab web application.
   * 
   * The frontend exporter reads the following arguments from the command line in this order:
   * * The filename to write the resulting YAML file to.
   * * A file whose content should be prepended. This is used to provide some parts of the
   *   frontend specification that cannot be extracted from the C++ block classes. dune-blocklab
   *   internally uses this for its Just-in-time webapp. If you are building a web application
   *   for fixed grids and finite elements, you do not need this.
   * 
   * @ingroup app
   */
  template<typename App>
  void frontendExport(App& app)
  {
    using namespace std::string_literals;

    PythonContext pyctx;

    pyctx.import("yaml");
    pyctx.import("sys");

    // We need to merge dictionaries
    pyctx.run(
      // A utility to deep-merge potentially nested dictionaries.
      // Adapted from https://stackoverflow.com/a/7205107/2819459
      "def merge_dicts(a, b):                                               \n"
      "    for key in b:                                                    \n"
      "        if key in a:                                                 \n"
      "            if isinstance(a[key], dict) and isinstance(b[key], dict):\n"
      "                merge_dicts(a[key], b[key])                          \n"
      "            elif a[key] == b[key]:                                   \n"
      "                pass # same leaf value                               \n"
      "            else:                                                    \n"
      "                raise Exception('Type conflict in merging dicts')    \n"
      "        else:                                                        \n"
      "            a[key] = b[key]                                          \n"
      "    return a                                                         \n"
    );

    pyctx.run(
      "def remove_underscore_keys(d):                                       \n"
      "    if isinstance(d, dict):                                          \n"          
      "        return {k: remove_underscore_keys(v) for k, v in d.items() if not k.startswith('_')} \n"
      "    else:                                                            \n"
      "        return d                                                     \n"
    );

    // This dictionary will hold all the data
    pyctx.run("data = {}");

    auto arg = app.getArgument(1);
    if(arg == "")
    {
      // Set the application title
      pyctx.run("data['title'] = '"s + app.getTitle() + "'"s);

      // Store the path to this executable so that the frontend can call it
      pyctx.run("data['executable'] = {}");
      pyctx.run("data['executable']['path'] = '"s + get_executable_path() + "'"s);
      pyctx.run("data['executable']['jitmode'] = 0");

      // Set the grid providers
      pyctx.run("data['grids'] = {}");
      auto griddata = decltype(app.getGridProvider())::element_type::blockData();
      impl::update_data(pyctx, griddata, "['grids']['grid']");

      // Set the vector providers
      pyctx.run("data['vectors'] = []");
      Dune::Hybrid::forEach(Dune::Hybrid::integralRange(std::integral_constant<std::size_t, 0>{},
                                                        std::integral_constant<std::size_t, App::num_vectors>{}),
        [&pyctx, &app](auto i)
        {
          auto name = std::get<i>(app.getVectorNames());
          auto apptitle = std::get<i>(app.getVectorTitles());

          pyctx.run("data['vectors'].append({'name': '"s + name + "', 'title': '"s + apptitle + "'})"s);
        }
      );
    }
    else
    {
      // If we passed an argument, we populate the data dictionary with its content.
      pyctx.run("data = yaml.safe_load(open('" + arg + "'))");
    }

    // Add blocks
    pyctx.run("data['blocks'] = {}");
    std::apply([&pyctx, &app](auto... v)
      {
        impl::DummyContext<typename decltype(v)::element_type...> ctx;
        app.getRegisterFunction()(ctx);

        for (auto [block, bdata]: ctx.data)
        {
          pyctx.run("bdata = {}");
          impl::update_data(pyctx, bdata, "['blocks']['" + block + "']");
        }
      },
      // We only need the types of vector providers in above lambda
      // and actually constructing grid providers requires YAML input
      // from the command line - so we use this decltype beauty
      decltype(app.getVectorProviders(app.getGridProvider())){}
    );

    // Dump the data dictionary onto std::out.
    pyctx.run("yaml.dump(data, open('" + app.getArgument(0) + "', 'w'))");
  }

} // namespace Dune::BlockLab

#endif