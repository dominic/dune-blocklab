#ifndef DUNE_BLOCKLAB_APP_RUN_HH
#define DUNE_BLOCKLAB_APP_RUN_HH

#include<dune/blocklab/construction/context.hh>
#include<dune/blocklab/events/init.hh>


namespace Dune::BlockLab {

  /** @brief Implement the run command
   * 
   * This implements a command that constructs and runs the solver.
   * 
   * @ingroup app
   */
  template<typename App>
  void runApplication(App& app)
  {
    // Make sure to register all the listeners that we want
    registerListenersFromConfig(app.getConfig()["solver"]["listeners"]);

    auto gp = app.getGridProvider();
    auto vp = app.getVectorProviders(gp);

    std::apply([&app](auto... v)
      {
        Dune::BlockLab::ConstructionContext<typename App::ParameterTuple, typename decltype(v)::element_type...> ctx(
          app.getInitState().helper,
          app.getConfig(),
          v...);
        
        app.getRegisterFunction()(ctx);

        auto solverConfig = app.getConfig()["solver"];
        if(!solverConfig["vectors"])
          solverConfig["vectors"] = app.getVectorNames();

        auto solver = ctx.constructSolver(solverConfig);
        solver->apply();
      },
      vp
    );
  }

} // namespace Dune::BlockLab

#endif