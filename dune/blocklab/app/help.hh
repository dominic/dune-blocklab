#ifndef DUNE_BLOCKLAB_APP_HELP_HH
#define DUNE_BLOCKLAB_APP_HELP_HH

#include<iostream>


namespace Dune::BlockLab {

  /** @brief Implement the help command
   * 
   * This implements a command that prints a meaningful help message to std::cout.
   * This is intended for apps which will be primarily run as command line tools.
   * The information will be taken from the block data.
   * 
   * @note This is currently not implemented.
   * @ingroup app
   */
  template<typename App>
  void helpMessage(App& app)
  {
    std::cout << "Help message not implemented" << std::endl;
  }

} // namespace Dune::BlockLab

#endif