#ifndef DUNE_BLOCKLAB_BLOCKS_VISUALIZATION_HH
#define DUNE_BLOCKLAB_BLOCKS_VISUALIZATION_HH

#include<dune/blocklab/blocks/blockbase.hh>
#include<dune/blocklab/events/fileio.hh>
#include<dune/grid/io/file/vtk.hh>
#include<dune/pdelab/gridfunctionspace/vtk.hh>

#include<filesystem>
#include<memory>
#include<variant>


namespace Dune::BlockLab {

  namespace impl {

    template<typename GV, bool instationary>
    struct VTKWriterChooser
    {
      using type = Dune::SubsamplingVTKWriter<GV>;
      using ptype = std::shared_ptr<type>;
    };


    template<typename GV>
    struct VTKWriterChooser<GV, true>
    {
      using type = Dune::VTKSequenceWriter<GV>;
      using ptype = std::shared_ptr<type>;
    };

  }

  /** @brief A block that manages visualization using VTK
   * 
   * This block will write VTK output during application. Datasets can be 
   * added to the visualization output by adding child blocks that do so.
   */
  template<typename P, typename V>
  class VisualizationBlock
    : public ParentBlockBase<P, V>
  {
    public:
    //! The traits class for this block
    using Traits = BlockTraits<P, V>;

    using VTKWriter = std::variant<typename impl::VTKWriterChooser<typename Traits::GridView, true>::ptype,
                                   typename impl::VTKWriterChooser<typename Traits::GridView, false>::ptype>;

    /** @copydoc ParentBlockBase::ParentBlockBase(Context&, const YAML::Node&) */
    template<typename Context>
    VisualizationBlock(Context& ctx, const YAML::Node& config)
      : ParentBlockBase<P, V>(ctx, config)
      , instationary(config["instationary"].as<bool>())
      , intervals(config["intervals"].as<int>())
      , time(0.0)
      , filename(config["filename"].as<std::string>())
      , path(config["path"].as<std::string>())
    {}

    //! We use the virtual default destructor
    virtual ~VisualizationBlock() = default;

    /** @copydoc ParentBlockBase::blockData() */
    static std::vector<std::string> blockData()
    {
      auto data = ParentBlockBase<P, V>::blockData();
      data.push_back(
        "title: VTK Visualization        \n"
        "category: visualization         \n"
        "schema:                         \n"
        "  filename:                     \n"
        "    type: string                \n"
        "    default: output             \n"
        "    meta:                       \n"
        "      subtype: outputfile       \n"
        "      title: Output File        \n"
        "  instationary:                 \n"
        "    type: boolean               \n"
        "    default: false              \n"
        "    meta:                       \n"
        "      title: Instationary problem?\n"
        "  intervals:                    \n"
        "    type: integer               \n"
        "    default: 1                  \n"
        "    meta:                       \n"
        "      title: Subsampling Intervals\n"
        "  path:                         \n"
        "    type: string                \n"
        "    default: vtk                \n"
        "    meta:                       \n"
        "      title: Subfolder Name     \n"
      );
      return data;
    }

    /** @copydoc ParentBlockBase::update_parameter(std::string, typename Traits::Parameter) */
    virtual void update_parameter(std::string name, typename Traits::Parameter param) override
    {
      if (name == "time")
        time = std::get<double>(param);

      for (auto block : this->blocks)
        block->update_parameter(name, param);
    }

    /** @copydoc ParentBlockBase::setup() */
    virtual void setup() override
    {
      // Make sure that the output directory exists
      std::filesystem::create_directory(std::filesystem::current_path().append(path));

      // Instantiate the VTK writer instance
      auto vector = this->solver->template getVector<0>();
      auto gv = vector->gridFunctionSpace().gridView();

      if (instationary)
      {
        auto inner = std::make_shared<typename impl::VTKWriterChooser<typename Traits::GridView, false>::type>(gv, Dune::RefinementIntervals(intervals));
        vtkwriter = std::make_shared<typename impl::VTKWriterChooser<typename Traits::GridView, true>::type>(inner, filename, path, "");
      }
      else
        vtkwriter = std::make_shared<typename impl::VTKWriterChooser<typename Traits::GridView, false>::type>(gv, Dune::RefinementIntervals(intervals));

      this->recursive_setup();
    }

    /** @copydoc ParentBlockBase::apply() */
    virtual void apply() override
    {
      this->recursive_apply();

      if (instationary)
        std::get<typename impl::VTKWriterChooser<typename Traits::GridView, true>::ptype>(vtkwriter)->write(time, Dune::VTK::appendedraw);
      else
        std::get<typename impl::VTKWriterChooser<typename Traits::GridView, false>::ptype>(vtkwriter)->write(filename, Dune::VTK::ascii);
      triggerFileIOEvent(*this, filename, "filename");
    }

    /** @brief Add a dataset to the VTK Output
     * 
     * Child blocks can call this method during their @c setup step by calling
     * <tt>std::dynamic_pointer_cast<VisualizationBlock<P, V>>(this->parent)->add_dataset</tt>
     * 
     * @tparam Container The PDELab container type - this should be deducable
     * @param container The name of the container to add a dataset for
     * @param datasetname The name how it should appear in the VTK output
     */
    template<typename Container>
    void add_dataset(std::shared_ptr<Container> container, std::string datasetname)
    {
      if (instationary)
        Dune::PDELab::addSolutionToVTKWriter(
            *(std::get<typename impl::VTKWriterChooser<typename Traits::GridView, true>::ptype>(vtkwriter)),
            container->gridFunctionSpaceStorage(),
            container,
            Dune::PDELab::vtk::DefaultFunctionNameGenerator(datasetname));
      else
        Dune::PDELab::addSolutionToVTKWriter(
            *(std::get<typename impl::VTKWriterChooser<typename Traits::GridView, false>::ptype>(vtkwriter)),
            container->gridFunctionSpaceStorage(),
            container,
            Dune::PDELab::vtk::DefaultFunctionNameGenerator(datasetname));
    };

    /** @brief Add a dataset with cellwise data to the VTK Output
     * 
     * Child blocks can call this method during their @c setup step by calling
     * <tt>std::dynamic_pointer_cast<VisualizationBlock<P, V>>(this->parent)->add_celldata</tt>
     * 
     * @tparam Function The @c Dune::VTKFunction that should be visualized - this should be deducable
     * @param function An @c std::shared_ptr to a @c Dune::VTKFunction instance that should be visualized
     *
     * The name of the dataset is specified by the @c Dune::VTKFunction .
     */
    template<typename Function>
    void add_celldata(std::shared_ptr<Function> function)
    {
      if (instationary)
        std::get<typename impl::VTKWriterChooser<typename Traits::GridView, true>::ptype>(vtkwriter)->addCellData(function);
      else
        std::get<typename impl::VTKWriterChooser<typename Traits::GridView, false>::ptype>(vtkwriter)->addCellData(function);
    }

    private:
    bool instationary;
    int intervals;
    double time;
    std::string filename;
    std::string path;
    VTKWriter vtkwriter;
  };

  /** @brief A block that visualizes a finite element function
   * 
   * This is the most important and most common child block for
   * @ref VisualizationBlock. It visualizes a finit element function
   * that is stored directly on the solver. 
   */ 
  template<typename P, typename V, std::size_t i>
  class VectorVisualizationBlock
    : public BlockBase<P, V, i>
  {
    public:
    //! The traits class for this block
    using Traits = BlockTraits<P, V, i>;

    /** @copydoc BlockBase::BlockBase(Context&, const YAML::Node&) */
    template<typename Context>
    VectorVisualizationBlock(Context& ctx, const YAML::Node& config)
      : BlockBase<P, V, i>(ctx, config)
      , name(ctx.getVectorName(i))
    {}

    //! We use the virtual default destructor
    virtual ~VectorVisualizationBlock() = default;

    /** @copydoc BlockBase::setup() */
    virtual void setup() override
    {
      auto vector = this->solver->template getVector<i>();
      std::dynamic_pointer_cast<VisualizationBlock<P, V>>(this->parent)->add_dataset(vector, name);
    }

    /** @copydoc BlockBase::blockData() */
    static std::vector<std::string> blockData()
    {
      auto data = BlockBase<P, V, i>::blockData();
      data.push_back(
        "title: Finite Element Function            \n"
        "category: visualization                   \n"
      );
      return data;
    }

    private:
    std::string name;
  };


  /** @brief A block that visualizes the MPI Rank in a parallel setting
   * 
   * This block adds a dataset to a @ref VisualizationBlock that allows to quickly
   * assess the grid partitioning by visualizing it.
   */
  template<typename P, typename V>
  class MPIRankVisualizationBlock
    : public BlockBase<P, V>
  {
    public:
    //! The traits class for this block
    using Traits = BlockTraits<P, V>;

    //! The @c Dune::VTKFunction that visualizes the MPI rank
    struct RankFunction
      : public Dune::VTKFunction<typename Traits::GridView>
    {
      RankFunction(const Dune::MPIHelper& helper)
        : rank(helper.rank())
      {}

      virtual ~RankFunction() = default;

      virtual int ncomps() const override
      {
        return 1;
      }

      virtual double evaluate (int comp, const typename Traits::Entity& e,
                               const typename Traits::LocalCoordinate& xi) const override
      {
        return static_cast<double>(rank);
      }

      virtual std::string name () const override
      {
        return "MPI Rank";
      }

      double rank;
    };

    /** @copydoc BlockBase::BlockBase(Context&, const YAML::Node&) */
    template<typename Context>
    MPIRankVisualizationBlock(Context& ctx, const YAML::Node& config)
      : BlockBase<P, V>(ctx, config)
      , helper(ctx.getMPIHelper())
    {}

    //! We use the virtual default destructor
    virtual ~MPIRankVisualizationBlock() = default;

    /** @copydoc BlockBase::setup() */
    virtual void setup() override
    {
      auto rankfunction = std::make_shared<RankFunction>(helper);
      std::dynamic_pointer_cast<VisualizationBlock<P, V>>(this->parent)->add_celldata(rankfunction);
    }

    /** @copydoc BlockBase::blockData() */
    static std::vector<std::string> blockData()
    {
      auto data = BlockBase<P, V>::blockData();
      data.push_back(
        "title: MPI Rank                       \n"
        "category: visualization               \n"
      );
      return data;
    }

    private:
    const Dune::MPIHelper& helper;
  };


  /** @brief A block that visualizes the grid's index set
   * 
   * This adds a dataset to a @ref VisualizationBlock that visualizes
   * the grid's index set.
   */
  template<typename P, typename V>
  class IndexSetVisualizationBlock
    : public BlockBase<P, V>
  {
    public:
    //! The trait class for this block
    using Traits = BlockTraits<P, V>;

    //! The @c Dune::VTKFunction that visualizes the index set
    struct IndexSetFunction
      : public Dune::VTKFunction<typename Traits::GridView>
    {
      using Base = Dune::VTKFunction<typename Traits::GridView>;
      using Entity = typename Base::Entity;

      IndexSetFunction(typename Traits::GridView gv)
        : is(gv.indexSet())
      {}

      virtual ~IndexSetFunction() = default;

      virtual int ncomps() const override
      {
        return 1;
      }

      virtual double evaluate (int comp, const typename Traits::Entity& e,
                               const typename Traits::LocalCoordinate& xi) const override
      {
        return static_cast<double>(is.index(e));
      }

      virtual std::string name () const override
      {
        return "Cell Indices";
      }

      const typename Traits::GridView::IndexSet& is;
    };

    /** @copydoc BlockBase::BlockBase(Context&, const YAML::Node&) */
    template<typename Context>
    IndexSetVisualizationBlock(Context& ctx, const YAML::Node& config)
	  : BlockBase<P, V>(ctx, config)
    {}

    //! We use the virtual default destructor
    virtual ~IndexSetVisualizationBlock() = default;

    /** @copydoc BlockBase::setup() */
    virtual void setup() override
    {
      auto indexsetfunc= std::make_shared<IndexSetFunction>(this->solver->template getVector<0>()->gridFunctionSpace().gridView());
      std::dynamic_pointer_cast<VisualizationBlock<P, V>>(this->parent)->add_celldata(indexsetfunc);
    }

    /** @copydoc BlockBase::blockData() */
    static std::vector<std::string> blockData()
    {
      auto data = BlockBase<P, V>::blockData();
      data.push_back(
        "title: Cell Index Set                \n"
        "category: visualization              \n"
      );
      return data;
    }
  };

} // namespace Dune::BlockLab

#endif
