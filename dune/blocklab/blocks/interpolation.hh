#ifndef DUNE_BLOCKLAB_BLOCKS_INTERPOLATION_HH
#define DUNE_BLOCKLAB_BLOCKS_INTERPOLATION_HH

/** A solver block that interpolates a function given through
 *  an array (one entry per component of the function space tree)
 *  into a DoF vector. Context-based construction uses the
 *  MuParser library to parse functions from the inifile.
 */
#include<dune/blocklab/blocks/blockbase.hh>
#include<dune/blocklab/construction/callabletree.hh>
#include<dune/blocklab/construction/muparser.hh>
#include<dune/pdelab/gridfunctionspace/interpolate.hh>
#include<dune/typetree/utility.hh>

#include<array>
#include<functional>
#include<iostream>
#include<memory>


namespace Dune::BlockLab {

  /** @brief A block that implements interpolation of an analytic function into a finite element container */
  template<typename P, typename V, std::size_t i>
  class InterpolationBlock
    : public BlockBase<P, V, i>
  {
    public:
    //! The traits class for this block
    using Traits = typename BlockBase<P, V, i>::Traits;
    using FunctionSignature = typename Traits::Range(typename Traits::Entity, typename Traits::GlobalCoordinate);

    /** @copydoc BlockBase::BlockBase(Context&, const YAML::Node&) */
    template<typename Context>
    InterpolationBlock(Context& ctx, const YAML::Node& config)
      : BlockBase<P, V, i>(ctx, config)
      , funcs(muparser_callable_array<Dune::TypeTree::TreeInfo<typename Traits::GridFunctionSpace>::leafCount, FunctionSignature>(
                             config["functions"].as<std::string>(),
                             ctx.getSolver())
			   )
    {}

    //! We use the virtual default destructor
    virtual ~InterpolationBlock() = default;

    /** @copydoc BlockBase::blockData() */
    static std::vector<std::string> blockData()
    {
      auto data = BlockBase<P, V, i>::blockData();
      data.push_back(
        "title: Function Interpolation           \n"
        "category: pdelab                        \n"
        "schema:                                 \n"
        "  functions:                            \n"
        "    type: string                        \n"
        "    default: 0                          \n"
        "    meta:                               \n"
        "      title: Function                   \n"
      );
      return data;
    }

    /** @copydoc BlockBase::apply() */
    virtual void apply() override
    {
      auto vector = this->solver->template getVector<i>();
      auto gfs = vector->gridFunctionSpaceStorage();
      auto gf = Dune::BlockLab::makeGridFunctionTreeFromCallables(*gfs, funcs);

      this->logEvent("Interpolating into solution vector");
      Dune::PDELab::interpolate(gf, *gfs, *vector);
    }

    private:
    // Store the lambdas
    std::array<std::function<FunctionSignature>,
	       Dune::TypeTree::TreeInfo<typename Traits::GridFunctionSpace>::leafCount
	       > funcs;
  };

} // namespace Dune::BlockLab

#endif
