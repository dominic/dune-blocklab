#ifndef DUNE_BLOCKLAB_BLOCKS_ERROR_HH
#define DUNE_BLOCKLAB_BLOCKS_ERROR_HH

#include<dune/blocklab/blocks/blockbase.hh>
#include<dune/blocklab/blocks/enableif.hh>
#include<dune/blocklab/construction/callabletree.hh>
#include<dune/blocklab/construction/muparser.hh>
#include<dune/pdelab/common/functionutilities.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionadapter.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include<dune/pdelab/gridfunctionspace/interpolate.hh>
#include<dune/typetree/utility.hh>

#include<array>
#include<functional>
#include<memory>
#include<string>


namespace Dune::BlockLab {


  /** @brief A block that calculates the discretization error of a finite element function 
   * 
   * This block is currently only implemented for scalar problems.
  */  
  template<typename P, typename V, std::size_t i, typename enabled = disabled>
  class DiscretizationErrorBlock
  /** @cond DISABLED_BLOCKS */
    : public DisabledBlock<P, V, i>
  {
    public:
    template<typename Context>
    DiscretizationErrorBlock(Context& ctx, const YAML::Node& config)
      : DisabledBlock<P, V, i>(ctx, config)
    {}
  };

  template<typename P, typename V, std::size_t i>
  class DiscretizationErrorBlock<P, V, i, enableBlock<isScalar<P, V, i>()>>
  /** @cond DISABLED_BLOCKS */
    : public BlockBase<P, V, i>
  {
    public:
    //! The traits class for this block
    using Traits = typename BlockBase<P, V, i>::Traits;
    using FunctionSignature = typename Traits::Range(typename Traits::Entity, typename Traits::GlobalCoordinate);

    /** @copydoc BlockBase::BlockBase(Context&, const YAML::Node&) */
    template<typename Context>
    DiscretizationErrorBlock(Context& ctx, const YAML::Node& config)
      : BlockBase<P, V, i>(ctx, config)
	  , funcs(muparser_callable_array<Dune::TypeTree::TreeInfo<typename Traits::GridFunctionSpace>::leafCount, FunctionSignature>(
                               config["analytic"].as<std::string>(),
                               ctx.getSolver()))
      , norm(config["norm"].as<std::string>())
      , name(config["name"].as<std::string>())
    {}

    //! We use the virtual default destructor
    virtual ~DiscretizationErrorBlock() = default;

    /** @copydoc BlockBase::blockData() */
    static std::vector<std::string> blockData()
    {
      auto data = BlockBase<P, V, i>::blockData();
      data.push_back(
        "title: Discretization Error Calculation   \n"
        "category: pdelab                          \n"
        "schema:                                   \n"
        "  analytic:                               \n"
        "    required: true                        \n"
        "    type: string                          \n"
        "    default: 0                            \n"
        "    meta:                                 \n"
        "      title: Analytic Solution            \n"
        "  norm:                                   \n"
        "    type: string                          \n"
        "    default: l2                           \n"
        "    allowed:                              \n"
        "      - l2                                \n"
        "    meta:                                 \n"
        "      title: Norm                         \n"
        "  name:                                   \n"
        "    type: string                          \n"
        "    default: error                        \n"
        "    meta:                                 \n"
        "      title: Variable Name                \n"
      );
      return data;
    }

    /** @copydoc BlockBase::setup() */
    virtual void setup() override
    {
      this->solver->template introduce_parameter<double>(name, 0.0);
    }

    /** @copydoc BlockBase::apply() */
    virtual void apply() override
    {
      auto vector = this->solver->template getVector<i>();
      auto gfs = vector->gridFunctionSpaceStorage();

      // Make a grid function object for the analytic solution
      auto f_analytic = Dune::BlockLab::makeGridFunctionTreeFromCallables(*gfs, funcs);

      if (norm == "l2")
      {
        Dune::PDELab::DiscreteGridFunction<typename Traits::GridFunctionSpace, typename Traits::Vector> dgf(gfs, vector);
        Dune::PDELab::DifferenceSquaredAdapter diff(dgf, f_analytic);
        // TODO Vector-valued stuff eternally broken
        Dune::FieldVector<double, 1> sum = 0.0;
        // TODO configure integration order
        Dune::PDELab::integrateGridFunction(diff, sum, 4);
        sum = dgf.getGridView().comm().sum(sum);

        this->solver->update_parameter(name, std::sqrt(sum));
      }
      else
        DUNE_THROW(Dune::Exception, "Norm unknown in discretization error calculation!");
    }

    private:
    // Store the lambdas that describe the analytic solution
    std::array<std::function<FunctionSignature>,
	       Dune::TypeTree::TreeInfo<typename Traits::GridFunctionSpace>::leafCount
	       > funcs;

    std::string norm;
    std::string name;
  };

} // namespace Dune::BlockLab

#endif
