#ifndef DUNE_BLOCKLAB_BLOCKS_LINEARSOLVER_HH
#define DUNE_BLOCKLAB_BLOCKS_LINEARSOLVER_HH

/* A solver block for a linear solver.
 * This currently unconditionally uses a direct solver, which is
 * completely unacceptable. However, configurable ISTL solvers are
 * currently implemented upstream and I want to transition directly
 * to that work instead of building another thing.
 */

#include<dune/blocklab/blocks/blockbase.hh>
#include<dune/blocklab/operators/virtualinterface.hh>
#include<dune/pdelab/backend/istl.hh>
#include<dune/pdelab/gridoperator/gridoperator.hh>
#include<dune/pdelab/stationary/linearproblem.hh>

#include<memory>
#include<variant>

namespace Dune::BlockLab {

  /** @brief A block that applies a linear solver
   * 
   * This block applies a linear solver. It is currently limited to a
   * direct UMFPack solver, but this is not really a technical limitation,
   * more solver will be available in the near future.
   * 
   * The operator for the linear solver is provided as a subblock named @a operator.
   */
  template<typename P, typename V, std::size_t i>
  class LinearSolverBlock
    : public NamedParentBlockBase<P, V>
  {
    public:
    //! The traits class for this block
    using Traits = BlockTraits<P, V, i>;

    using VirtLocalOperator = AbstractLocalOperatorInterface<typename Traits::GridFunctionSpace>;
    using GridOperator = Dune::PDELab::GridOperator<typename Traits::GridFunctionSpace,
						    typename Traits::GridFunctionSpace,
						    VirtLocalOperator,
						    Dune::PDELab::ISTL::BCRSMatrixBackend<>,
						    typename Traits::ctype,
						    typename Traits::Range,
						    typename Traits::Range,
						    typename Traits::ConstraintsContainer,
						    typename Traits::ConstraintsContainer>;

    using LinearSolver = Dune::PDELab::ISTLBackend_SEQ_UMFPack;
    using StationaryLinearProblemSolver = Dune::PDELab::StationaryLinearProblemSolver<GridOperator, LinearSolver, typename Traits::Vector>;

    /** @copydoc AbstractBlockBase::AbstractBlockBase(Context&, const YAML::Node&) */
    template<typename Context>
    LinearSolverBlock(Context& ctx, const YAML::Node& config)
      : NamedParentBlockBase<P, V>(ctx, config, {"operator"})
    {}

    //! We use the virtual default destructor
    virtual ~LinearSolverBlock() = default;

    /** @copydoc NamedParentBlockBase::blockData() */
    static std::vector<std::string> blockData()
    {
      auto data = BlockBase<P, V, i>::blockData();
      data.push_back(
        "title: Linear Solver                        \n"
        "category: pdelab                            \n"
        "schema:                                     \n"
        "  operator:                                 \n"
        "    type: dict                              \n"
        "    minlength: 1                            \n"
        "    maxlength: 1                            \n"
        "    meta:                                   \n"
        "      title: Operator                       \n"
        "      subtype: blocks                       \n"
      );
      return data;
    }

    /** @copydoc NamedParentBlockBase::update_parameter(std::string, typename Traits::Parameter) */
    virtual void update_parameter(std::string name, typename Traits::Parameter param) override
    {
      this->recursive_update_parameter(name, param);
      if (name == this->getChildBlock("operator")->getBlockName() ||
          (name == "adapted" && std::get_if<bool>(&param)))
      {
        // The operator changed - rebuild the linear solver object!
        auto vector = this->solver->template getVector<i>();
        auto cc = this->solver->template getConstraintsContainer<i>();
        auto gfs = vector->gridFunctionSpaceStorage();
        auto localoperator = this->solver->template param<std::shared_ptr<VirtLocalOperator>>(this->getChildBlock("operator")->getBlockName());
        Dune::PDELab::ISTL::BCRSMatrixBackend<> mb(21);
        gridoperator = std::make_shared<GridOperator>(*gfs, *cc, *gfs, *cc, *localoperator, mb);
        linearsolver = std::make_shared<LinearSolver>(0);
        slp = std::make_shared<StationaryLinearProblemSolver>(*gridoperator, *linearsolver, *vector, 1e-12);
      }
    }

    /** @copydoc NamedParentBlockBase::apply() */
    virtual void apply() override
    {
      this->logEvent("Applying linear Solver!");
      this->recursive_apply();
      slp->apply();
    }

    protected:
    std::shared_ptr<LinearSolver> linearsolver;
    std::shared_ptr<GridOperator> gridoperator;
    std::shared_ptr<StationaryLinearProblemSolver> slp;
  };

} // namespace Dune::BlockLab

#endif
