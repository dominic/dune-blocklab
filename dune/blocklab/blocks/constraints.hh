#ifndef DUNE_BLOCKLAB_BLOCKS_CONSTRAINTS_HH
#define DUNE_BLOCKLAB_BLOCKS_CONSTRAINTS_HH

#include<dune/blocklab/blocks/blockbase.hh>
#include<dune/blocklab/construction/callabletree.hh>
#include<dune/blocklab/construction/muparser.hh>
#include<dune/pdelab/constraints/common/constraints.hh>
#include<dune/typetree/utility.hh>

#include<array>
#include<memory>


namespace Dune::BlockLab {

  /** @brief A block that implements constraints assembly */
  template<typename P, typename V, std::size_t i>
  class ConstraintsBlock
    : public BlockBase<P, V, i>
  {
    public:
    //! The traits class for this block
    using Traits = typename BlockBase<P, V, i>::Traits;
    using FunctionSignature = bool(typename Traits::GridView::Intersection, typename Traits::GridView::Intersection::Geometry::LocalCoordinate);

    /** @copydoc BlockBase::BlockBase(Context&, const YAML::Node&) */
    template<typename Context>
    ConstraintsBlock(Context& ctx, const YAML::Node& config)
      : BlockBase<P, V, i>(ctx, config)
      , funcs(muparser_callable_array<Dune::TypeTree::TreeInfo<typename Traits::GridFunctionSpace>::leafCount, FunctionSignature>(
                         config["functions"].as<std::string>(),
                         ctx.getSolver())
			 )
    {}

    //! We use the virtual default destructor
    virtual ~ConstraintsBlock() = default;

    /** @copydoc BlockBase::blockData() */
    static std::vector<std::string> blockData()
    {
      auto data = BlockBase<P, V, i>::blockData();
      data.push_back(
        "title: Constraints Assembly                                                      \n"
        "category: pdelab                                                                 \n"
        "schema:                                                                          \n"
        "  functions:                                                                     \n"
        "    required: true                                                               \n"
        "    type: string                                                                 \n"
        "    default: 0                                                                   \n"
        "    meta:                                                                        \n"
        "      title: Constraints Function                                                \n"
        "      help: >                                                                    \n"
        "        This parameter specifies how exactly constraints are to be assembled.    \n"
        "        The given string is a comma-separated list of functions each of which    \n"
        "        is attached to one child of the function space tree (for scalar problems \n"
        "        you only need to specify one function, otherwise one function per leaf   \n"
        "        of the function space tree). The functions are expected to return 1 if   \n"
        "        a degree of freedom is constrained and 0 if it isn't. The symbols        \n"
        "        \\(x\\), \\(y\\) and \\(z\\) can be used to access the global coordinates\n"
        "        of the degree of freedom.                                                \n"
        "help: >                                                                          \n"
        "  Constraints assembly is an integral part of PDELab's core building blocks.     \n"
        "  bla                                                                            \n"
        "  blubb                                                                          \n"
        "  foo                                                                            \n"
      );
      return data;
    }

    /** @copydoc BlockBase::apply() */
    virtual void apply() override
    {
      auto constraintscontainer = this->solver->template getConstraintsContainer<i>();
      auto gfs = this->solver->template getVector<i>()->gridFunctionSpaceStorage();
      auto bctype = makeBoundaryConditionTreeFromCallables(*gfs, funcs);

      Dune::PDELab::constraints(bctype, *gfs, *constraintscontainer);
      this->logEvent("Assembled constraints - " + std::to_string(constraintscontainer->size()) + " of " + std::to_string(gfs->size()) + " dofs constrained!");
    }

    private:
    // Store the lambdas
    std::array<std::function<FunctionSignature>,
               Dune::TypeTree::TreeInfo<typename Traits::GridFunctionSpace>::leafCount
               > funcs;
  };

} // namespace Dune::BlockLab

#endif
