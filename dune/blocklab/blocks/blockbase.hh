#ifndef DUNE_BLOCKLAB_BLOCKS_BLOCKBASE_HH
#define DUNE_BLOCKLAB_BLOCKS_BLOCKBASE_HH

/** @file blockbase.hh
 *
 *  This file provides the (abstract) base classes all solver blocks are derived from.
 *  It is the main entry point that one could study when implementing one's own block.
 */

#include<dune/blocklab/blocks/blocktraits.hh>
#include<dune/blocklab/events.hh>
#include<dune/blocklab/utilities/enumerate.hh>
#include<dune/blocklab/utilities/stringsplit.hh>
#include<dune/blocklab/utilities/yaml.hh>
#include<dune/common/shared_ptr.hh>

#include<filesystem>
#include<memory>
#include<tuple>
#include<vector>


namespace Dune::BlockLab {

  /** @brief The abstract base class for a solver block.
   *
   * This is the abstract base class that all solver blocks are derived from. Instead
   * of directly inheriting of this base class, one should look out for a more specialized
   * base class by reading through the list in the @ref blocks guide.
   * 
   * @tparam P The @c std::tuple of additional types in the parameter system
   * @tparam V The @c std::tuple of finite element containers used in the block solver
   * 
   * All block implementations should be templated to these two types.
   *
   * @ingroup blocks_baseclass
   * @ingroup blocks_custom
   */
  template<typename P, typename V>
  class AbstractBlockBase
    : public std::enable_shared_from_this<AbstractBlockBase<P, V>>
  {
    public:
    //! The traits class for this block
    using Traits = BlockTraits<P, V>;

    /** @brief Constructs an object given a construction context and a YAML configuration
     *
     * For documentation which values a block accepts in their YAML configuration,
     * you should check the user documentation which is generated using a different
     * mechanism.
     * 
     * @tparam Context The context type, which will be @ref ConstructionContext most of the time.
     * @param ctx The context object for solver construction.
     * @param config The YAML configuration object. You can expect this to be validated
     *               against the schema defined in the @ref blockData method.
     */
    template<typename Context>
    AbstractBlockBase(Context& ctx, const YAML::Node& config)
	    : enabled(config["enabled"].as<bool>())
      , blockname(config["_blockname"].as<std::string>())
    {}

    //! We use the virtual default destructor
    virtual ~AbstractBlockBase() = default;

    /** @brief Set the parent block of this block.
     *
     * This method is used to propagate parent/children information across
     * the block graph. Do not directly override this, but use a specialized
     * base class instead as outlined in the @ref blocks guide.
     */
    virtual void set_parent(std::shared_ptr<typename Traits::BlockBase> parent) = 0;

    /** @brief Link this block to a solver instance.
     *
     * All blocks need to have knowledge of the solver instance they are attached
     * to. This method propagates this information. Do not directly override this,
     * but use a specialized base class instead as outlined in the @ref blocks guide.
     */
    virtual void set_solver(std::shared_ptr<typename Traits::Solver> solver) = 0;

    /** @brief Set up this block.
     *
     * This method is called exactly once on each block before any call to @ref apply
     * on any block. Use this to set up expensive objects or to register parameters
     * in the solver's parameter system with @ref introduce_parameter .
     */
    virtual void setup() = 0;

    /** @brief Apply this block.
     *
     * This method does the main chunk of work of this block. It is called by the solver.
     * In the case of blocks being attached to @ref ParentBlockBase instance, this method
     * may be called multiple times.
     */
    virtual void apply() = 0;

    /** @brief Notify block of an update solver parameter.
     *
     * Whenever a parameter in the solver changed, this method is called on each block.
     * For information on how exactly the parameter system works, see the @ref parameter
     * guide.
     */
    virtual void update_parameter(std::string name, typename Traits::Parameter param) = 0;

    /** @brief Return schema data for this block.
     *
     * This static method returns meta data for this block. This data is used for two purposes:
     *   * Input configuration is validated against the given schema
     *   * The data can be exported from applications to generate a frontend specification.
     *     More about this can be read in the @ref app and @ref frontend guides.
     * 
     * @ingroup blocks_config
     */
    static std::vector<std::string> blockData()
    {
      return {
          "schema:                 \n"
          "  _blockname:           \n"
          "    type: string        \n"
          "    required: true      \n"
          "  enabled:              \n"
          "    type: boolean       \n"
          "    default: true       \n"
          "    meta:               \n"
          "      title: Enabled    \n"
          "      subtype: omit     \n"
          "  loglevel:             \n"
          "    type: integer       \n"
          "    default: -1         \n"
          "    meta:               \n"
          "      subtype: omit     \n"
          "      title: Log Level  \n"
      };
    }

    //! Get whether this block is currently enabled
    bool getEnabled() const
    {
      return enabled;
    }

    //! Get the human-readable block name
    std::string getBlockName() const
    {
      return blockname;
    }

    //! Get the log-level of this block
    LogLevel getLogLevel() const
    {
      return blocklevel;
    }

    //! Trigger a logging event associated with this block.
    void logEvent(const std::string& message, LogLevel level = LogLevel::INFO)
    {
      EventDistributor::instance().distribute(std::make_shared<BlockLogEvent>(*this, message, level));
    }

    protected:
    //! The stored solver instance
    std::shared_ptr<typename Traits::Solver> solver;
    //! The stored parent block
    std::shared_ptr<AbstractBlockBase<P, V>> parent;
    //! Whether this block is enabled
    bool enabled;
    //! This block's name
    std::string blockname;
    //! This block's log level
    LogLevel blocklevel;
  };

  namespace impl {

    constexpr std::size_t zero_if_undefined(std::size_t i)
    {
      if (i == std::numeric_limits<std::size_t>::max())
        return 0;
      else
        return i;
    }

  }

  /** @brief A base class for a block without child blocks
   * 
   * This is the standard non-abstract base class to be derive from
   * when implementing a block without child blocks.
   * 
   * @tparam P The @c std::tuple of additional types in the parameter system
   * @tparam V The @c std::tuple of finite element containers used in the block solver
   * @tparam i The index in the tuple of vector containers that this block operates on
   * 
   * The template parameter @c i is optional and should be used if and only if
   * the block depends on a finite element or constraints container. If in use, the following
   * snippets of code yield the respective objects:
   * @arg <tt>this->solver->template getVector<i>()</tt> for the finite element container
   * @arg <tt>this->solver->template getConstraintsContainer<i>()</tt> for the constraints container
   *
   * All block implementations derived from this class should use the same template
   * parameters and forward them to this base class.
   * 
   * @ingroup blocks_baseclass
   */
  template<typename P, typename V, std::size_t i = std::numeric_limits<std::size_t>::max()>
  class BlockBase
    : public AbstractBlockBase<P, V>
  {
    public:
    //! The traits class for this block
    using Traits = BlockTraits<P, V, impl::zero_if_undefined(i)>;

    /** @copydoc AbstractBlockBase::AbstractBlockBase(Context&, const YAML::Node&) */
    template<typename Context>
    BlockBase(Context& ctx, const YAML::Node& config)
      : AbstractBlockBase<P, V>(ctx, config)
    {}

    //! A block can only constructed with a construction context and a configuration
    BlockBase() = delete;

    //! We use the virtual default destructor
    virtual ~BlockBase() = default;

    /** @copydoc AbstractBlockBase::set_parent(std::shared_ptr<typename Traits::BlockBase>) */
    virtual void set_parent(std::shared_ptr<typename Traits::BlockBase> parent) override final
    {
      this->parent = parent;
    }

    /** @copydoc AbstractBlockBase::set_solver(std::shared_ptr<typename Traits::BlockBase>) */
    virtual void set_solver(std::shared_ptr<typename Traits::Solver> solver_) override final
    {
      this->solver = solver_;
    }

    /** @copydoc AbstractBlockBase::setup() */
    virtual void setup() override
    {}

    /** @copydoc AbstractBlockBase::apply() */
    virtual void apply() override
    {}

    /** @copydoc AbstractBlockBase::update_parameter(std::string, typename Traits::Parameter) */
    virtual void update_parameter(std::string name, typename Traits::Parameter param) override
    {}

    /** @copydoc AbstractBlockBase::blockData() */
    static std::vector<std::string> blockData()
    {
      auto data = AbstractBlockBase<P, V>::blockData();
      if (i != std::numeric_limits<std::size_t>::max())
        data.push_back(
          "schema:                                                  \n"
          "  vector:                                                \n"
          "    type: integer                                        \n"
          "    default: 0                                           \n"
          "    meta:                                                \n"
          "      title: Vector                                      \n"
          "      subtype: vector                                    \n"
          "      help: >                                            \n"
          "        This specifies which vector of the block solver  \n"
          "        this block operates on. Each block solver has a  \n"
          "        number of finite element functions (represented  \n"
          "        by a container) that are considered first class  \n"
          "        citizens of the solver. These are configured in  \n"
          "        the main solver node.                            \n"
        );
      return data;
    }
  };

  /** @brief A base class for a block with child blocks
   * 
   * This is the standard non-abstract base class you should be using
   * for a block that accepts an arbitrary amount of child blocks.
   * 
   * @tparam P The @c std::tuple of additional types in the parameter system
   * @tparam V The @c std::tuple of finite element containers used in the block solver
   * 
   * When inheriting from this block, beware that recursive traversal of child blocks
   * should be done using the dedicated @c recursive_* methods instead of reimplementing
   * recursion.
   * 
   * @ingroup blocks_baseclass
   */
  template<typename P, typename V>
  class ParentBlockBase
    : public AbstractBlockBase<P, V>
  {
    public:
    //! The traits class for this block
    using Traits = BlockTraits<P, V>;

    /** \brief A block can only constructed with a construction context and a configuration */
    ParentBlockBase() = delete;

    /** @copydoc AbstractBlockBase::AbstractBlockBase(Context&, const YAML::Node&)
     *
     * This constructor reads the @a blocks subsection of given configuration and
     * constructs subblocks from it.
     */
    template<typename Context>
    ParentBlockBase(Context& ctx, const YAML::Node& config)
      : ParentBlockBase(ctx, config, {"blocks"})
    {}

    /** @copydoc AbstractBlockBase::AbstractBlockBase(Context&, const YAML::Node&)
     *
     * \param names A vector of names that describes subsections in the given configuration
     *              that we are constructing subblocks from.
     * 
     * This constructor can be used to construct children from sections in the configuration
     * that are named differently than @a blocks. If you want this, you should first check
     * whether @ref NamedParentBlockBase is what you are looking for.
    */
    template<typename Context>
    ParentBlockBase(Context& ctx, const YAML::Node& config, const std::vector<std::string>& names)
      : AbstractBlockBase<P, V>(ctx, config)
    {
      // Iterate over the given names - each of which represents a
      // subdictionary with blocks to construct
      for (auto name: names)
      {
        if(config[name] && config[name].IsMap())
        {
          auto it = config[name].begin();
          YAML::Node child = it->second;
          blocks.push_back(ctx.constructBlock(it->first.as<std::string>(), child));
        }
        else
        {
          for(auto child: config[name])
            blocks.push_back(ctx.constructBlock(child["_type"].as<std::string>(), child));
        }
      }
    }

    public:
    //! We use the virtual default destructor
    virtual ~ParentBlockBase() = default;

    /** @copydoc AbstractBlockBase::set_parent(std::shared_ptr<typename Traits::BlockBase>) */
    virtual void set_parent(std::shared_ptr<typename Traits::BlockBase> parent) override final
    {
      this->parent = parent;
      for (auto block : blocks)
        block->set_parent(this->shared_from_this());
    }

    /** @copydoc AbstractBlockBase::set_solver(std::shared_ptr<typename Traits::Solver>) */
    virtual void set_solver(std::shared_ptr<typename Traits::Solver> solver_) override final
    {
      this->solver = solver_;
      for (auto block : blocks)
        block->set_solver(solver_);
    }

    /** @copydoc AbstractBlockBase::update_parameter(std::string, typename Traits::Parameter)
     *
     * When overriding this implementation, recursion across the attached subblocks
     * should be done using the @ref recursive_update_parameter method.
     */
    virtual void update_parameter(std::string name, typename Traits::Parameter param) override
    {
      recursive_update_parameter(name, param);
    }

    /** @brief Implements recursion of the @ref update_parameter method across child blocks
     * 
     * When inheriting from this class, you should call this from @ref update_parameter
     * to ensure that subblocks of your blocks will be notified of parameter changes.
     */
    void recursive_update_parameter(std::string name, typename Traits::Parameter param)
    {
      for (auto block : blocks)
        block->update_parameter(name, param);
    }

    /** @copydoc AbstractBlockBase::apply() 
     * 
     * When overriding this implementation, recursion across the attached subblocks
     * should be done using the @ref recursive_apply method.
     */
    virtual void apply() override
    {
      recursive_apply();
    }

    /** @brief Implements recursion of the @ref apply method across child blocks
     * 
     * When inheriting from this class, you should call this from @ref apply
     * to ensure that subblocks of your blocks will function properly.
     */
    void recursive_apply()
    {
      for (auto block : blocks)
      {
        if (block->getEnabled())
        {
          triggerBlockStatusEvent(*block, BlockStatus::STARTED);
          block->apply();
          triggerBlockStatusEvent(*block, BlockStatus::STOPPED);
        }
        else
          triggerBlockStatusEvent(*block, BlockStatus::SKIPPED);
      }
    }

    /** @copydoc AbstractBlockBase::setup() 
     * 
     * When overriding this implementation, recursion across the attached subblocks
     * should be done using the @ref recursive_setup method.
     */
    virtual void setup() override
    {
      recursive_setup();
    }

    /** @brief Implements recursion of the @ref setup method across child blocks
     * 
     * When inheriting from this class, you should call this from @ref setup
     * to ensure that subblocks of your blocks will function properly.
     */
    void recursive_setup()
    {
      for (auto block : blocks)
        if (block->getEnabled())
          block->setup();
    }

    /** @copydoc AbstractBlockBase::blockData() */
    static std::vector<std::string> blockData()
    {
      auto data = AbstractBlockBase<P, V>::blockData();
      data.push_back(
        "schema:                 \n"
        "  blocks:               \n"
        "    type: dict          \n"
        "    meta:               \n"
        "      title: Blocks     \n"
        "      subtype: blocks   \n"
      );
      return data;
    }

    protected:
    //! The stored vector of child blocks
    std::vector<std::shared_ptr<AbstractBlockBase<P, V>>> blocks;
  };

  /** @brief A base class for a block with arbitrarily named children
   * 
   * This is a block base class that accepts child blocks. In contrast to the
   * more generic @ref ParentBlockBase , it reads those blocks from arbitrarily
   * named sections of the configuration and allows to access the child blocks
   * by this name and not only through iteration of all child blocks.
   * 
   * @tparam P The @c std::tuple of additional types in the parameter system
   * @tparam V The @c std::tuple of finite element containers used in the block solver
   * 
   * When inheriting from this block, beware that recursive traversal of child blocks
   * should be done using the dedicated @c recursive_* methods instead of reimplementing
   * recursion.
   * 
   * @ingroup blocks_baseclass
   */
  template<typename P, typename V>
  class NamedParentBlockBase
    : public ParentBlockBase<P, V>
  {
    public:
    //! The traits class for this block
    using Traits = BlockBase<P, V>;

    /** \brief A block can only constructed with a construction context and a configuration */
    NamedParentBlockBase() = delete;

    /** @copydoc AbstractBlockBase::AbstractBlockBase(Context&, const YAML::Node&)
     *
     * \param names A vector of names that describes subsections in the given configuration
     *              that we are constructing subblocks from.
    */
    template<typename Context>
    NamedParentBlockBase(Context& ctx, const YAML::Node& config, const std::vector<std::string>& names)
      : ParentBlockBase<P, V>(ctx, config, names)
    {
      for(auto [i, n] : enumerate(names))
        namemapping[n] = i;
    }

    //! We use the virtual default destructor
    virtual ~NamedParentBlockBase() = default;

    /** @brief Retrieve a block given its name
     * 
     * @param name The string name identifying the block
     * 
     * This method retrieves a block given its name.
     */
    std::shared_ptr<AbstractBlockBase<P, V>> getChildBlock(const std::string& name)
    {
      return this->blocks[namemapping[name]];
    }

    /** @copydoc AbstractBlockBase::blockData() */
    static std::vector<std::string> blockData()
    {
      // Note bene: Derived classes need to add the child blocks
      // to their schema. This base class has no compile-time knowledge
      // about the names of the child blocks.
      return AbstractBlockBase<P, V>::blockData();
    }

    private:
    //! The mapping of names to indices in the @c std::vector of @ref ParentBlockBase
    std::map<std::string, std::size_t> namemapping;
  };

  /** @brief A non-operational block
   *
   * This block can be used as a fallback when a block should be deactivated based on
   * an enable-if mechanism. See the @ref blocks guide for details.
   * 
   * @ingroup blocks_disable
   */
  template<typename P, typename V, std::size_t i>
  class DisabledBlock
    : public BlockBase<P, V, i>
  {
    public:
    /** @copydoc BlockBase::BlockBase(Context&, const YAML::Node&) */
    template<typename Context>
    DisabledBlock(Context& ctx, const YAML::Node& config)
      : BlockBase<P, V, i>(ctx, config)
    {
      DUNE_THROW(Dune::Exception, "Trying to use a block that was disabled for vector '" + ctx.getVectorName(i) + "'");
    }

    /** @brief A disabled block does not export data */
    static std::vector<std::string> blockData()
    {
      // A disabled block returns nothing here and code such as the frontend exporter
      // can identify disabled blocks through this fact.
      return {};
    }
  };

} // namespace Dune::BlockLab

#endif
