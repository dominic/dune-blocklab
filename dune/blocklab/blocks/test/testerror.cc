#include"config.h"

#include<dune/blocklab/init.hh>
#include<dune/blocklab/blocks/error.hh>
#include<dune/blocklab/blocks/test/providersetup.hh>
#include<dune/common/test/testsuite.hh>
#include<dune/grid/uggrid.hh>

#include<memory>
#include<tuple>

int main(int argc, char** argv)
{
  auto init = Dune::BlockLab::initBlockLab(argc, argv);
  YAML::Node config, node;

  node["_type"] = "error";
  node["analytic"] = "1.0";
  config["solver"]["blocks"].push_back(node);

  auto ctx = structured_ug2_p1fem(init.helper, config);
  ctx.template registerBlock<Dune::BlockLab::DiscretizationErrorBlock>("error");

  auto solver = ctx.constructSolver(config["solver"]);
  solver->apply();

  Dune::TestSuite test;
  test.check(std::abs(solver->template param<double>("error") - 1.0) < 1e-8)
    << "Integration of constant 1 in error context failed";

  return test.exit();
}
