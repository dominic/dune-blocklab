#include"config.h"

#include<dune/blocklab/init.hh>
#include<dune/blocklab/blocks/probe.hh>
#include<dune/blocklab/blocks/interpolation.hh>
#include<dune/blocklab/blocks/test/providersetup.hh>
#include<dune/blocklab/utilities/yaml.hh>
#include<dune/common/fvector.hh>
#include<dune/common/test/testsuite.hh>

#include<memory>
#include<tuple>


int main(int argc, char** argv)
{
  auto init = Dune::BlockLab::initBlockLab(argc, argv);
  Dune::TestSuite test;
  YAML::Node config;

  config = YAML::Load(
    "solver:\n"
    "  blocks:\n"
    "    -\n"
    "      _type: interpolation\n"
    "      functions: x * y\n"
    "    -\n"
    "      _type: probe\n"
    "      name: myprobe\n"
    "      position:\n"
    "        - 0.5\n"
    "        - 0.5"
  );

  auto ctx = structured_ug2_p2fem(init.helper, config);
  ctx.template registerBlock<Dune::BlockLab::InterpolationBlock>("interpolation");
  ctx.template registerBlock<Dune::BlockLab::ProbeBlock>("probe");

  auto solver = ctx.constructSolver(config["solver"]);
  solver->apply();

  // We (ab)use the error node to validate the integral of the interpolate solution
  test.check(std::abs(solver->template param<Dune::FieldVector<double, 1>>("myprobe") - 0.25) < 1e-8)
     << "Probing function x*y at (0.5, 0.5) yielded wrong result.";

  return test.exit();
}
