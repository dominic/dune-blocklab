#include"config.h"

#include<dune/blocklab/init.hh>
#include<dune/blocklab/blocks/control.hh>
#include<dune/blocklab/blocks/test/providersetup.hh>
#include<dune/common/parallel/mpihelper.hh>
#include<dune/common/test/testsuite.hh>

#include<memory>
#include<tuple>


template<typename P, typename V>
class Dummy
  : public Dune::BlockLab::BlockBase<P, V>
{
  public:
  template<typename Context>
  Dummy(Context& ctx, const YAML::Node& config)
    : Dune::BlockLab::BlockBase<P, V>(ctx, config)
  {}

  virtual ~Dummy() = default;

  virtual void setup() override
  {
    this->solver->template introduce_parameter<int>("count", 0);
  }

  virtual void apply() override
  {
    this->solver->update_parameter("count", this->solver->template param<int>("count") + 1);
  }
};


int main(int argc, char** argv)
{
  auto init = Dune::BlockLab::initBlockLab(argc, argv);
  Dune::TestSuite test;
  YAML::Node config;

  config = YAML::Load(
    "solver:\n"
    "  blocks:\n"
    "    - \n"
    "      _type: repeat\n"
    "      blocks:\n"
    "        -\n"
    "          _type: dummy\n"
    "      iterations: 42"
  );

  auto ctx = structured_ug2_p1fem(init.helper, config);
  ctx.template registerBlock<Dune::BlockLab::RepeatBlock>("repeat");
  ctx.template registerBlock<Dummy>("dummy");

  auto solver = ctx.constructSolver(config["solver"]);
  solver->apply();

  test.check(solver->template param<int>("count") == 42)
     << "Dummy block was not executed the correct amount of times";

  return test.exit();
}
