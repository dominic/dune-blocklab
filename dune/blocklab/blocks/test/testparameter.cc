#include"config.h"

#include<dune/blocklab/init.hh>
#include<dune/blocklab/blocks/parameter.hh>
#include<dune/blocklab/blocks/test/providersetup.hh>
#include<dune/common/test/testsuite.hh>


int main(int argc, char** argv)
{
  auto init = Dune::BlockLab::initBlockLab(argc, argv);
  Dune::TestSuite test;
  YAML::Node config;

  config = YAML::Load(
    "solver:\n"
    "  blocks:\n"
    "    -\n"
    "      _type: parameter\n"
    "      datatype: int\n"
    "      value: 42\n"
    "      name: myparam"
  );

  auto ctx = structured_ug2_p1fem(init.helper, config);
  ctx.template registerBlock<Dune::BlockLab::ParameterBlock>("parameter");

  auto solver = ctx.constructSolver(config["solver"]);
  solver->apply();

  test.check(solver->template param<int>("myparam") == 42)
     << "Retrieval of int parameter myparam failed";

  return test.exit();
}
