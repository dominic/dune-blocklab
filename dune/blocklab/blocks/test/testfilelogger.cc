#include"config.h"

#include<dune/blocklab/init.hh>
#include<dune/blocklab/blocks/filelogger.hh>
#include<dune/blocklab/blocks/parameter.hh>
#include<dune/blocklab/blocks/test/providersetup.hh>
#include<dune/common/test/testsuite.hh>

#include<fstream>


int main(int argc, char** argv)
{
  auto init = Dune::BlockLab::initBlockLab(argc, argv);
  Dune::TestSuite test;
  YAML::Node config;

  config = YAML::Load(
    "solver:\n"
    "  blocks:\n"
    "    -\n"
    "      _type: parameter\n"
    "      datatype: int\n"
    "      value: 42\n"
    "      name: myparam\n"
    "    -\n"
    "      _type: filelogger\n"
    "      filename: fileloggertest.log\n"
    "      parameter: myparam\n"
    "      datatype: int"
  );

  // Nest this in a block so that the fstream destructor is called
  // for sure before we assert the log file content.
  {
    auto ctx = structured_ug2_p1fem(init.helper, config);
    ctx.template registerBlock<Dune::BlockLab::ParameterBlock>("parameter");
    ctx.template registerBlock<Dune::BlockLab::FileLoggerBlock>("filelogger");

    auto solver = ctx.constructSolver(config["solver"]);
    solver->apply();
  }

  std::fstream stream("fileloggertest.log");
  int parse;
  stream >> parse;

  test.check(parse == 42)
     << "Retrieval of int parameter myparam from log file failed";

  return test.exit();
}
