#include"config.h"

#include<dune/blocklab/init.hh>
#include<dune/blocklab/blocks/visualization.hh>
#include<dune/blocklab/blocks/test/providersetup.hh>
#include<dune/common/test/testsuite.hh>

#include<memory>
#include<tuple>


int main(int argc, char** argv)
{
  auto init = Dune::BlockLab::initBlockLab(argc, argv);

  Dune::TestSuite test;
  YAML::Node config;

  config = YAML::Load(
    "solver:\n"
    "  blocks:\n"
    "    -\n"
    "      _type: visualization\n"
    "      filename: vtktest\n"
    "      blocks:\n"
    "        -\n"
    "          _type: vis_vector\n"
    "        -\n"
    "          _type: vis_mpirank\n"
    "        -\n"
    "          _type: vis_indexset"
  );

  auto ctx = structured_ug2_p1fem(init.helper, config);
  ctx.template registerBlock<Dune::BlockLab::VisualizationBlock>("visualization");
  ctx.template registerBlock<Dune::BlockLab::IndexSetVisualizationBlock>("vis_indexset");
  ctx.template registerBlock<Dune::BlockLab::MPIRankVisualizationBlock>("vis_mpirank");
  ctx.template registerBlock<Dune::BlockLab::VectorVisualizationBlock>("vis_vector");

  auto solver = ctx.constructSolver(config["solver"]);
  solver->apply();

  return test.exit();
}
