#include"config.h"

#include<dune/blocklab/init.hh>
#include<dune/blocklab/blocks/newton.hh>
#include<dune/blocklab/blocks/test/providersetup.hh>
#include<dune/blocklab/operators/convectiondiffusionfem.hh>
#include<dune/common/test/testsuite.hh>

#include<memory>
#include<tuple>


int main(int argc, char** argv)
{
  auto init = Dune::BlockLab::initBlockLab(argc, argv);
  Dune::TestSuite test;
  YAML::Node config, node;

  node["_type"] = "newton";
  node["operator"]["convectiondiffusionfem"]["enabled"] = "true";
  config["solver"]["blocks"].push_back(node);

  auto ctx = structured_ug2_p1fem(init.helper, config);
  ctx.template registerBlock<Dune::BlockLab::NewtonSolverBlock>("newton");
  ctx.template registerBlock<Dune::BlockLab::ConvectionDiffusionFEMBlock>("convectiondiffusionfem");

  auto solver = ctx.constructSolver(config["solver"]);
  solver->apply();

  return test.exit();
}
