#include "config.h"

#include <dune/blocklab/blocks/adaptivity.hh>
#include <dune/blocklab/blocks/blockbase.hh>
#include <dune/blocklab/blocks/control.hh>
#include <dune/blocklab/blocks/linearsolver.hh>
#include <dune/blocklab/blocks/test/providersetup.hh>
#include <dune/blocklab/init.hh>
#include <dune/blocklab/operators/convectiondiffusionfem.hh>

#include <dune/common/test/testsuite.hh>

#include <dune/grid/uggrid.hh>

#include <iostream>
#include <memory>
#include <string>
#include <tuple>

template<typename P, typename V>
class TrivialGridMarker : public Dune::BlockLab::BlockBase<P, V>
{
public:
  template<typename Context>
  TrivialGridMarker(Context& ctx, const YAML::Node& config)
    : Dune::BlockLab::BlockBase<P, V>(ctx, config)
  {}

  virtual ~TrivialGridMarker() = default;

  virtual void apply() override
  {
    // Mark the first element in the top-level (finest level) grid view
    auto grid = this->solver->template getGrid<0>();
    auto entity = *grid->levelGridView(grid->maxLevel()).template begin<0>();
    grid->mark(1, entity);
  }

  static std::vector<std::string> blockData()
  {
    auto data = Dune::BlockLab::BlockBase<P, V>::blockData();
    data.push_back("title: Grid Marker Block                 \n");
    return data;
  }
};

std::string cfg_str = "solver:\n"
                      "  blocks:\n"
                      "    - _type: repeat\n"
                      "      iterations: 2\n"
                      "      blocks:\n"
                      "        - _type: adaptivity\n"
                      "          blocks:\n"
                      "            - _type: grid_marker\n"
                      "        - _type: linearsolver\n"
                      "          operator:\n"
                      "            convectiondiffusionfem:\n"
                      "              enabled: True\n";

int
main(int argc, char** argv)
{
  auto init = Dune::BlockLab::initBlockLab(argc, argv);
  Dune::TestSuite test;
  YAML::Node config = YAML::Load(cfg_str);

  auto ctx = structured_ug2_p1fem(init.helper, config);
  ctx.template registerBlock<Dune::BlockLab::RepeatBlock>("repeat");
  ctx.template registerBlock<Dune::BlockLab::AdaptivityBlock>("adaptivity");
  ctx.template registerBlock<TrivialGridMarker>("grid_marker");
  ctx.template registerBlock<Dune::BlockLab::LinearSolverBlock>("linearsolver");
  ctx.template registerBlock<Dune::BlockLab::ConvectionDiffusionFEMBlock>(
    "convectiondiffusionfem");

  try {
    auto solver = ctx.constructSolver(config["solver"]);
    solver->apply();

    // Check if first grid cell in levels 0 and 1 are refined
    auto grid = solver->template getGrid<0>();
    test.check(not grid->levelGridView(0).template begin<0>()->isLeaf(),
               "First grid cell in level 0 was adapted and has child cells");
    test.check(not grid->levelGridView(1).template begin<0>()->isLeaf(),
               "First grid cell in level 1 was adapted and has child cells");
    test.check(grid->maxLevel() == 2, "Max hierarchic level in grid");
  }

  catch (std::exception& e) {
    test.check(false, "Adaptivity works")
      << "Error when creating solver: " << e.what();
  }

  return test.exit();
}
