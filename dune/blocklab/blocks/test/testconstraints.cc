#include"config.h"

#include<dune/blocklab/init.hh>
#include<dune/blocklab/blocks/constraints.hh>
#include<dune/blocklab/blocks/test/providersetup.hh>
#include<dune/common/test/testsuite.hh>

#include<memory>
#include<tuple>


int main(int argc, char** argv)
{
  auto init = Dune::BlockLab::initBlockLab(argc, argv);
  Dune::TestSuite test;
  YAML::Node config, node;

  node["_type"] = "constraints";
  node["functions"] = "x < 1e-8";
  config["solver"]["blocks"].push_back(node);

  auto ctx = structured_ug2_p1fem(init.helper, config);
  ctx.template registerBlock<Dune::BlockLab::ConstraintsBlock>("constraints");

  auto solver = ctx.constructSolver(config["solver"]);
  solver->apply();

  // We (ab)use the error node to validate the integral of the interpolate solution
  test.check(solver->template getConstraintsContainer<0>()->size() == 11)
     << "Number of constrained DoFs for x<eps yielded wrong result.";

  return test.exit();
}
