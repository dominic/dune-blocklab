#include"config.h"

#include<dune/blocklab/init.hh>
#include<dune/blocklab/blocks/variation.hh>
#include<dune/blocklab/blocks/test/providersetup.hh>
#include<dune/common/test/testsuite.hh>


template<typename P, typename V>
class Dummy
  : public Dune::BlockLab::BlockBase<P, V>
{
  public:
  template<typename Context>
  Dummy(Context& ctx, const YAML::Node& config)
    : Dune::BlockLab::BlockBase<P, V>(ctx, config)
    , increment(config["increment"].as<std::string>())
  {}

  virtual ~Dummy() = default;

  static std::vector<std::string> blockData()
  {
    auto data = Dune::BlockLab::BlockBase<P, V>::blockData();
    data.push_back(
      "schema:             \n"
      "  increment:        \n"
      "    type: string    \n"
      "    required: true  \n"
    );
    return data;
  }

  virtual void setup() override
  {
    this->solver->template introduce_parameter<double>("sum", 0.0);
  }

  virtual void apply() override
  {
    this->solver->update_parameter("sum", this->solver->template param<double>("sum") + this->solver->template param<double>(increment));
  }

  private:
  std::string increment;
};


int main(int argc, char** argv)
{
  auto init = Dune::BlockLab::initBlockLab(argc, argv);
  Dune::TestSuite test;

  {
    YAML::Node config;

    config = YAML::Load(
      "solver:\n"
      "  blocks:\n"
      "    -\n"
      "      _type: cv\n"
      "      blocks:\n"
      "        -\n"
      "          _type: dummy\n"
      "          increment: foo\n"
      "      iterations: 5\n"
      "      name: foo\n"
    );

    auto ctx = structured_ug2_p1fem(init.helper, config);
    ctx.template registerBlock<Dummy>("dummy");
    ctx.template registerBlock<Dune::BlockLab::ContinuousVariationBlock>("cv");

    auto solver = ctx.constructSolver(config["solver"]);
    solver->apply();

    test.check(std::abs(solver->template param<double>("sum") - 3.0) < 1e-8)
      << "Continuous variation parameter did not sum up as expected!";
  }

  {
    YAML::Node config;

    config = YAML::Load(
      "solver:\n"
      "  blocks:\n"
      "    dv:\n"
      "      blocks:\n"
      "        dummy:\n"
      "          increment: foo\n"
      "      values: 0.2, 0.4, 0.6, 0.8, 1.0\n"
      "      name: foo\n"
      "      datatype: double"
    );

    auto ctx = structured_ug2_p1fem(init.helper, config);
    ctx.template registerBlock<Dummy>("dummy");
    ctx.template registerBlock<Dune::BlockLab::DiscreteVariationBlock>("dv");

    auto solver = ctx.constructSolver(config["solver"]);
    solver->apply();

    test.check(std::abs(solver->template param<double>("sum") - 3.0) < 1e-8)
      << "Discrete variation parameter did not sum up as expected!";
  }

  return test.exit();
}
