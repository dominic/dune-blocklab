#include"config.h"

#include<dune/blocklab/init.hh>
#include<dune/blocklab/blocks/error.hh>
#include<dune/blocklab/blocks/interpolation.hh>
#include<dune/blocklab/blocks/test/providersetup.hh>
#include<dune/common/test/testsuite.hh>

#include<memory>
#include<tuple>


int main(int argc, char** argv)
{
  auto init = Dune::BlockLab::initBlockLab(argc, argv);
  Dune::TestSuite test;
  YAML::Node config;

  config = YAML::Load(
    "solver:\n"
    "  blocks:\n"
    "    -\n"
    "      _type: interpolation\n"
    "      functions: x * y\n"
    "    -\n"
    "      _type: error\n"
    "      analytic: 0.0"
  );

  auto ctx = structured_ug2_p2fem(init.helper, config);
  ctx.template registerBlock<Dune::BlockLab::InterpolationBlock>("interpolation");
  ctx.template registerBlock<Dune::BlockLab::DiscretizationErrorBlock>("error");

  auto solver = ctx.constructSolver(config["solver"]);
  solver->apply();

  // We (ab)use the error node to validate the integral of the interpolate solution
  test.check(std::abs(solver->template param<double>("error") - 1.0/3.0) < 1e-8)
     << "Integration of interpolated function x*y yielded wrong result.";

  return test.exit();
}
