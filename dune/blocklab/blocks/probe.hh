#ifndef DUNE_BLOCKLAB_BLOCKS_PROBE_HH
#define DUNE_BLOCKLAB_BLOCKS_PROBE_HH

#include<dune/blocklab/blocks/blockbase.hh>
#include<dune/blocklab/blocks/enableif.hh>
#include<dune/pdelab/common/functionutilities.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>

#include<memory>
#include<string>


namespace Dune::BlockLab {

  namespace impl
  {

    /** Switch between discrete grid function types for scalar and vector
     *  grid function spaces to reduce the amount of code duplication below.
     */
    template<typename GFS, typename V, bool scalar>
    struct DGFType
    {
      using type = Dune::PDELab::DiscreteGridFunction<GFS, V>;
    };

    template<typename GFS, typename V>
    struct DGFType<GFS, V, false>
    {
      using type = Dune::PDELab::VectorDiscreteGridFunction<GFS, V>;
    };

  }

  /** @brief A block that implements a function probe
   * 
   * This block evaluates a finite element function at a given global coordinate
   * and stores the result into the parameter system. Currently, this is limited
   * to scalar problems.
   */
  template<typename P, typename V, std::size_t i, typename enabled = disabled>
  class ProbeBlock
  /** @cond DISABLED_BLOCKS */
    : public DisabledBlock<P, V, i>
  {
    public:
    template<typename Context>
    ProbeBlock(Context& ctx, const YAML::Node& config)
      : DisabledBlock<P, V, i>(ctx, config)
    {}
  };

  template<typename P, typename V, std::size_t i>
  class ProbeBlock<P, V, i, enableBlock<isScalar<P, V, i>()>>
  /** @endcond */
    : public BlockBase<P, V, i>
  {
    public:
    //! The traits class for this block
    using Traits = BlockTraits<P, V, i>;
    using DGF = typename impl::DGFType<typename Traits::GridFunctionSpace, typename Traits::Vector, isScalar<P, V, i>()>::type;
    using Probe = Dune::PDELab::GridFunctionProbe<DGF>;

    /** @copydoc BlockBase::BlockBase(Context&, const YAML::Node&) */
    template<typename Context>
    ProbeBlock(Context& ctx, const YAML::Node& config)
      : BlockBase<P, V, i>(ctx, config)
      , name(config["name"].as<std::string>())
      , position(config["position"].as<typename Traits::GlobalCoordinate>())
    {}

    //! We use the virtual default destructor
    virtual ~ProbeBlock() = default;

    /** @copydoc BlockBase::blockData() */
    static std::vector<std::string> blockData()
    {
      // TODO: Validate coordinate
      auto data = ParentBlockBase<P, V>::blockData();
      data.push_back(
        "title: Function Probe                    \n"
        "category: pdelab                         \n"
        "schema:                                  \n"
        "  name:                                  \n"
        "    default: probe                       \n"
        "    type: string                         \n"
        "    meta:                                \n"
        "      title: Result Name                 \n"
        "  position:                              \n"
        "    required: true                       \n"
        "    type: list                           \n"
        "    maxlength: " + std::to_string(Traits::dim) + "\n"
        "    minlength: " + std::to_string(Traits::dim) + "\n"
        "    schema:                              \n"
        "      type: float                        \n"
        "    meta:                                \n"
        "      title: Position                    \n"
      );
      return data;
    }

    /** @copydoc BlockBase::setup() */
    virtual void setup() override
    {
      if (name != "")
        this->solver->introduce_parameter(name, typename DGF::Traits::RangeType(0.0));

      auto vector = this->solver->template getVector<i>();
      probe = std::make_shared<Probe>(vector->gridFunctionSpace().gridView(), position);
    }

    /** @copydoc BlockBase::apply() */
    virtual void apply() override
    {
      auto vector = this->solver->template getVector<i>();
      DGF dgf(vector->gridFunctionSpaceStorage(), vector);
      probe->setGridFunction(dgf);

      typename DGF::Traits::RangeType eval(0.0);
      probe->eval(eval);

      // Report the value of the probe into the solvers parameter system
      if (name != "")
        this->solver->update_parameter(name, eval);

      this->logEvent("Probe " + name + ": " + std::to_string(eval));
    }

    private:
    std::string name;
    typename Traits::GlobalCoordinate position;
    std::shared_ptr<Probe> probe;
  };

} // namespace Dune::BlockLab

#endif
