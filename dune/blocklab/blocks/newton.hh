#ifndef DUNE_BLOCKLAB_BLOCKS_NEWTON_HH
#define DUNE_BLOCKLAB_BLOCKS_NEWTON_HH

#include<dune/blocklab/blocks/blockbase.hh>
#include<dune/blocklab/operators/virtualinterface.hh>
#include<dune/common/parametertree.hh>
#include<dune/pdelab/backend/istl.hh>
#include<dune/pdelab/gridoperator/gridoperator.hh>
#include<dune/pdelab/solver/newton.hh>

#include<memory>

namespace Dune::BlockLab {

  /** @brief A block that applies a Newton solver
   * 
   * This block applies a Newton solver. The linear solver within is currently limited to a
   * direct UMFPack solver, but this is not really a technical limitation,
   * more solvers will be available in the near future.
   * 
   * The operator for the Newton solver is provided as a subblock named @a operator.
   */

  template<typename P, typename V, std::size_t i>
  class NewtonSolverBlock
    : public NamedParentBlockBase<P, V>
  {
    public:
    //! The traits class for this block
    using Traits = BlockTraits<P, V, i>;

    using VirtLocalOperator = AbstractLocalOperatorInterface<typename Traits::GridFunctionSpace>;
    using GridOperator = Dune::PDELab::GridOperator<typename Traits::GridFunctionSpace,
                                                    typename Traits::GridFunctionSpace,
                                                    VirtLocalOperator,
                                                    Dune::PDELab::ISTL::BCRSMatrixBackend<>,
                                                    typename Traits::ctype,
                                                    typename Traits::Range,
                                                    typename Traits::Range,
                                                    typename Traits::ConstraintsContainer,
                                                    typename Traits::ConstraintsContainer>;

    using LinearSolver = Dune::PDELab::ISTLBackend_SEQ_UMFPack;
    using NewtonSolver = Dune::PDELab::NewtonMethod<GridOperator, LinearSolver>;

    /** @copydoc AbstractBlockBase::AbstractBlockBase(Context&, const YAML::Node&) */
    template<typename Context>
    NewtonSolverBlock(Context& ctx, const YAML::Node& config)
      : NamedParentBlockBase<P, V>(ctx, config, {"operator"})
      , config(ymlToParameterTree(config))
    {}

    //! We use the virtual default destructor
    virtual ~NewtonSolverBlock() = default;

    /** @copydoc NamedParentBlockBase::blockData() */
    static std::vector<std::string> blockData()
    {
      // TODO: Write a schema for all those options Newton itself accepts
      auto data = BlockBase<P, V, i>::blockData();
      data.push_back(
        "title: Newton Solver                        \n"
        "category: pdelab                            \n"
        "schema:                                     \n"
        "  operator:                                 \n"
        "    type: dict                              \n"
        "    minlength: 1                            \n"
        "    maxlength: 1                            \n"
        "    meta:                                   \n"
        "      title: Operator                       \n"
        "      subtype: blocks                       \n"
      );
      return data;
    }

    /** @copydoc NamedParentBlockBase::updateParameter(std::string, typename Traits::Parameter) */
    virtual void update_parameter(std::string name, typename Traits::Parameter param) override
    {
      this->recursive_update_parameter(name, param);
      if (name == this->getChildBlock("operator")->getBlockName())
      {
        // The operator changed - rebuild the Newton solver object!
        auto vector = this->solver->template getVector<i>();
        auto cc = this->solver->template getConstraintsContainer<i>();
        auto gfs = vector->gridFunctionSpaceStorage();
        auto localoperator = this->solver->template param<std::shared_ptr<VirtLocalOperator>>(this->getChildBlock("operator")->getBlockName());
        Dune::PDELab::ISTL::BCRSMatrixBackend<> mb(21);
        gridoperator = std::make_shared<GridOperator>(*gfs, *cc, *gfs, *cc, *localoperator, mb);
        linearsolver = std::make_shared<LinearSolver>(0);
        newton = std::make_shared<NewtonSolver>(*gridoperator, *linearsolver, config);
        newton->setVerbosityLevel(2);
      }
    }

    /** @copydoc NamedParentBlockBase::apply() */
    virtual void apply() override
    {
      this->recursive_apply();
      this->logEvent("Applying Newton Solver!");
      newton->apply(*(this->solver->template getVector<i>()));
    }

    protected:
    Dune::ParameterTree config;
    std::shared_ptr<LinearSolver> linearsolver;
    std::shared_ptr<GridOperator> gridoperator;
    std::shared_ptr<NewtonSolver> newton;
  };

} // namespace Dune::BlockLab

#endif
