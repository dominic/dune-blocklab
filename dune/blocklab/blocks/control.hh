#ifndef DUNE_BLOCKLAB_BLOCKS_CONTROL_HH
#define DUNE_BLOCKLAB_BLOCKS_CONTROL_HH

#include<dune/blocklab/blocks/blockbase.hh>

namespace Dune::BlockLab {

  /** @brief A block that repeats a sequence of child blocks */
  template<typename P, typename V>
  class RepeatBlock
    : public ParentBlockBase<P, V>
  {
    public:
    /** @copydoc ParentBlockBase::ParentBlockBase(Context&, const YAML::Node&) */
    template<typename Context>
    RepeatBlock(Context& ctx, const YAML::Node& config)
      : ParentBlockBase<P, V>(ctx, config)
      , repeats(config["iterations"].as<int>())
    {}

    //! We use the virtual default destructor
    virtual ~RepeatBlock() = default;

    /** @copydoc ParentBlockBase::blockData() */
    static std::vector<std::string> blockData()
    {
      auto data = ParentBlockBase<P, V>::blockData();
      data.push_back(
        "title: Repeat Block Sequence             \n"
        "category: control                        \n"
        "schema:                                  \n"
        "  iterations:                            \n"
        "    default: 2                           \n"
        "    type: integer                        \n"
        "    meta:                                \n"
        "      title: Number of Iterations        \n"
      );
      return data;
    }

    /** @copydoc ParentBlockBase::apply() */
    virtual void apply() override
    {
      for(int i=0; i<repeats; ++i)
        this->recursive_apply();
    }

    private:
    int repeats;
  };

} // namespace Dune::BlockLab

#endif
