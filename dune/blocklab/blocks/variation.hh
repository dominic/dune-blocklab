#ifndef DUNE_BLOCKLAB_BLOCKS_VARIATION_HH
#define DUNE_BLOCKLAB_BLOCKS_VARIATION_HH

#include<dune/blocklab/blocks/blockbase.hh>
#include<dune/blocklab/construction/dataparser.hh>

#include<string>


namespace Dune::BlockLab {

  /** @brief A block that introduces a parameter and varies it continuously
   * 
   * This block introduces a parameter into the block solver parameter system.
   * It then repeats the application of all of its child blocks until a stopping
   * criterion is met. Currently this can be used to increase/decrease a floating
   * point parameter until a threshold is hit.
   */
  template<typename P, typename V>
  class ContinuousVariationBlock
    : public ParentBlockBase<P, V>
  {
    public:
    //! The traits class for thi block
    using Traits = BlockTraits<P, V>;

    /** @copydoc ParentBlockBase::ParentBlockBase(Context&, const YAML::Node&) */
    template<typename Context>
    ContinuousVariationBlock(Context& ctx, const YAML::Node& config)
      : ParentBlockBase<P, V>(ctx, config)
      , name(config["name"].as<std::string>())
      , iterations(config["iterations"].as<int>())
      , start(config["start"].as<double>())
      , end(config["end"].as<double>())
    {}

    //! We use the virtual default destructor
    virtual ~ContinuousVariationBlock() = default;

    /** @copydoc ParentBlockBase::blockData() */
    static std::vector<std::string> blockData()
    {
      auto data = ParentBlockBase<P, V>::blockData();
      data.push_back(
        "title: Continuous Parameter Variation    \n"
        "category: control                        \n"
        "schema:                                  \n"
        "  name:                                  \n"
        "    type: string                         \n"
        "    default: cparam                      \n"
        "    meta:                                \n"
        "      title: Parameter Name              \n"
        "  iterations:                            \n"
        "    type: integer                        \n"
        "    default: 2                           \n"
        "    meta:                                \n"
        "      title: Number of Iterations        \n"
        "  start:                                 \n"
        "    type: float                          \n"
        "    default: 0.0                         \n"
        "    meta:                                \n"
        "      title: Start Parameter             \n"
        "  end:                                   \n"
        "    type: float                          \n"
        "    default: 1.0                         \n"
        "    meta:                                \n"
        "      title: End Parameter               \n"
      );
      return data;
    }

    /** @copydoc ParentBlockBase::setup() */
    virtual void setup() override
    {
      this->solver->introduce_parameter(name, start);
      this->recursive_setup();
    }

    /** @copydoc ParentBlockBase::apply() */
    virtual void apply() override
    {
      double val = start;
      for (int i=0; i<iterations; ++i)
      {
        val += (end - start) / iterations;
        this->solver->update_parameter(name, val);
        this->recursive_apply();
      }
    }

    private:
    std::string name;
    int iterations;
    double start, end;
  };


  /** @brief A block that introduces a parameter and varies it discretely
   * 
   * This block introduces a parameter into the block solver parameter system.
   * It then applies all of its child blocks once for all of the specified
   * parameter values.
   */
  template<typename P, typename V>
  class DiscreteVariationBlock
    : public ParentBlockBase<P, V>
  {
    public:
    //! The traits class for this block
    using Traits = BlockTraits<P, V>;

    /** @copydoc ParentBlockBase::ParentBlockBase(Context&, const YAML::Node&) */
    template<typename Context>
    DiscreteVariationBlock(Context& ctx, const YAML::Node& config)
      : ParentBlockBase<P,V>(ctx, config)
      , name(config["name"].as<std::string>())
      , values(parse_parameter_list<typename Traits::Parameter>(config))
    {}

    //! We use the virtual default destructor
    virtual ~DiscreteVariationBlock() = default;

    /** @copydoc ParentBlockBase::blockData() */
    static std::vector<std::string> blockData()
    {
      // TODO: Validate comma separated list of something
      auto data = ParentBlockBase<P, V>::blockData();
      data.push_back(
        "title: Discrete Parameter Variation      \n"
        "category: control                        \n"
        "schema:                                  \n"
        "  name:                                  \n"
        "    type: string                         \n"
        "    default: dparam                      \n"
        "    meta:                                \n"
        "      title: Parameter Name              \n"
        "  datatype:                              \n"
        "    type: string                         \n"
        "    default: float                       \n"
        "    meta:                                \n"
        "      title: Parameter Type              \n"
        "  values:                                \n"
        "    type: string                         \n"
        "    required: true                       \n"
        "    meta:                                \n"
        "      title: Value List                  \n"
      );
      return data;
    }

    /** @copydoc ParentBlockBase::setup() */
    virtual void setup() override
    {
      this->solver->introduce_parameter(name, values[0]);
      this->recursive_setup();
    }

    /** @copydoc ParentBlockBase::apply() */
    virtual void apply() override
    {
      for (auto val: values)
      {
        this->solver->update_parameter(name, val);
        this->recursive_apply();
      }
    }

    private:
    std::string name;
    std::vector<typename Traits::Parameter> values;
  };

} // namespace Dune::BlockLab

#endif
