#ifndef DUNE_BLOCKLAB_BLOCKS_ADAPTIVITY_HH
#define DUNE_BLOCKLAB_BLOCKS_ADAPTIVITY_HH

#include<dune/blocklab/blocks/blockbase.hh>
#include<dune/blocklab/utilities/tuple.hh>
#include<dune/common/hybridutilities.hh>
#include<dune/pdelab/adaptivity/adaptivity.hh>

#include<tuple>

namespace Dune::BlockLab {

  /** @brief A block that adapts the grid */
  template<typename P, typename V>
  class AdaptivityBlock
    : public ParentBlockBase<P, V>
  {
    public:
    //! The traits class for this block
    using Traits = BlockTraits<P, V>;

    /** @copydoc ParentBlockBase::ParentBlockBase(Context&, const YAML::Node&) */
    template<typename Context>
    AdaptivityBlock(Context& ctx, const YAML::Node& config)
      : ParentBlockBase<P, V>(ctx, config)
    {}

    //! We use the virtual default destructor
    virtual ~AdaptivityBlock() = default;

    /** @copydoc ParentBlockBase::setup() */
    virtual void setup() override
    {
      // Introduce a parameter that notifies steps that the grid has changed.
      // The actual type and value are not particularly relevant: We simply
      // encode that the grid has undergone adaptation. More importantly each
      // update of this parameter can be used as a notification event.
      this->solver->template introduce_parameter<bool>("adapted", false);

      this->recursive_setup();
    }

    /** @copydoc ParentBlockBase::apply() */
    virtual void apply() override
    {
      // TODO: Here, we should check that the grid is unmarked.
      //       All marking should be done by substeps of this one.

      // Recursively call all substeps. These are expected to mark cells for refinement.
      this->recursive_apply();

      this->logEvent("Adapting the grid...");

      /**
       * The following code is horribly complicated. So it might deserve some remarks:
       *
       * * This is necessary because all vectors to be adapted need to be passed to the
       *   `adaptGrid` function in one go.
       * *  PDELab requires a non-const reference to the grid function space, but our way
       *    of not redundantly carrying around gfs's gives us only shared pointers to `const GFS`.
       *    We solve this by casting away the const...
       * *  Another thing that bothers me, where I do not understand PDELab semantics enough:
       *    If two of the vectors in V... use the same grid function space, is it okay to pass
       *    that gfs twice into adapt or do I actually need to group my vectors by gfs object???
       *    I doubt I will ever succeed in doing that in meta-programming, I would much rather
       *    duplicate and invade the PDELab adaptivity interface.
       * *  The `4` below hardcodes the integration order. It should of course be configurable in
       *    some way, but I will not target that before I actually think about coarsening.
       */
      auto transfer = make_tuple_from_indices<std::tuple_size_v<V>>(
        [this](auto i)
        {
          return Dune::PDELab::transferSolutions(
            *const_cast<typename std::remove_const<typename std::tuple_element_t<i, V>::GridFunctionSpace>::type*>(this->solver->template getVector<i>()->gridFunctionSpaceStorage().get()),
            4,
			*(this->solver->template getVector<i>())
          );
        });

      // TODO The fact that we extract the grid from index 0 limits our approach to one grid
      //      which is kind of reasonable, but a limitation.
      std::apply(
        [this](auto... v)
        {
          Dune::PDELab::adaptGrid(
            *(this->solver->template getGrid<0>()),
            v...
          );
        },
        transfer
      );

      // Notify other solver steps that the grid has been adapted
      // They might need to rebuild some data structures.
      this->solver->update_parameter("adapted", true);
    }
  };

} // namespace Dune::BlockLab

#endif
