#ifndef DUNE_BLOCKLAB_BLOCKS_BLOCKTRAITS_HH
#define DUNE_BLOCKLAB_BLOCKS_BLOCKTRAITS_HH

#include<tuple>


namespace Dune::BlockLab {

  // The forward declaration of the BlockSolver class
  template<typename P, typename V>
  class BlockSolver;

  // The forward declaration of the abstract block base
  template<typename P, typename V>
  class AbstractBlockBase;

  /** @brief The traits class for a solver block
   * 
   * This class is a collection of types that you might want to use
   * when implementing a solver block. You should take all your type
   * information from this class.
   */
  template<typename P, typename V, std::size_t i=0>
  class BlockTraits
  {
    public:
    //! The block solver type
    using Solver = BlockSolver<P, V>;

    //! The abstract block base type
    using BlockBase = AbstractBlockBase<P, V>;

    //! The finite element function container type
    using Vector = typename std::tuple_element<i, V>::type;

    //! The grid function space type
    using GridFunctionSpace = typename Vector::GridFunctionSpace;

    //! The grid view type
    using GridView = typename GridFunctionSpace::Traits::GridViewType;

    //! The grid dimension
    static constexpr int dim = GridView::dimension;

    //! The grid type
    using Grid = typename GridView::Traits::Grid;

    //! The entity set type
    using EntitySet = typename GridFunctionSpace::Traits::EntitySet;

    //! The floating point type used for coordinates
    using ctype = typename Grid::ctype;

    //! The type of an Entity in the Grid
    using Entity = typename GridView::template Codim<0>::Entity;

    //! The type used for global coordinates
    using GlobalCoordinate = typename Entity::Geometry::GlobalCoordinate;

    //! The types used for (element-)local coordinates
    using LocalCoordinate = typename Entity::Geometry::LocalCoordinate;

    //! The floating point type used for the field range
    using Range = typename Vector::field_type;

    //! The constraints container type
    using ConstraintsContainer = typename GridFunctionSpace::template ConstraintsContainer<Range>::Type;

    //! The vector backend type
    using VectorBackend = typename GridFunctionSpace::Traits::Backend;

    //! The variant type used for parameters in the solvers parameter system
    using Parameter = typename Solver::Parameter;
  };

} // namespace Dune::BlockLab

#endif
