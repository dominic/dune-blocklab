#ifndef DUNE_BLOCKLAB_BLOCKS_ENABLEIF_HH
#define DUNE_BLOCKLAB_BLOCKS_ENABLEIF_HH

/** Some helpers to use SFINAE on blocks such that they are only
 *  available if the vector types fulfill some conditions.
 */

#include<dune/blocklab/blocks/blocktraits.hh>
#include<dune/typetree/utility.hh>

#include<type_traits>


namespace Dune::BlockLab {

  /** @brief The default type to use in Blocklab's enable_if.
   *
   * When disabling a block according to the @ref blocks_disable guide,
   * the block class should inherit from the @ref DisableBlock base class
   * and use this type as the default argument for the enable template parameter.
   *
   * @ingroup blocks_disable
   */
  using disabled = void;


  /** @brief A helper for Blocklab's enable_if mechanism
   *
   * When disabling a block according to the @ref blocks_disable guide,
   * any template specialization that activates the block should use this
   * as its enable template parameter
   *
   * @tparam b a condition when to enable the block
   *
   * @ingroup blocks_disable
   */
  template<bool b>
  using enableBlock = std::enable_if_t<b, void>;

  /** @brief A predicate that is true if and only if a finite element is scalar
   *
   * @tparam P The @c std::tuple of additional types in the parameter system
   * @tparam V The @c std::tuple of finite element containers used in the block solver
   * @tparam i The index in the tuple of vector containers that this block operates on
   *
   * @ingroup blocks_disable
   */
  template<typename P, typename V, std::size_t i>
  constexpr bool isScalar()
  {
    return Dune::TypeTree::TreeInfo<typename BlockTraits<P, V, i>::GridFunctionSpace>::leafCount == 1;
  }

  /** @brief A predicate for vector finite elements
   *
   * This predicate is true if and only if the number of children in the finite space
   * matches the dimension of the grid.
   *
   * @tparam P The @c std::tuple of additional types in the parameter system
   * @tparam V The @c std::tuple of finite element containers used in the block solver
   * @tparam i The index in the tuple of vector containers that this block operates on
   *
   * @ingroup blocks_disable
   */
  template<typename P, typename V, std::size_t i>
  constexpr bool isDimPower()
  {
    return Dune::TypeTree::TreeInfo<typename BlockTraits<P, V, i>::GridFunctionSpace>::leafCount == BlockTraits<P, V, i>::dim;
  }

  /** @brief A predicate that is true if and only if the grid is two-dimensional
   *
   * @tparam P The @c std::tuple of additional types in the parameter system
   * @tparam V The @c std::tuple of finite element containers used in the block solver
   * @tparam i The index in the tuple of vector containers that this block operates on
   *
   * @ingroup blocks_disable
   */
  template<typename P, typename V, std::size_t i>
  constexpr bool is2D()
  {
    return BlockTraits<P, V, i>::dim == 2;
  }

  /** @brief A predicate that is true if and only if the grid is three-dimensional
   *
   * @tparam P The @c std::tuple of additional types in the parameter system
   * @tparam V The @c std::tuple of finite element containers used in the block solver
   * @tparam i The index in the tuple of vector containers that this block operates on
   *
   * @ingroup blocks_disable
   */
  template<typename P, typename V, std::size_t i>
  constexpr bool is3D()
  {
    return BlockTraits<P, V, i>::dim == 3;
  }


  /** @brief A predicate that requires additional vectors after this one
   *
   * @tparam P The @c std::tuple of additional types in the parameter system
   * @tparam V The @c std::tuple of finite element containers used in the block solver
   * @tparam i The index in the tuple of vector containers that this block operates on
   * @tparam additional The number of additional vectors required after this one
   *
   * This can be used if a block operates on several consecutive
   * vectors. In that case it is important that the block template
   * is not instantiated for the last elements of the vector tuple.
   *
   * @ingroup blocks_disable
   */
  template<typename P, typename V, std::size_t i, std::size_t additional>
  constexpr bool accessesAdditionalVectors()
  {
    return i + additional < std::tuple_size_v<V>;
  }

} // namespace Dune::BlockLab

#endif
