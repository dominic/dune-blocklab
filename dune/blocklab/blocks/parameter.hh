#ifndef DUNE_BLOCKLAB_BLOCKS_PARAMETER_HH
#define DUNE_BLOCKLAB_BLOCKS_PARAMETER_HH

#include<dune/blocklab/blocks/blockbase.hh>
#include<dune/blocklab/construction/dataparser.hh>

#include<memory>

namespace Dune::BlockLab {

  /** @brief A block that injects a parameter into the block solver parameter system
   * 
   * This introduces a new parameter into the parameter system. Such parameter can be
   * accessed from all blocks and even be used in arithmetic expressions e.g. for interpolation.
   */
  template<typename P, typename V>
  class ParameterBlock
    : public BlockBase<P, V>
  {
    public:
    //! The traits class for this block
    using Traits = BlockTraits<P, V>;

    /** @copydoc BlockBase::BlockBase(Context&, const YAML::Node&) */
    template<typename Context>
    ParameterBlock(Context& ctx, const YAML::Node& config)
      : BlockBase<P, V>(ctx, config)
      , name(config["name"].as<std::string>())
      , param(parse_parameter<typename Traits::Parameter>(config))
    {}

    //! We use the virtual default destructor
    virtual ~ParameterBlock() = default;

    /** @copydoc BlockBase::blockData() */
    static std::vector<std::string> blockData()
    {
      // TODO: Validate the (datatype, value) pair correctly
      //       By introducing a validator for Parameter (I guess)
      auto data = BlockBase<P, V>::blockData();
      data.push_back(
        "title: Parameter Setup                      \n"
        "category: control                           \n"
        "schema:                                     \n"
        "  name:                                     \n"
        "    type: string                            \n"
        "    default: param                          \n"
        "    meta:                                   \n"
        "      title: Parameter Name                 \n"
        "  datatype:                                 \n"
        "    type: string                            \n"
        "    default: float                          \n"
        "    meta:                                   \n"
        "      title: Parameter Type                 \n"
        "  value:                                    \n"
        "    type: string                            \n"
        "    required: true                          \n"
        "    meta:                                   \n"
        "      title: Parameter Value                \n"
      );

      return data;
    }

    /** @copydoc BlockBase::setup() */
    virtual void setup() override
    {
      this->solver->introduce_parameter(name, param);
    }

    private:
    std::string name;
    typename Traits::Parameter param;
  };

} // namespace Dune::BlockLab

#endif
