#ifndef DUNE_BLOCKLAB_BLOCKS_INSTATIONARY_HH
#define DUNE_BLOCKLAB_BLOCKS_INSTATIONARY_HH

#include<dune/blocklab/blocks/blockbase.hh>

#include<memory>

namespace Dune::BlockLab {

  /** @brief The time stepping block for instationary problems
   * 
   * This block controls the time-stepping in instationary problems.
   * It holds a number of subblocks each of which is applied at each
   * time step. This block introduces the variables @a time and @a timestep
   * into the parameter system.
   */
  template<typename P, typename V>
  class TimestepperBlock
    : public ParentBlockBase<P, V>
  {
    public:
    //! The traits class for this block
    using Traits = BlockTraits<P, V>;

    /** @copydoc ParentBlockBase::ParentBlockBase(Context&, const YAML::Node&) */
    template<typename Context>
    TimestepperBlock(Context& ctx, const YAML::Node& config)
      : ParentBlockBase<P, V>(ctx, config)
      , dt(config["timestep"].as<double>())
      , Tstart(config["starttime"].as<double>())
      , Tend(config["endtime"].as<double>())
    {}

    //! We use the virtual default destructor
    virtual ~TimestepperBlock() = default;

    /** @copydoc ParentBlockBase::blockData() */
    static std::vector<std::string> blockData()
    {
      auto data = ParentBlockBase<P, V>::blockData();
      data.push_back(
        "title: Time Stepping Loop                   \n"
        "category: pdelab                            \n"
        "schema:                                     \n"
        "  timestep:                                 \n"
        "    type: float                             \n"
        "    default: 0.1                            \n"
        "    meta:                                   \n"
        "      title: Starting Timestep              \n"
        "  starttime:                                \n"
        "    type: float                             \n"
        "    required: true                          \n"
        "    default: 0.0                            \n"
        "    meta:                                   \n"
        "      title: Start Time                     \n"
        "  endtime:                                  \n"
        "    type: float                             \n"
        "    required: true                          \n"
        "    default: 1.0                            \n"
        "    meta:                                   \n"
        "      title: End Time                       \n"
      );
      return data;
    }

    /** @copydoc ParentBlockBase::setup() */
    virtual void setup() override
    {
      this->solver->introduce_parameter("time", Tstart);
      this->solver->introduce_parameter("timestep", dt);

      this->recursive_setup();
    }

    /** @copydoc ParentBlockBase::apply() */
    virtual void apply() override
    {
      this->logEvent("Starting time stepping loop");
      double time = Tstart;
      while (time < Tend - 1e-8)
      {
        this->update_parameter("time", time);
        this->update_parameter("timestep", dt);
        this->logEvent("Performing time step " + std::to_string(time) + " -> " + std::to_string(time + dt) + " with " + std::to_string(this->blocks.size()) + " steps");

        // Apply the solver
        this->recursive_apply();

        time += dt;
      }
    }

    private:
    double dt;
    double Tstart;
    double Tend;
  };

} // namespace Dune::BlockLab

#endif
