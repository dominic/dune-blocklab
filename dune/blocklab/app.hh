#ifndef DUNE_BLOCKLAB_APP_HH
#define DUNE_BLOCKLAB_APP_HH

/** A convenience header for application tools from BlockLab */

#include<dune/blocklab/app/appclass.hh>
#include<dune/blocklab/app/frontend.hh>
#include<dune/blocklab/app/help.hh>
#include<dune/blocklab/app/run.hh>
#include<dune/blocklab/construction/registry.hh>

#endif