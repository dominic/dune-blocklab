#ifndef DUNE_BLOCKLAB_OPERATORS_CONVECTIONDIFFUSIONFEM_HH
#define DUNE_BLOCKLAB_OPERATORS_CONVECTIONDIFFUSIONFEM_HH

/** Define a solver block that adds PDELab's convection diffusion
 *  FEM operator to the block solver parameter system.
 */

#include<dune/blocklab/blocks/blockbase.hh>
#include<dune/blocklab/blocks/enableif.hh>
#include<dune/blocklab/construction/muparser.hh>
#include<dune/blocklab/operators/virtualinterface.hh>
#include<dune/pdelab/localoperator/convectiondiffusionfem.hh>
#include<dune/pdelab/localoperator/convectiondiffusionparameter.hh>

#include<memory>
#include<string>


namespace Dune::BlockLab {


  /** @brief A runtime implementation of @c Dune::PDELab::ConvectionDiffusionParameters
   *
   *  A parameter class that fulfills the interface required by PDELab's
   *  convection diffusion operator, but parser the actual implementation
   *  from the configuration at runtime.
   */
  template<typename GV, typename RF>
  class ConvectionDiffusionRuntimeParameters
  {
    public:
    using BCType = Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type;
    using Traits = Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF>;
    using RFT = typename Traits::RangeFieldType;
    using RT = typename Traits::RangeType;
    using E = typename Traits::ElementType;
    using ED = typename Traits::DomainType;
    using I = typename Traits::IntersectionType;
    using ID = typename Traits::IntersectionDomainType;
    using PT = typename Traits::PermTensorType;

    ConvectionDiffusionRuntimeParameters(const YAML::Node& config)
      : bctype_func(muparser_callable<BCType(I, ID)>(config["bctype"].as<std::string>()))
      , c_func(muparser_callable<RFT(E, ED)>(config["c"].as<std::string>()))
      , f_func(muparser_callable<RFT(E, ED)>(config["f"].as<std::string>()))
      , j_func(muparser_callable<RFT(I, ID)>(config["j"].as<std::string>()))
      , o_func(muparser_callable<RFT(I, ID)>(config["o"].as<std::string>()))
    {}

    // we cannot guarantee this statically, so we pay the price of always evaluating
    // at every quadrature point
    static constexpr bool permeabilityIsConstantPerCell()
    {
      return false;
    }

    // TODO Implement this!
    PT A (const E& e, const ED& x) const
    {
      PT I;
      for (std::size_t i=0; i<Traits::dimDomain; i++)
        for (std::size_t j=0; j<Traits::dimDomain; j++)
          I[i][j] = (i==j) ? 1 : 0;
      return I;
    }

    // TODO Implement this!
    RT b (const E& e, const ED& x) const
    {
      return RT(0.0);
    }

    //! reaction term
    RFT c (const E& e, const ED& x) const
    {
      return c_func(e, x);
    }

    //! source term
    RFT f (const E& e, const ED& x) const
    {
      return f_func(e, x);
    }

    //! boundary condition type function
    BCType bctype (const I& is, const ID& x) const
    {
      return bctype_func(is, x);
    }

    //! Neumann boundary condition
    RFT j (const I& is, const ID& x) const
    {
      return j_func(is, x);
    }

    //! outflow boundary condition
    RFT o (const I& is, const ID& x) const
    {
      return o_func(is, x);
    }

    private:
    std::function<BCType(I, ID)> bctype_func;
    std::function<RFT(E, ED)> c_func;
    std::function<RFT(E, ED)> f_func;
    std::function<RFT(I, ID)> j_func;
    std::function<RFT(I, ID)> o_func;
  };

  /** @brief A block that construct a convection-diffusion operator
   *
   * This block constructs a convection-diffusion operator from PDELab
   * and injects it into the solver's parameter system.
   */
  template<typename P, typename V, std::size_t i, typename enable = disabled>
  class ConvectionDiffusionFEMBlock
  /** @cond DISABLED_BLOCKS */
    : public DisabledBlock<P, V, i>
  {
    public:
    template<typename Context>
    ConvectionDiffusionFEMBlock(Context& ctx, const YAML::Node& config)
      : DisabledBlock<P, V, i>(ctx, config)
    {}
  };

  template<typename P, typename V, std::size_t i>
  class ConvectionDiffusionFEMBlock<P, V, i, enableBlock<isScalar<P, V, i>()>>
  /** @endcond */
    : public BlockBase<P, V, i>
  {
    public:
    //! The traits class for this block
    using Traits = BlockTraits<P, V, i>;
    using ParameterInterface = ConvectionDiffusionRuntimeParameters<typename Traits::EntitySet, typename Traits::Range>;
    using AbstractOperator = AbstractLocalOperatorInterface<typename Traits::GridFunctionSpace>;
    using LocalOperator = Dune::PDELab::ConvectionDiffusionFEM<ParameterInterface, typename Traits::GridFunctionSpace::Traits::FiniteElementMap>;
    using WrappedOperator = VirtualizedLocalOperator<LocalOperator, typename Traits::GridFunctionSpace>;

    /** @copydoc BlockBase::BlockBase(Context&, const YAML::Node&) */
    template<typename Context>
    ConvectionDiffusionFEMBlock(Context& ctx, const YAML::Node& config)
      : BlockBase<P, V, i>(ctx, config)
      , params(std::make_shared<ParameterInterface>(config))
    {}

    //! We use the virtual default destructor
    virtual ~ConvectionDiffusionFEMBlock() = default;

    /** @copydoc BlockBase::blockData() */
    static std::vector<std::string> blockData()
    {
      auto data = BlockBase<P, V, i>::blockData();
      data.push_back(
        "title: Convection-Diffusion Operator                       \n"
        "category: operators                                        \n"
        "schema:                                                    \n"
        "  bctype:                                                  \n"
        "    type: string                                           \n"
        "    default: 1                                             \n"
        "    meta:                                                  \n"
        "      title: 'Boundary Condition (0: Neumann, 1: Dirichlet)' \n"
        "  c:                                                       \n"
        "    type: string                                           \n"
        "    default: 0                                             \n"
        "    meta:                                                  \n"
        "      title: Concentration term                            \n"
        "  f:                                                       \n"
        "    type: string                                           \n"
        "    default: 0                                             \n"
        "    meta:                                                  \n"
        "      title: Source Term                                   \n"
        "  j:                                                       \n"
        "    type: string                                           \n"
        "    default: 0                                             \n"
        "    meta:                                                  \n"
        "      title: Neumann Flux                                  \n"
        "  o:                                                       \n"
        "    type: string                                           \n"
        "    default: 0                                             \n"
        "    meta:                                                  \n"
        "      title: Outflow Boundary Condition                    \n"
      );
      return data;
    }

    /** @copydoc BlockBase::setup() */
    virtual void setup() override
    {
      auto lop = std::make_shared<LocalOperator>(*params);
      auto wrapped = std::make_shared<WrappedOperator>(lop);
      this->solver->template introduce_parameter<std::shared_ptr<AbstractOperator>>(this->getBlockName(), wrapped);
    }

    private:
    std::shared_ptr<ParameterInterface> params;
  };

} // namespace Dune::BlockLab

#endif
