#ifndef DUNE_BLOCKLAB_EVENTS_DISTRIBUTOR_HH
#define DUNE_BLOCKLAB_EVENTS_DISTRIBUTOR_HH

#include<dune/blocklab/events/base.hh>

#include<memory>
#include<vector>


namespace Dune::BlockLab {

  /** @brief The event distributor that handles BlockLab events
   *
   * This is implemented using the singleton pattern and allows
   * to register new listeners or distribute events
   */
  class EventDistributor
  {
    public:

    /** @brief Singleton access to the distributor instance */
    static EventDistributor& instance()
    {
      static EventDistributor instance;
      return instance;
    }

    //! Make sure that this cannot be copied
    EventDistributor(const EventDistributor&) = delete;

    //! Make sure that this cannot be assigned
    void operator=(const EventDistributor&) = delete;

    /** @brief Distribute an event
     *
     * @param event The event to distribute
     *
     * The event will be distributed to all registered listeners.
     * This will trigger their @c receive method.
     */
    void distribute(const std::shared_ptr<EventBase>& event)
    {
      for(auto listener : listeners)
        listener->receive(event);
    }

    /** @brief Register a listener instance
     *
     * @param listener The listener instance to register
     *
     * After registration, any triggered event will be received by the listener
     */
    void registerListener(const std::shared_ptr<ListenerBase>& listener)
    {
      listeners.push_back(listener);
    }

    private:
    //! Make sure this cannot be constructed from the outside world
    EventDistributor()
    {}

    // The registered listeners
    std::vector<std::shared_ptr<ListenerBase>> listeners;
  };

  /** @brief A free function for listener registration
   *
   * @param listener The listener instance to register
   */
  void registerListener(const std::shared_ptr<ListenerBase>& listener)
  {
    EventDistributor::instance().registerListener(listener);
  }

  /** @brief A free function for an event trigger
   *
   * @param event The event to trigger
   */
  void triggerEvent(const std::shared_ptr<EventBase>& event)
  {
    EventDistributor::instance().distribute(event);
  }

} // namespace Dune::BlockLab

#endif