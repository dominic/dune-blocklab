#ifndef DUNE_BLOCKLAB_EVENTS_FILEIO_HH
#define DUNE_BLOCKLAB_EVENTS_FILEIO_HH

#include<dune/blocklab/events/base.hh>


namespace Dune::BlockLab {

  /** @brief An event that signals that a file has been written
   * 
   * This signals to listeners that a file has been written to disk.
   */
  class FileIOEvent
    : public EventBase
    , public BlockEventMixin
  {
    public:
    /** @brief Construct a file output event
     * 
     * @tparam Block The type of the associated block (can be deduced)
     * @param block The block this event is associated with
     * @param filename The filename of the written file
     * @param fileidentifier Additional data about what file is written. Currently used in the frontend
     *                       to know which file output the file belongs to (will eventually go away).
     */
    template<typename Block>
    FileIOEvent(Block& block, const std::string& filename, const std::string& fileidentifier)
      : BlockEventMixin(block)
      , filename(filename)
      , fileidentifier(fileidentifier)
    {}

    //! We use the virtual default destructor
    virtual ~FileIOEvent() = default;

    /** @copydoc EventBase::getType() */
    virtual std::string getType() override
    {
      return "fileio";
    }

    /** @copydoc EventBase::getMessage() */
    virtual std::string getMessage() override
    {
      return "Block " + this->getBlockName() + " wrote file to disk: " + filename;
    }

    /** @copydoc EventBase::getData() */
    virtual std::string getData() override
    {
      return filename + ":" + fileidentifier;
    }

    private:
    std::string filename;
    std::string fileidentifier;
  };

  /** @brief Trigger a file output event
   * 
   * @tparam Block The type of the associated block (can be deduced)
   * @param block The block this event is associated with
   * @param filename The filename of the written file
   * @param fileidentifier Additional data about what file is written. Currently used in the frontend
   *                       to know which file output the file belongs to (will eventually go away).
   */
  template<typename Block>
  void triggerFileIOEvent(Block& block, std::string filename, std::string fileidentifier)
  {
    EventDistributor::instance().distribute(std::make_shared<FileIOEvent>(block, filename, fileidentifier));
  }

} // namespace Dune::BlockLab

#endif