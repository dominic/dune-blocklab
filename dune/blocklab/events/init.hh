#ifndef DUNE_BLOCKLAB_EVENTS_INIT_HH
#define DUNE_BLOCKLAB_EVENTS_INIT_HH

#include<dune/blocklab/events/base.hh>
#include<dune/blocklab/events/distributor.hh>
#include<dune/blocklab/events/http.hh>
#include<dune/blocklab/utilities/yaml.hh>
#include<dune/common/exceptions.hh>


namespace Dune::BlockLab {

  /** @brief A helper that constructs listeners from a configuration
   * 
   * @param config The configuration to construct listeners from
   * 
   * Web frontend applications need to modify the event listeners that are
   * registered in an application e.g. to enable HTTP request feedback from
   * the application. This helper function constructs BlockLab's built-in
   * listeners using a simple mechanism. In the future, this might be replaced
   * with a more complicated infrastructure that allows registration of
   * user-provided listeners.
   */
  void registerListenersFromConfig(const YAML::Node& config)
  {
    for(auto listener: config)
    {
      auto type = listener["type"].as<std::string>();
      if(type == "console")
        registerListener(std::make_shared<ConsoleListener>(listener));
      else if(type == "http")
        registerListener(std::make_shared<HTTPRequestListener>(listener));
      else
        DUNE_THROW(Dune::Exception, "Unknown listener type");
    }
  }

} // namespace Dune::BlockLab

#endif