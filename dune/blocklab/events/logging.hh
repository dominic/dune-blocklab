#ifndef DUNE_BLOCKLAB_EVENTS_LOGGING_HH
#define DUNE_BLOCKLAB_EVENTS_LOGGING_HH

#include<dune/blocklab/events/base.hh>
#include<dune/blocklab/events/distributor.hh>

#include<memory>
#include<string>


namespace Dune::BlockLab {

  /** @brief An event that simply contains a log message */
  class LogEvent
    : public EventBase
    , public LevelEventMixin
  {
    public:
    /** @brief Construct a log event
     * 
     * @param message The log message string
     * @param level The log level of this message
     */
    LogEvent(const std::string& message, LogLevel level)
      : LevelEventMixin(level)
      , message(message)
    {}

    /** @copydoc EventBase::getType() */
    virtual std::string getType() override
    {
      return "log";
    }

    /** @copydoc EventBase::getMessage() */
    virtual std::string getMessage() override
    {
      return message;
    }

    /** @copydoc EventBase::getData() */
    virtual std::string getData() override
    {
      return message;
    }

    private:
    std::string message;
  };

  /** @brief An event class for a log event associated with a block */
  class BlockLogEvent
    : public LogEvent
    , public BlockEventMixin
  {
    public:
    /** @brief Construct a log event associated with a block
     * 
     * @tparam Block The type of the block this event is associated with (can be deduced)
     * @param block The block this event is associated with
     * @param message The log message
     * @param level The log level of the message
     */
    template<typename Block>
    BlockLogEvent(Block& block, const std::string& message, LogLevel level = LogLevel::INFO)
      : LogEvent(message, level)
      , BlockEventMixin(block)
    {}
  };

  /** @brief Trigger a log event
   * 
   * @param message The log message string
   * @param level The log level of this message
   */
  void logEvent(const std::string& message, LogLevel level = LogLevel::INFO)
  {
    EventDistributor::instance().distribute(std::make_shared<LogEvent>(message, level));
  }

} // namespace Dune::BlockLab

#endif