#ifndef DUNE_BLOCKLAB_EVENTS_HTTP_HH
#define DUNE_BLOCKLAB_EVENTS_HTTP_HH

/** A listener that receives events and sends them as HTTP requests
 *  to a server. This is used to have the C++ executable communicate
 *  to the web frontend.
 */

#include<dune/blocklab/events/base.hh>
#include<dune/blocklab/utilities/python.hh>
#include<dune/blocklab/utilities/yaml.hh>
#include<dune/common/parametertree.hh>

#include<algorithm>
#include<memory>
#include<string>
#include<vector>


namespace Dune::BlockLab {

  /** @brief A listener that fires HTTP requests when events are triggered
   * 
   * This is used to send server-side events to the web frontend.
   */
  class HTTPRequestListener
    : public ListenerBase
    , public PythonContext
  {
    public:
    /** @brief Construct the HTTP listener from a configuration */
    HTTPRequestListener(const YAML::Node& config)
      : HTTPRequestListener(config["url"].as<std::string>("http://localhost:5000/events"))
    {}

    /** @brief Construct the HTTP listener given a server URL */
    HTTPRequestListener(const std::string& url)
      : PythonContext("listener")
      , url(url)
    {
      import("requests");
    }

    /** @copydoc ListenerBase::receive(std::shared_ptr<EventBase>) */
    virtual void receive(std::shared_ptr<EventBase> event) override
    {
      auto type = event->getType();

      // If we are white-listing, but this is not on the list - skip
      if((!whiteList.empty()) && (std::find(whiteList.begin(), whiteList.end(), type) == whiteList.end()))
        return;

      // If this is black-listed - skip
      if(std::find(blackList.begin(), blackList.end(), type) != blackList.end())
        return;
    
      // We want this - lets fire a request!
      run("data = {}");
      run("data['type'] = '" + type + "'");
      run("data['data'] = '" + event->getData() + "'");

      auto blockevent = std::dynamic_pointer_cast<BlockEventMixin>(event);
      if(blockevent)
        run("data['block'] = '" + blockevent->getBlockName() + "'");

      // Send the request
      run("requests.post('" + url + "', json=data)");
    }

    private:
    std::string url;
    std::vector<std::string> whiteList;
    std::vector<std::string> blackList;
  };

} //namespace Dune::BlockLab

#endif