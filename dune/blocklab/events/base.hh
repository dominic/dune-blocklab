#ifndef DUNE_BLOCKLAB_EVENTS_BASE_HH
#define DUNE_BLOCKLAB_EVENTS_BASE_HH

#include<memory>
#include<string>


namespace Dune::BlockLab {

  /** @brief A log level definition for BlockLab */
  enum class LogLevel {
    UNDEFINED = -1,
    ERROR = 0,
    CRITICAL = 1,
    WARNING = 2,
    INFO = 3,
    DEBUG = 4
  };

  /** @brief The abstract base class for an event 
   * 
   * BlockLab uses an event system to encapsulate all communication of
   * the solver with the outside world. All events inherit from this
   * abstract base class. For the listening counterpart see @ref ListenerBase .
   */
  class EventBase
  {
    public:
    /** @brief A type identifier for this event 
     * 
     * This is a simple string that allows listeners to query the type
     * of an event without casting things around.
     */
    virtual std::string getType() = 0;

    /** @brief A string version of this events message
     *
     * For debugging purposes all events should carry a human-readable
     * version of their data. For logging events, this *is* the data.
     */
    virtual std::string getMessage() = 0;

    /** @brief The events data string
     * 
     * Events may hold an additional data string. How this string is to
     * be interpreted is a convention between event and listener implementations.
     */
    virtual std::string getData()
    {
      return {};
    }
  };

  /** @brief A mixin class if an event should have a log level associated with it
   * 
   * This mixin can be used for events that need log level information.
   * This is a mixin instead of a base class to avoid diamond inheritance.
   */
  class LevelEventMixin
  {
    public:
    /** @brief Construct the log level mixin given a log level
     * 
     * @param level The log level of this event
     */
    LevelEventMixin(LogLevel level)
      : level(level)
    {}

    //! get the log level of this event
    LogLevel getLevel()
    {
      return level;
    }

    protected:
    LogLevel level;
  };
  
  /** @brief A mixin class if an event is associated with a block
   * 
   * This mixin can be used for events that are triggered by a block
   * This is a mixin instead of a base class to avoid diamond inheritance.
   */
  class BlockEventMixin
  {
    public:
    /** @brief Construct the block mixing from a block
     * 
     * @tparam Block The type of the block (can be deduced)
     * @param block The block this event is associated with
     */
    template<typename Block>
    BlockEventMixin(Block& block)
      : blockname(block.getBlockName())
      , blocklevel(block.getLogLevel())
    {}

    //! Get the block name
    std::string getBlockName()
    {
      return blockname;
    }

    //! Get the block's defined log level
    LogLevel getBlockLevel()
    {
      return blocklevel;
    }

    protected:
    std::string blockname;
    LogLevel blocklevel;
  };

  /** @brief The abstract base class for listeners
   * 
   * The BlockLab event system can have any number of listeners
   * whose implementation decides how to react to certain events.
   * These must inherit from this base class. Actual distribution
   * of events is implemented by @ref EventDistributor
   */
  class ListenerBase
  {
    public:
    /** @brief Receive an event
     * 
     * This method will be called for any triggered event. The listener
     * implementation can then decided how (and if) to react.
     */
    virtual void receive(std::shared_ptr<EventBase> event) = 0;
  };

}

#endif