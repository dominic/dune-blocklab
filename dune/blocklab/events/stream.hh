#ifndef DUNE_BLOCKLAB_EVENTS_STREAM_HH
#define DUNE_BLOCKLAB_EVENTS_STREAM_HH

#include<dune/blocklab/events/base.hh>
#include<dune/blocklab/events/distributor.hh>
#include<dune/blocklab/utilities/yaml.hh>

#include<iostream>


namespace Dune::BlockLab {

  /** @brief A listener that outputs events to a C++ stream */
  template<typename Stream>
  class StreamListener
    : public ListenerBase
  {
    public:
    /** @brief Construct a listener object given a C++ stream
     * 
     * @tparam Stream The type of the C++ stream (can be deduced)
     * @param stream The C++ stream
     * @param level The log level that this stream prints
     */
    StreamListener(Stream& stream, LogLevel level = LogLevel::INFO)
      : stream(stream)
      , level(level)
    {}

    /** @copydoc ListenerBase::receive(std::shared_ptr<EventBase>) */
    virtual void receive(std::shared_ptr<EventBase> event) override
    {
      auto levelevent = std::dynamic_pointer_cast<LevelEventMixin>(event);
      // Not sure yet whether this by default should log events without loglevel
      if (!levelevent)
        return;

      std::string message = event->getMessage();
      auto blockevent = std::dynamic_pointer_cast<BlockEventMixin>(event);
      LogLevel blocklevel = LogLevel::UNDEFINED;
      if(blockevent)
      {
        message = "Block " + blockevent->getBlockName() + ": " + message;
        blocklevel = blockevent->getBlockLevel();
      }

      bool doSomething = true;
      if(levelevent)
      {
        LogLevel target = level;
        if(blocklevel != LogLevel::UNDEFINED)
          target = std::max(blocklevel, level);
        if(levelevent->getLevel() > target)
          doSomething = false;
      }

      if (doSomething)
        stream << message << std::endl;
    }

    private:
    Stream& stream;
    LogLevel level;
  };


  /** @brief A listener that output to std::cout */
  class ConsoleListener
    : public StreamListener<decltype(std::cout)>
  {
    public:
    /** @brief Construct a std::cout listener from a configuration 
     * 
     * @param config The configuration node to construct the listener from 
     */
    ConsoleListener(const YAML::Node& config)
      : ConsoleListener(config["loglevel"].as<LogLevel>(LogLevel::INFO))
    {}

    /** @brief Construct a std::cour listener given the log level
     * 
     * @param level The log level this listener prints
     */
    ConsoleListener(LogLevel level = LogLevel::INFO)
      : StreamListener<decltype(std::cout)>(std::cout, level)
    {}
  };
}

#endif