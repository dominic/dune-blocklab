#ifndef DUNE_BLOCKLAB_EVENTS_BLOCK_HH
#define DUNE_BLOCKLAB_EVENTS_BLOCK_HH

#include<dune/blocklab/events/base.hh>
#include<dune/blocklab/events/distributor.hh>

namespace Dune::BlockLab {

  /** @brief The definition of a block status */
  enum class BlockStatus {
      STARTED = 0,
      STOPPED = 1,
      SKIPPED = 2
  };

  namespace impl {

    std::string statusToString(BlockStatus status)
    {
      if(status == BlockStatus::STARTED)
        return "STARTED";
      else if(status == BlockStatus::STOPPED)
        return "STOPPED";
      else
        return "SKIPPED";
    }

  } // namespace impl

  /** @brief An event that notifies about a block's status
   * 
   * This is fired whenever a block changes its status
   */
  class BlockStatusEvent
    : public EventBase
    , public BlockEventMixin
  {
    public:
    /** @brief Construct the block status event 
     * 
     * @tparam Block The type of the block (can be deduced)
     * @param block The block whose status changes
     * @param status The new status of the Block
    */
    template<typename Block>
    BlockStatusEvent(Block& block, BlockStatus status)
      : BlockEventMixin(block)
      , status(status)
    {}

    //! We use the virtual default destructor
    virtual ~BlockStatusEvent() = default;

    /** @copydoc EventBase::getType() */
    virtual std::string getType() override
    {
      return "status";
    }

    /** @copydoc EventBase::getMessage() */
    virtual std::string getMessage() override
    {
      return "Block " + this->getBlockName() + " status changed: " + impl::statusToString(status);
    }

    /** @copydoc EventBase::getData() */
    virtual std::string getData() override
    {
      return impl::statusToString(status);
    }

    private:
    BlockStatus status;
  };

  /** @brief Trigger a block status event
   * 
   * @tparam Block The type of the block (can be deduced)
   * @param block The block whose status changes
   * @param status The new status of the Block
   */
  template<typename Block>
  void triggerBlockStatusEvent(Block& block, BlockStatus status)
  {
    EventDistributor::instance().distribute(std::make_shared<BlockStatusEvent>(block, status));
  }

  namespace impl {

    struct DummyBlock
    {
      const std::string& getBlockName() const
      {
        return name;
      }

      LogLevel getLogLevel() const
      {
        return LogLevel::INFO;
      }

      const std::string& name;
    };

  } // namespace impl

  /** @brief Trigger a block status event with the name of the block only
   * 
   * @param blockname The name of the block whose status changes
   * @param status The new status of the Block
   */
  void triggerBlockStatusEventForString(const std::string& blockname, BlockStatus status)
  {
    impl::DummyBlock b{blockname};
    triggerBlockStatusEvent(b, status);
  }

} // namespace Dune::BlockLab

#endif