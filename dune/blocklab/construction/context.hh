#ifndef DUNE_BLOCKLAB_CONSTRUCTION_CONTEXT_HH
#define DUNE_BLOCKLAB_CONSTRUCTION_CONTEXT_HH

#include<dune/blocklab/events/block.hh>
#include<dune/blocklab/blocks/blockbase.hh>
#include<dune/blocklab/solver.hh>
#include<dune/blocklab/utilities/enumerate.hh>
#include<dune/blocklab/utilities/stringsplit.hh>
#include<dune/blocklab/utilities/tuple.hh>
#include<dune/blocklab/utilities/yaml.hh>
#include<dune/common/exceptions.hh>
#include<dune/common/hybridutilities.hh>
#include<dune/common/parallel/mpihelper.hh>

#include<cerberus-cpp/validator.hh>

#include<array>
#include<functional>
#include<memory>
#include<tuple>

namespace Dune::BlockLab {

  /** @brief An object that manages the construction of a @ref BlockSolver instance
   *
   * @tparam UserParameters An @c std::tuple of types that are added to the solver parameter system
   * @tparam VectorProviders A variadic list of types of finite element container providers
   *
   * This object manages the construction of @ref BlockSolver instances. You can have
   * a look at its interface to see what type of information is available during block
   * construction - as each block will receive a reference to an object of this type.
   * 
   * @ingroup blocks_register
   */
  template<typename UserParameters, typename... VectorProviders>
  class ConstructionContext
  {
    // Construct a tuple of all vector types in the given providers
    using V = std::tuple<typename VectorProviders::Vector...>;

    // and a tuple of all parameter types
    using P = tuple_cat_t<UserParameters, typename VectorProviders::Parameter...>;

    public:

    /** @brief Construct the construction context
     * 
     * @param helper The MPI helper manages MPI for Dune
     * @param rootconfig The entire configuration tree
     * @param providers All the vector providers
     */
    ConstructionContext(Dune::MPIHelper& helper,
			const YAML::Node& rootconfig,
			std::shared_ptr<VectorProviders>... providers)
      : providers{providers...}
      , mpihelper(helper)
      , rootconfig(rootconfig)
    {}

    /** @brief Access the MPIHelper during block construction */
    const Dune::MPIHelper& getMPIHelper() const
    {
      return mpihelper;
    }

    /** @brief Access the root configuration during block construction */
    const YAML::Node& getRootConfig() const
    {
      return rootconfig;
    }

    /** @brief Access the solver object that is about to be constructed during block construction
     * 
     * I am more than unhappy with this being in the interface because the
     * solver interface cannot be actually used during construction. But it
     * happens that some parts (Muparser callbacks) need the pointer to the
     * solver during construction! I am now handing out a const version,
     * which should offer some protection against misuse.
     */
    std::shared_ptr<const BlockSolver<P, V>> getSolver()
    {
      return solver;
    }

    /** @brief Register a block in the context given its full type
     * 
     * @tparam Block The block type to register
     * @param identifier The name it is registered to
     * 
     * Usually, one does not call this signature with the explicit block type,
     * but a signature with a template template parameter. This is necessary
     * because the actual template parameters of the block base classes are 
     * usually only known within this context and not to the user
     */
    template<typename Block>
    void registerBlock(std::string identifier)
    {
      mapping[identifier] = [](auto& ctx, auto& p)
      {
        // Construct a schema from the given schema snippets
        YAML::Node schema;
        for(auto snippet : Block::blockData())
          for(auto item : YAML::Load(snippet)["schema"])
            schema[item.first] = item.second;

        // Validate the given data against this schema
        cerberus::Validator validator;
        validator.validate(p, schema);
        return std::make_shared<Block>(ctx, validator.getDocument());
      };
    }

    /** @brief Register a block in the context given its full type
     * 
     * @tparam Block The block class template name (without the template parameters!) to register
     * @param identifier The name it is registered to
     * 
     * This will automatically append the correct template parameters and register
     * a block using @ref registerBlock(std::string) . For those blocks that depend
     * on a finite element container, this will also loop over all finite element
     * containers known to the solver and add the resulting blocks individually.
     */
    template<template<typename, typename, std::size_t, typename...> typename Block>
    void registerBlock(std::string identifier)
    {
      Dune::Hybrid::forEach(Dune::Hybrid::integralRange(std::integral_constant<std::size_t, 0>{},
                                                        std::integral_constant<std::size_t, std::tuple_size<V>::value>{}),
        [this, identifier](auto i)
        {
          auto subident = "_" + identifier + "_" + std::to_string(i);
          this->template registerBlock<Block<P, V, i>>(subident);
        }
      );
    }
    
    /** @cond CONFUSING_DETAIL
     * 
     * It would be pretty confusing to have this show up in the Doxygen documentation as well
     */
    template<template<typename, typename, typename...> typename Block>
    void registerBlock(std::string identifier)
    {
      this->template registerBlock<Block<P, V>>(identifier);
    }
    /** @endcond */

    /** @brief Construct the @c BlockSolver object
     * 
     * @param config The configuration node to construct it from
     */
    std::shared_ptr<BlockSolver<P, V>> constructSolver(YAML::Node config)
    {
      if(!solver)
      {
        // Construct a name -> parameter mapping
        using ParameterVariant = typename BlockSolver<P, V>::Parameter;
        std::map<std::string, ParameterVariant> pmapping;
        std::apply([&pmapping](auto... p)
                   {
                     ( pmapping.merge(p->template createParameters<ParameterVariant>()), ...);
                   },
                   providers);

        // Fire some events concerning grid and vector generation
        auto gridblock = config["grid"]["_blockname"].as<std::string>("grid");
        triggerBlockStatusEventForString(gridblock, BlockStatus::STARTED);
        auto grids = std::apply([](auto... p){ return std::make_tuple(p->getGrid()...); }, providers);
        triggerBlockStatusEventForString(gridblock, BlockStatus::STOPPED);

        // Send start events for vectors
        for(auto vec: config["vectors"])
          triggerBlockStatusEventForString(vec.as<std::string>(), BlockStatus::STARTED);

        auto vectors = std::apply([](auto... p){ return std::make_tuple(p->getVector()...); }, providers);
        auto ccs = std::apply([](auto... p){ return std::make_tuple(p->getConstraintsContainer()...); }, providers);

        // Send stop events for vectors
        for(auto vec: config["vectors"])
          triggerBlockStatusEventForString(vec.as<std::string>(), BlockStatus::STOPPED);

        solver = std::make_shared<BlockSolver<P, V>>(vectors, ccs, grids, pmapping);

        // TODO Open issue - when and how is the [solver] section validated
        YAML::Node vconfig(config);
        vconfig["enabled"] = "true";
        vconfig["_blockname"] = "solver";

        solver->add(std::make_shared<ParentBlockBase<P, V>>(*this, vconfig));
      }
      return solver;
    }

    /** @brief Construct a block given its type identifier and configuration
     * 
     * @param blocktype The type of the block that is to be constructed
     * @param config The configuration node to construct the block from
     * 
     * This is mostly for internal use by @ref ParentBlockBase and similar classes.
     */
    std::shared_ptr<AbstractBlockBase<P, V>> constructBlock(const std::string& blocktype, YAML::Node& config)
    {
      // Blocks should know their name, so we add it here into the config
      if(!config["_blockname"].IsDefined())
        config["_blockname"] = blocktype + "_" + std::to_string(block_counter[blocktype]++);

      // If this identifier does not exist, we are treating it as a vector identifier
      auto identifier = blocktype;
      if (mapping.find(identifier) == mapping.end())
        identifier = "_" + identifier + "_" + config["vector"].as<std::string>("0");

      std::shared_ptr<AbstractBlockBase<P, V>> block;
      try {
        block = mapping[identifier](*this, config);
      }
      catch(std::bad_function_call&)
      {
        DUNE_THROW(Dune::Exception, "Looking for a block of type '" + identifier + "' failed!");
      }

      return block;
    }

    /** @brief Return a name that describes a finite element container
     * 
     * @param i The index of the finite element container
     * @returns The name of the container
     * 
     * This info is currently user-provided.
     */
    std::string getVectorName(std::size_t i)
    {
      return rootconfig["solver"]["vectors"][i].template as<std::string>("solution");
    }

    private:
    // The construction function mapping
    std::map<std::string, std::function<std::shared_ptr<AbstractBlockBase<P, V>>(ConstructionContext<UserParameters, VectorProviders...>&, YAML::Node&)>> mapping;

    // A counter mapping that allows to assign unique block names
    std::map<std::string, int> block_counter;

    // The vector providers - maybe we can achieve that all information
    // is extracted already and not store this!
    std::tuple<std::shared_ptr<VectorProviders>...> providers;

    // The objects that are available during grid construction through above getter
    Dune::MPIHelper& mpihelper;
    YAML::Node rootconfig;

    // The solver class. Actually this should not be exposed during construction
    // because only a very limited subset of its interface is valid
    std::shared_ptr<BlockSolver<P, V>> solver;
  };

} // namespace Dune::BlockLab

#endif
