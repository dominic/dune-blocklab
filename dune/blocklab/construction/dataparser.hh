#ifndef DUNE_BLOCKLAB_CONSTRUCTION_DATAPARSER_HH
#define DUNE_BLOCKLAB_CONSTRUCTION_DATAPARSER_HH

/** Some utilities to parse parameters from strings
 *  into data types that are supported by the parameter
 *  system. These can be used in block construction
 *  from ini files.
 */
#include<dune/blocklab/utilities/stringsplit.hh>
#include<dune/common/exceptions.hh>

#include<string>
#include<vector>

namespace Dune::BlockLab {

  template<typename T>
  T parse_data(const std::string& s)
  {
    YAML::Node n(s);
    return n.as<T>();
  }

  template<typename P>
  P parse_parameter(const std::string& type, const std::string& value)
  {
    if (type == "double")
      return P(parse_data<double>(value));
    else if (type == "int")
      return P(parse_data<int>(value));
    else if (type == "string")
      return P(value);
    else
      DUNE_THROW(Dune::Exception, "Cannot parse parameter");
  }

  template<typename P>
  P parse_parameter(const YAML::Node& param)
  {
    auto type = param["datatype"].as<std::string>("double");
    auto value = param["value"].as<std::string>();

    return parse_parameter<P>(type, value);
  }

  template<typename P>
  std::vector<P> parse_parameter_list(const YAML::Node& param)
  {
    auto type = param["datatype"].as<std::string>("double");
    auto values = param["values"].as<std::string>();

    std::vector<P> ret;
    for (auto value : string_split(values))
      ret.push_back(parse_parameter<P>(type, value));

    return ret;
  }

} // namespace Dune::BlockLab

#endif
