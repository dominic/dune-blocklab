#include"config.h"

#include<dune/blocklab/init.hh>
#include<dune/blocklab/construction/registry.hh>
#include<dune/blocklab/blocks/test/providersetup.hh>


int main(int argc, char** argv)
{
  auto init = Dune::BlockLab::initBlockLab(argc, argv);
  YAML::Node config;

  // Test a number of vector providers with all blocks added to the context.
  // This is much more of a compile test than anything else

  {
    auto ctx = structured_ug2_p1fem(init.helper, config);
    registerBuiltinBlocks(ctx);
    registerBuiltinOperators(ctx);
    ctx.constructSolver(config)->apply();
  }

  {
    auto ctx = structured_ug2_p2fem(init.helper, config);
    registerBuiltinBlocks(ctx);
    registerBuiltinOperators(ctx);
    ctx.constructSolver(config)->apply();
  }

  {
    auto ctx = structured_ug2_powerp1fem(init.helper, config);
    registerBuiltinBlocks(ctx);
    registerBuiltinOperators(ctx);
    ctx.constructSolver(config)->apply();
  }

  return 0;
}
