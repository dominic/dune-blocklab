#include"config.h"

#include<dune/blocklab/init.hh>
#include<dune/blocklab/construction/muparser.hh>
#include<dune/blocklab/solver.hh>
#include<dune/blocklab/blocks/test/providersetup.hh>
#include<dune/common/filledarray.hh>
#include<dune/common/test/testsuite.hh>
#include<dune/grid/yaspgrid.hh>

#include<iostream>
#include<memory>
#include<string>


template<typename Solver, typename... Callbacks>
bool check(std::shared_ptr<Solver> solver, std::string expr, double result, Callbacks&&... callbacks)
{
  auto gv = solver->template getVector<0>()->gridFunctionSpaceStorage()->gridView();
  auto f = Dune::BlockLab::muparser_callable<double(typename decltype(gv)::template Codim<0>::Entity, Dune::FieldVector<double, 3>)>(expr, std::forward<Callbacks>(callbacks)...);
  auto eval = f(*gv.template begin<0>(), Dune::FieldVector<double, 3>(0.5));

  return (std::abs(eval - result) < 1e-8);
}


template<typename Solver, typename... Callbacks>
bool check_array(std::shared_ptr<Solver> solver, std::string expr, std::array<double, 3> result, Callbacks&&... callbacks)
{
  auto gv = solver->template getVector<0>()->gridFunctionSpaceStorage()->gridView();
  auto ft = Dune::BlockLab::muparser_callable_array<3, double(typename decltype(gv)::template Codim<0>::Entity, Dune::FieldVector<double, 3>)>(expr, std::forward<Callbacks>(callbacks)...);
  std::array<double, 3> error;
  std::transform(ft.begin(), ft.end(), result.begin(), error.begin(),
		 [gv](auto f, auto res)
		 {
       return std::abs(f(*gv.template begin<0>(), Dune::FieldVector<double, 3>(0.5)) - res);
		 }
  );

  return (std::accumulate(error.begin(), error.end(), 0.0) < 1e-8);
}

int main(int argc, char** argv)
{
  auto init = Dune::BlockLab::initBlockLab(argc, argv);
  Dune::TestSuite test;

  // Construct a solver instance
  YAML::Node config;
  config["N"] = std::array{1, 1, 1};
  auto ctx = structured_ug3cube_p1fem(init.helper, config);
  auto solver = ctx.constructSolver(YAML::Node());

  // Introduce a parameter
  solver->introduce_parameter("foo", 2.0);

  // Check a normal expression that depends on the custom geometric symbols x, y and z
  test.check(check(solver, "x + y + z", 1.5)) << "Expression depending on x, y, z failed!";

  // Check the function that accesses parameters from the parameter class
  test.check(check(solver, "param(\"foo\")", 2.0, solver)) << "Access to param from solver class failed!";

  // Check that the evaluation of param(foo) is still correct after changing the value
  // This currently holds, but I am unsure whether I should mark param as volatile in
  // muparser in order to be on the safe side.
  solver->update_parameter("foo", 3.0);
  test.check(check(solver, "param(\"foo\")", 3.0, solver)) << "Access to param from solver class failed afer update!";;

  // Check that a function array that broadcasts one function to all array entries
  // works correctly
  test.check(check_array(solver, "x + y + z", {1.5, 1.5, 1.5}));

  // A function array of three functions
  test.check(check_array(solver, "x - 0.5, y, z + 0.5", {0.0, 0.5, 1.0}));

  return test.exit();
}
