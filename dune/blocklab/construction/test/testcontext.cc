#include"config.h"

/** The unit test for the ConstructionContext object.
 * It tests the registration of blocks and their construction
 * with dummy blocks.
 */

#include<dune/blocklab/init.hh>
#include<dune/blocklab/blocks/blockbase.hh>
#include<dune/blocklab/construction/context.hh>
#include<dune/blocklab/grids/structured.hh>
#include<dune/blocklab/utilities/yaml.hh>
#include<dune/blocklab/vectors/pkfem.hh>
#include<dune/grid/uggrid.hh>
#include<dune/pdelab.hh>

#include<iostream>
#include<tuple>


// A standard block that does not specify the vector index
template<typename P, typename V>
class DummyBlock1
  : public Dune::BlockLab::BlockBase<P, V>
{
  public:
  template<typename Context>
  DummyBlock1(Context& ctx, const YAML::Node& config)
    : Dune::BlockLab::BlockBase<P, V>(ctx, config)
  {
    std::cout << "Block Dummy1 created!" << std::endl;
  }

  virtual ~DummyBlock1() = default;

  virtual void apply() override
  {
    std::cout << "Applying Dummy1" << std::endl;
  }
};

// A standard block that does specify the vector index
template<typename P, typename V, std::size_t i>
class DummyBlock2
  : public Dune::BlockLab::BlockBase<P, V, i>
{
  public:
  template<typename Context>
  DummyBlock2(Context& ctx, const YAML::Node& config)
    : Dune::BlockLab::BlockBase<P, V, i>(ctx, config)
  {
    std::cout << "Block Dummy2 with i=" << i << " created!" << std::endl;
  }

  virtual ~DummyBlock2() = default;

  virtual void apply() override
  {
    std::cout << "Applying Dummy2" << std::endl;
  }
};


// A parent block
template<typename P, typename V>
class DummyParent
  : public Dune::BlockLab::ParentBlockBase<P, V>
{
  public:
  template<typename Context>
  DummyParent(Context& ctx, const YAML::Node& config)
    : Dune::BlockLab::ParentBlockBase<P, V>(ctx, config)
  {
    std::cout << "Block DummyParent constructed" << std::endl;
  }
};

int main(int argc, char** argv)
{
  auto init = Dune::BlockLab::initBlockLab(argc, argv);

  // A dummy input
  YAML::Node config;
  
  config = YAML::Load(
    "solver:\n"
    "  vectors:\n"
    "    - first\n"
    "    - second\n"
    "  blocks:\n"
    "    -\n"
    "      _type: parent\n"
    "      blocks:\n"
    "        -\n"
    "          _type: dummy2\n"
    "          vector: 1\n"
    "    -\n"
    "      _type: dummy1"
  );

  // Construct a vector type - just to have one that has the correct Traits
  using Grid = Dune::UGGrid<2>;
  using GridProvider = Dune::BlockLab::StructuredSimplexGridProvider<Grid>;
  using VectorProvider = Dune::BlockLab::PkFemVectorProvider<GridProvider, 1>;

  auto grid = std::make_shared<GridProvider>(config);
  auto vector1 = std::make_shared<VectorProvider>(grid);
  auto vector2 = std::make_shared<VectorProvider>(grid);

  using UserParams = std::tuple<>;

  using Context = Dune::BlockLab::ConstructionContext<UserParams, VectorProvider, VectorProvider>;

  Context ctx(init.helper, config, vector1, vector2);

  ctx.registerBlock<DummyBlock1>("dummy1");
  ctx.registerBlock<DummyBlock2>("dummy2");
  ctx.registerBlock<DummyParent>("parent");

  auto solver = ctx.constructSolver(config["solver"]);
  solver->apply();

  return 0;
}
