#ifndef DUNE_BLOCKLAB_SOLVER_HH
#define DUNE_BLOCKLAB_SOLVER_HH

/** This is the main include for the dune-blocklab solver class.
 *  No additional includes needed.
 */

#include<dune/blocklab/blocks/blockbase.hh>
#include<dune/blocklab/blocks/blocktraits.hh>
#include<dune/blocklab/operators/virtualinterface.hh>
#include<dune/blocklab/utilities/uniquevariant.hh>
#include<dune/common/fvector.hh>
#include<dune/common/shared_ptr.hh>

#include<map>
#include<memory>
#include<string>
#include<tuple>
#include<vector>

namespace Dune::BlockLab {

  template<typename P, typename V>
  class BlockSolver;

  /** @brief The main block solver class
   * 
   * @ingroup blocks_custom
   */
  template<typename... Parameters, typename... Vectors>
  class BlockSolver<std::tuple<Parameters...>, std::tuple<Vectors...>>
  {
    public:
    using ParameterTuple = std::tuple<Parameters...>;
    using VectorTuple = std::tuple<Vectors...>;

    // The possible types for parametrization of solver steps
    using Parameter = unique_variant<bool,
                                     double,
                                     int,
                                     std::string,
                                     YAML::Node,
                                     Dune::FieldVector<double, 1>,
                                     Dune::FieldVector<double, Vectors::GridFunctionSpace::Traits::GridView::dimension>...,
                                     std::shared_ptr<AbstractLocalOperatorInterface<typename Vectors::GridFunctionSpace>>...,
                                     Parameters...
				     >;

    BlockSolver(
      std::tuple<std::shared_ptr<Vectors>...> vectors,
      std::tuple<std::shared_ptr<typename Vectors::GridFunctionSpace::template ConstraintsContainer<double>::Type>...> ccs,
      std::tuple<std::shared_ptr<typename Vectors::GridFunctionSpace::Traits::GridView::Traits::Grid>...> grids,
      std::map<std::string, Parameter> parameters = {}
      )
      : vectors(vectors)
      , constraintscontainers(ccs)
      , grids(grids)
    {
      for(auto [name, p] : parameters)
        introduce_parameter(name, p);
    }

    void apply()
    {
      // Make sure that each block knows its parent block
      for (auto block : blocks)
        block->set_parent(nullptr);

      // Call setup on all blocks
      for (auto block : blocks)
        block->setup();

      // Make sure that all blocks can update to the initial state
      // of the parameter system
      for (auto [name, param] : paramdata)
      {
        for (auto block : blocks)
          block->update_parameter(name, param);
      }

      // Call apply on all blocks
      triggerBlockStatusEvent(*this, BlockStatus::STARTED);
      for (auto block : blocks)
        block->apply();
      triggerBlockStatusEvent(*this, BlockStatus::STOPPED);
    }

    template<typename BLOCK>
    void add(std::shared_ptr<BLOCK> block)
    {
      blocks.push_back(block);
      block->set_solver(Dune::stackobject_to_shared_ptr(*this));
    }

    template<typename BLOCK>
    void add(BLOCK& block)
    {
      add(Dune::stackobject_to_shared_ptr(block));
    }

    /** @brief Introduce a new parameter into the solver's parameter system
     * 
     * @ingroup parameter
     */
    template<typename T>
    void introduce_parameter(std::string name, T&& val)
    {
      introduce_parameter(name, Parameter(std::forward<T>(val)));
    }

    void introduce_parameter(std::string name, Parameter param)
    {
      if (paramdata.count(name) == 0)
        paramdata[name] = param;
    }

    /** @brief Update an already existing parameter in the solver's parameter system.
     * 
     * This will notify all blocks of the change.
     * 
     * @ingroup parameter
     */
    template<typename T>
    void update_parameter(std::string name, T&& val)
    {
      update_parameter(name, Parameter(std::forward<T>(val)));
    }

    void update_parameter(std::string name, Parameter val)
    {
      paramdata[name] = val;

      for (auto block: blocks)
        block->update_parameter(name, paramdata[name]);
    }

    /** @brief Access the value of an existing parameter in the solver's parameter systen
     * 
     * @ingroup parameter
     */
    template<typename T>
    T param(std::string name) const
    {
      return std::get<typename std::decay<T>::type>(paramdata.find(name)->second);
    }

    template<std::size_t i>
    auto getVector()
    {
      return std::get<i>(vectors);
    }

    template<std::size_t i>
    auto getConstraintsContainer()
    {
      return std::get<i>(constraintscontainers);
    }

    template<std::size_t i>
    auto getGrid()
    {
      return std::get<i>(grids);
    }

    static std::vector<std::string> blockData()
    {
      return {
        "title: BlockLab PDE Solver                 \n"
        "schema:                                    \n"
        "  blocks:                                  \n"
        "    required: true                         \n"
        "    type: string                           \n"
      };
    }

    std::string getBlockName() const
    {
      return "solver_0";
    }

    LogLevel getLogLevel() const
    {
      return LogLevel::INFO;
    }

    private:
    std::vector<std::shared_ptr<AbstractBlockBase<std::tuple<Parameters...>, std::tuple<Vectors...>>>> blocks;
    std::map<std::string, Parameter> paramdata;

    // The data gathered by vector providers and passed to the constructor of this class
    std::tuple<std::shared_ptr<Vectors>...> vectors;
    std::tuple<std::shared_ptr<typename Vectors::GridFunctionSpace::template ConstraintsContainer<double>::Type>...> constraintscontainers;
    std::tuple<std::shared_ptr<typename Vectors::GridFunctionSpace::Traits::GridView::Traits::Grid>...> grids;
  };

} // namespace Dune::BlockLab

#endif
