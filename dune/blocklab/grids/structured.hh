#ifndef DUNE_BLOCKLAB_GRIDS_STRUCTURED_HH
#define DUNE_BLOCKLAB_GRIDS_STRUCTURED_HH

#include<dune/blocklab/grids/capabilities.hh>
#include<dune/blocklab/utilities/yaml.hh>
#include<dune/common/filledarray.hh>
#include<dune/common/fvector.hh>
#include<dune/grid/utility/structuredgridfactory.hh>
#include<dune/pdelab/common/partitionviewentityset.hh>

#include<map>
#include<memory>
#include<tuple>


namespace Dune::BlockLab {

  namespace impl {

    template<typename GridImpl, bool simplex>
    class StructuredGridProviderImpl
    {
      public:
      using Grid = GridImpl;
      using Parameter = std::tuple<>;

      static constexpr int dim = Grid::dimension;

      StructuredGridProviderImpl(const YAML::Node& config)
        : config(config)
      {}

      std::shared_ptr<Grid> createGrid()
      {
        if(!grid)
        {
          auto ll = config["lowerleft"].template as<Dune::FieldVector<double, dim>>(Dune::FieldVector<double, dim>(0.0));
          auto ur = config["upperright"].template as<Dune::FieldVector<double, dim>>(Dune::FieldVector<double, dim>(1.0));
          auto N = config["N"].template as<std::array<unsigned int, dim>>(Dune::filledArray<dim, unsigned int>(10));

          if constexpr (simplex)
            grid = Dune::StructuredGridFactory<Grid>::createSimplexGrid(ll, ur, N);
          else
            grid = Dune::StructuredGridFactory<Grid>::createCubeGrid(ll, ur, N);
          grid->loadBalance();
          grid->globalRefine(config["refinement"].template as<int>(0));
        }

        return grid;
      }

      template<typename P>
      std::map<std::string, P> createParameters()
      {
        return {};
      }

      static std::vector<std::string> blockData()
      {
        std::string title = std::to_string(dim) + "D Structured " + (simplex ? "Simplex" : "Cube") + " Grid";
        return {
          "title: " + title + "             \n"
          "category: grids                  \n"
          "schema:                          \n"
          "  lowerleft:                     \n"
          "    type: list                   \n"
          "    minlength: " + std::to_string(dim) + "\n"
          "    maxlength: " + std::to_string(dim) + "\n"
          "    schema:                      \n"
          "      type: float                \n"
          "      default: 0.0               \n"
          "    meta:                        \n"
          "      title: Lowerleft corner    \n"
          "  upperright:                    \n"
          "    type: list                   \n"
          "    minlength: " + std::to_string(dim) + "\n"
          "    maxlength: " + std::to_string(dim) + "\n"
          "    schema:                      \n"
          "      type: float                \n"
          "      default: 1.0               \n"
          "    meta:                        \n"
          "      title: Upperright corner   \n"
          "  N:                             \n"
          "    type: list                   \n"
          "    minlength: " + std::to_string(dim) + "\n"
          "    maxlength: " + std::to_string(dim) + "\n"
          "    schema:                      \n"
          "      type: integer              \n"
          "      default: 10                \n"
          "      min: 1                     \n"
          "    meta:                        \n"
          "      title: Number of grid cells\n"
          "  refinement:                    \n"
          "    type: integer                \n"
          "    default: 0                   \n"
          "    meta:                        \n"
          "      title: Global refinement levels \n"
        };
      }

      private:
      std::shared_ptr<Grid> grid;
      YAML::Node config;
    };

  } // namespace impl


  template<typename Grid>
  class StructuredSimplexGridProvider
    : public impl::StructuredGridProviderImpl<Grid, true>
  {
    using impl::StructuredGridProviderImpl<Grid, true>::StructuredGridProviderImpl;
  };

  template<typename Grid>
  class StructuredCubeGridProvider
    : public impl::StructuredGridProviderImpl<Grid, false>
  {
    using impl::StructuredGridProviderImpl<Grid, false>::StructuredGridProviderImpl;
  };

  namespace Capabilities {

    template<typename Grid>
    struct HasSimplices<StructuredSimplexGridProvider<Grid>>
    {
      static constexpr bool value = true;
    };

    template<typename Grid>
    struct HasCubes<StructuredCubeGridProvider<Grid>>
    {
      static constexpr bool value = true;
    };

    template<typename Grid>
    struct HasSimplices<StructuredCubeGridProvider<Grid>>
    {
      static constexpr bool value = isAdaptive<StructuredCubeGridProvider<Grid>>();
    };

  } // namespace Capabilities

} // namespace Dune::BlockLab

#endif
