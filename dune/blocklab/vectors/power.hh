#ifndef DUNE_BLOCKLAB_VECTORS_POWER_HH
#define DUNE_BLOCKLAB_VECTORS_POWER_HH

#include<dune/pdelab/backend/istl.hh>
#include<dune/pdelab/gridfunctionspace/powergridfunctionspace.hh>

#include<memory>
#include<string>
#include<tuple>


namespace Dune::BlockLab {

  template<typename VectorProvider, std::size_t n>
  class PowerVectorProvider
  {
    using LeafGridFunctionSpace = typename VectorProvider::GridFunctionSpace;
    using VectorBackend = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;

    public:
    // The types of the objects constructed by this provider
    using GridFunctionSpace = Dune::PDELab::PowerGridFunctionSpace<LeafGridFunctionSpace, n, VectorBackend>;
    using Grid = typename GridFunctionSpace::Traits::GridView::Traits::Grid;
    using Vector = Dune::PDELab::Backend::Vector<GridFunctionSpace, double>;
    using ConstraintsContainer = typename GridFunctionSpace::template ConstraintsContainer<double>::Type;

    // The Parameter types exported by this provider
    using Parameter = typename VectorProvider::Parameter;

    PowerVectorProvider(std::shared_ptr<VectorProvider> vectorprovider)
      : leafvectorprovider(vectorprovider)
      , gfs(nullptr)
      , v(nullptr)
      , cc(nullptr)
    {}

    std::shared_ptr<Grid> getGrid()
    {
      return leafvectorprovider->getGrid();
    }

    template<typename P>
    std::map<std::string, P> createParameters()
    {
      return leafvectorprovider->template createParameters<P>();
    }

    std::shared_ptr<const GridFunctionSpace> getGridFunctionSpace()
    {
      if(!gfs)
        gfs = std::make_shared<GridFunctionSpace>(*leafvectorprovider->getGridFunctionSpace());
      return gfs;
    }

    std::shared_ptr<Vector> getVector()
    {
      if (!v)
        v = std::make_shared<Vector>(getGridFunctionSpace());
      return v;
    }

    std::shared_ptr<ConstraintsContainer> getConstraintsContainer()
    {
      if (!cc)
        cc = std::make_shared<ConstraintsContainer>();
      return cc;
    }

    static std::vector<std::string> blockData()
    {
      return {
        "title: 'Power Element'                           \n"
        "category: vectors                                \n"
      };
    }

    private:
    std::shared_ptr<VectorProvider> leafvectorprovider;

    // Internally store the return objects to ensure that the getter methods
    // always return the same object, even if called multiple times
    std::shared_ptr<GridFunctionSpace> gfs;
    std::shared_ptr<Vector> v;
    std::shared_ptr<ConstraintsContainer> cc;
  };

  template<typename std::size_t n, typename VectorProvider>
  auto powerProvider(std::shared_ptr<VectorProvider> vectorprovider)
  {
    return std::make_shared<PowerVectorProvider<VectorProvider, n>>(vectorprovider);
  }

} // namespace Dune::BlockLab

#endif
