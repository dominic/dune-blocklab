#ifndef DUNE_BLOCKLAB_VECTORS_COMPOSITE_HH
#define DUNE_BLOCKLAB_VECTORS_COMPOSITE_HH

#include<dune/blocklab/utilities/tuple.hh>
#include<dune/pdelab/backend/istl.hh>
#include<dune/pdelab/gridfunctionspace/compositegridfunctionspace.hh>

#include<memory>
#include<string>
#include<tuple>


namespace Dune::BlockLab {

  template<typename... VectorProvider>
  class CompositeVectorProvider
  {
    using VectorBackend = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
    using OrderingTag = Dune::PDELab::LexicographicOrderingTag;

    public:
    // The types of the objects constructed by this provider
    using GridFunctionSpace = Dune::PDELab::CompositeGridFunctionSpace<VectorBackend, OrderingTag, typename VectorProvider::GridFunctionSpace...>;
    using Grid = typename GridFunctionSpace::Traits::GridView::Traits::Grid;
    using Vector = Dune::PDELab::Backend::Vector<GridFunctionSpace, double>;
    using ConstraintsContainer = typename GridFunctionSpace::template ConstraintsContainer<double>::Type;

    // The Parameter types exported by this provider
    using Parameter = tuple_cat_t<typename VectorProvider::Parameter...>;

    CompositeVectorProvider(std::shared_ptr<VectorProvider>... vectorprovider)
      : leafvectorprovider(vectorprovider...)
      , gfs(nullptr)
      , v(nullptr)
      , cc(nullptr)
    {}

    std::shared_ptr<Grid> getGrid()
    {
      return std::get<0>(leafvectorprovider)->getGrid();
    }

    template<typename P>
    std::map<std::string, P> createParameters()
    {
      std::map<std::string, P> ret;
      std::apply([&ret](auto... p)
                 {
                   ( ret.merge(p->template createParameters<P>()), ...);
                 },
                 leafvectorprovider);

      return ret;
    }

    std::shared_ptr<const GridFunctionSpace> getGridFunctionSpace()
    {
      if(!gfs)
        gfs = std::apply([](auto... v) {
                           return std::make_shared<GridFunctionSpace>(v->getGridFunctionSpace()...);
                         },
                         leafvectorprovider);
      return gfs;
    }

    std::shared_ptr<Vector> getVector()
    {
      if (!v)
        v = std::make_shared<Vector>(getGridFunctionSpace());
      return v;
    }

    std::shared_ptr<ConstraintsContainer> getConstraintsContainer()
    {
      if (!cc)
        cc = std::make_shared<ConstraintsContainer>();
      return cc;
    }

    static std::vector<std::string> blockData()
    {
      return {
        "title: 'Composite Element'                           \n"
        "category: vectors                                    \n"
      };
    }

    private:
    std::tuple<std::shared_ptr<VectorProvider>...> leafvectorprovider;

    // Internally store the return objects to ensure that the getter methods
    // always return the same object, even if called multiple times
    std::shared_ptr<GridFunctionSpace> gfs;
    std::shared_ptr<Vector> v;
    std::shared_ptr<ConstraintsContainer> cc;
  };

  template<typename... VectorProvider>
  auto compositeProvider(std::shared_ptr<VectorProvider>... vectorprovider)
  {
    return std::make_shared<CompositeVectorProvider<VectorProvider...>>(vectorprovider...);
  }

} // namespace Dune::BlockLab

#endif
