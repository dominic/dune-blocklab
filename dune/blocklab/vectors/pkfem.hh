#ifndef DUNE_BLOCKLAB_VECTORS_PKFEM_HH
#define DUNE_BLOCKLAB_VECTORS_PKFEM_HH

/** A standard PkFEM-based DoF vector provider
 */

#include<dune/pdelab/backend/interface.hh>
#include<dune/pdelab/backend/istl.hh>
#include<dune/pdelab/common/partitionviewentityset.hh>
#include<dune/pdelab/constraints/conforming.hh>
#include<dune/pdelab/finiteelementmap/pkfem.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspace.hh>

#include<map>
#include<memory>
#include<string>
#include<tuple>


namespace Dune::BlockLab {

  template<typename GridProvider, unsigned int degree>
  class PkFemVectorProvider
  {
    using GridView = typename GridProvider::Grid::LeafGridView;
    using EntitySet = Dune::PDELab::OverlappingEntitySet<GridView>;
    using FiniteElementMap = Dune::PDELab::PkLocalFiniteElementMap<EntitySet, double, double, degree>;
    using ConstraintsAssembler = Dune::PDELab::OverlappingConformingDirichletConstraints;

    public:
    // The types of the objects constructed by this provider
    using GridFunctionSpace = Dune::PDELab::GridFunctionSpace<EntitySet, FiniteElementMap, ConstraintsAssembler>;
    using Grid = typename GridFunctionSpace::Traits::GridView::Traits::Grid;
    using Vector = Dune::PDELab::Backend::Vector<GridFunctionSpace, double>;
    using ConstraintsContainer = typename GridFunctionSpace::template ConstraintsContainer<double>::Type;

    // The Parameter types exported by this provider
    using Parameter = typename GridProvider::Parameter;

    PkFemVectorProvider(std::shared_ptr<GridProvider> gridprovider)
      : gridprovider(gridprovider)
      , gfs(nullptr)
      , v(nullptr)
      , cc(nullptr)
    {}

    std::shared_ptr<Grid> getGrid()
    {
      return gridprovider->createGrid();
    }

    template<typename P>
    std::map<std::string, P> createParameters()
    {
      return gridprovider->template createParameters<P>();
    }

    std::shared_ptr<GridFunctionSpace> getGridFunctionSpace()
    {
      if(!gfs)
      {
        EntitySet es(gridprovider->createGrid()->leafGridView());
        auto fem = std::make_shared<FiniteElementMap>(es);
        gfs = std::make_shared<GridFunctionSpace>(es, fem);
      }
      return gfs;
    }

    std::shared_ptr<Vector> getVector()
    {
      if (!v)
	      v = std::make_shared<Vector>(getGridFunctionSpace());
      return v;
    }

    std::shared_ptr<ConstraintsContainer> getConstraintsContainer()
    {
      if (!cc)
        cc = std::make_shared<ConstraintsContainer>();
      return cc;
    }

    static std::vector<std::string> blockData()
    {
      return {
        "title: 'P" + std::to_string(degree) + "Lagrange Element'   \n"
        "category: vectors                                          \n"
      };
    }

    private:
    std::shared_ptr<GridProvider> gridprovider;

    // Internally store the return objects to ensure that the getter methods
    // always return the same object, even if called multiple times
    std::shared_ptr<GridFunctionSpace> gfs;
    std::shared_ptr<Vector> v;
    std::shared_ptr<ConstraintsContainer> cc;
  };

} // namespace Dune::BlockLab

#endif
