#ifndef DUNE_BLOCKLAB_VECTORS_FIELD_HH
#define DUNE_BLOCKLAB_VECTORS_FIELD_HH

#include<dune/pdelab/backend/istl.hh>
#include<dune/pdelab/constraints/conforming.hh>
#include<dune/pdelab/gridfunctionspace/vectorgridfunctionspace.hh>
#include<dune/typetree/utility.hh>

#include<memory>
#include<string>
#include<tuple>
#include<type_traits>


namespace Dune::BlockLab {

  template<typename VectorProvider, std::size_t n, typename enable = void>
  class VectorFieldProvider
  {
    // TODO: How do we get the best error message if somebody
    //       ends up in this branch of enable_if
  };

  template<typename VectorProvider, std::size_t n>
  class VectorFieldProvider<VectorProvider, n, std::enable_if_t<Dune::TypeTree::TreeInfo<typename VectorProvider::GridFunctionSpace>::leafCount == 1>>
  {
    using EntitySet = typename VectorProvider::GridFunctionSpace::Traits::EntitySet;
    using LeafFiniteElementMap = typename VectorProvider::GridFunctionSpace::Traits::FiniteElementMap;
    using VectorBackend = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
    using LeafVectorBackend = Dune::PDELab::ISTL::VectorBackend<>;
    using ConstraintsAssembler = Dune::PDELab::OverlappingConformingDirichletConstraints;

    public:
    // The types of the objects constructed by this provider
    using GridFunctionSpace = Dune::PDELab::VectorGridFunctionSpace<EntitySet, LeafFiniteElementMap, n, VectorBackend, LeafVectorBackend, ConstraintsAssembler>;
    using Grid = typename GridFunctionSpace::Traits::GridView::Traits::Grid;
    using Vector = Dune::PDELab::Backend::Vector<GridFunctionSpace, double>;
    using ConstraintsContainer = typename GridFunctionSpace::template ConstraintsContainer<double>::Type;

    // The Parameter types exported by this provider
    using Parameter = typename VectorProvider::Parameter;

    VectorFieldProvider(std::shared_ptr<VectorProvider> vectorprovider)
      : leafvectorprovider(vectorprovider)
      , gfs(nullptr)
      , v(nullptr)
      , cc(nullptr)
    {}

    std::shared_ptr<Grid> getGrid()
    {
      return leafvectorprovider->getGrid();
    }

    template<typename P>
    std::map<std::string, P> createParameters()
    {
      return leafvectorprovider->template createParameters<P>();
    }

    std::shared_ptr<const GridFunctionSpace> getGridFunctionSpace()
    {
      if(!gfs)
      {
        auto leafgfs = leafvectorprovider->getGridFunctionSpace();
        auto es = leafgfs->entitySet();
        gfs = std::make_shared<GridFunctionSpace>(es, leafgfs->finiteElementMapStorage());
      }
      return gfs;
    }

    std::shared_ptr<Vector> getVector()
    {
      if (!v)
        v = std::make_shared<Vector>(getGridFunctionSpace());
      return v;
    }

    std::shared_ptr<ConstraintsContainer> getConstraintsContainer()
    {
      if (!cc)
        cc = std::make_shared<ConstraintsContainer>();
      return cc;
    }

    static std::vector<std::string> blockData()
    {
      return {
        "title: 'Vector Element'                           \n"
        "category: vectors                                 \n"
      };
    }

    private:
    std::shared_ptr<VectorProvider> leafvectorprovider;

    // Internally store the return objects to ensure that the getter methods
    // always return the same object, even if called multiple times
    std::shared_ptr<GridFunctionSpace> gfs;
    std::shared_ptr<Vector> v;
    std::shared_ptr<ConstraintsContainer> cc;
  };

  template<typename std::size_t n, typename VectorProvider>
  auto fieldProvider(std::shared_ptr<VectorProvider> vectorprovider)
  {
    return std::make_shared<VectorFieldProvider<VectorProvider, n>>(vectorprovider);
  }

} // namespace Dune::BlockLab

#endif
