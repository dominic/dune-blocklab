#ifndef DUNE_BLOCKLAB_INIT_HH
#define DUNE_BLOCKLAB_INIT_HH

#include<dune/common/parallel/mpihelper.hh>

#include<Python.h>

#include<iostream>


namespace Dune::BlockLab {

  /** @brief A struct that is returned by @ref initBlockLab
   * 
   * All initialization data can be retrieved from it (e.g. the MPIHelper).
   * On top of that, its destructor handles finalization of the application.
   * 
   * Currently, the following initialization procedure take place:
   * * MPI through @c Dune::MPIHelper
   * * The embedded Python interpreter
   */
  struct InitState
  {
    /** Construct the state object and do the actual initialization
     *
     *  @param argc The argument count as passed to @c main
     *  @param argv The argument values as passed to @c main
     */ 
    InitState(int argc, char** argv)
      : argc(argc)
      , argv(argv)
      , helper(Dune::MPIHelper::instance(argc, argv))
    {
      // Initialize Python
      Py_InitializeEx(0);
    }

    /** @brief Prohibit copies of InitState objects!
     * 
     *  Copies of the init state are dangerous as their destruction
     * alters global state of the application!
     */
    InitState(const InitState&) = delete;

    /** Destruct the state object
     * 
     * This is equivalent to finalization of the initialized processes
     */
    ~InitState()
    {
      // Finalize Python
      if(Py_FinalizeEx())
        std::cerr << "Python Interpreter exited with errors." << std::endl;
    }

    //! We export argc as we received it from main
    int argc;
    //! We export argv as we received it from main
    char** argv;

    //! The MPIHelper object is used in Dune to perform MPI operations
    Dune::MPIHelper& helper;
  };

  /** @brief Initialize BlockLab
   * 
   * This needs to be called in the first line of any BlockLab application.
   * 
   * @param argc The argument count as passed to @c main
   * @param argv The argument values as passed to @c main
   * 
   * @returns An object of type @ref InitState that can be queried for any
   *          results from initialization.
   * 
   * @ingroup app
   */
  InitState initBlockLab(int argc, char** argv)
  {
    return InitState(argc, argv);
  }

} // namespace Dune::BlockLab

#endif
