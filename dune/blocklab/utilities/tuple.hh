#ifndef DUNE_BLOCKLAB_UTILITIES_TUPLECAT_HH
#define DUNE_BLOCKLAB_UTILITIES_TUPLECAT_HH

/** Some utilities for metaprogramming with std::tuple.
 *  These are often unreadable and need a lot of helper structs,
 *  so they are placed in this utility header.
 */

#include<tuple>

namespace Dune::BlockLab {

  /** @brief Get the type of a tuple concatenation 
   * 
   *  The C++ standard library has @c std::tuple_cat to
   *  concatenate tuples, but it does not have @c std::tuple_cat_t
   *  which just gives you the type of the concatenated tuple.
   *  Implementation from https://stackoverflow.com/a/53398815
   */
  template<typename... T>
  using tuple_cat_t = decltype(std::tuple_cat(std::declval<T>()...));


  namespace impl {

    template<typename F, std::size_t... I>
    auto _make_tuple_from_indices(F&& f, std::index_sequence<I...>)
    {
      return std::make_tuple(std::forward<F>(f)(std::integral_constant<std::size_t, I>{})...);
    }

  } // namespace impl

  /** @brief Creates a tuple from a lambda that accepts only a compile time index */
  template<std::size_t n, typename F>
  auto make_tuple_from_indices(F&& f)
  {
    return impl::_make_tuple_from_indices<F>(std::forward<F>(f), std::make_index_sequence<n>{});
  }

} // namespace Dune::BlockLab

#endif
