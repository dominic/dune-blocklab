#ifndef DUNE_BLOCKLAB_UTILTIES_HH
#define DUNE_BLOCKLAB_UTILTIES_HH

/** \file Some tools to embed a Python interpreter into BlockLab executables */

#include<dune/common/exceptions.hh>

#include<Python.h>

#include<csignal>
#include<string>
#include<vector>


namespace Dune::BlockLab
{

  /** @brief A class to encapsulate code execution in an embedded Python interpreter
   * 
   * A @c PythonContext object will create a new Python module. All commands executed in this
   *  context will use the globals/locals dictionary of this module. A technical alternative
   *  to this approach would be the use of sub-interpreters, but there are some caveats
   *  with the GIL-API when using sub-interpreters that cause the program to hang if more
   *  involved Python extensions like numpy or numba are used. As we are explicitly interested
   *  in these, we use the module approach.
   */
  class PythonContext

  {
    public:
    /** Construct the Python context given a module name
     * 
     * @param module A module name identfying this execution context
     */
    PythonContext(const std::string& module = "mymodule")
    {
      PyImport_AddModule(module.c_str());
      PyObject* impmodule = PyImport_ImportModule(module.c_str());
      globals = PyModule_GetDict(impmodule);
      locals = PyModule_GetDict(impmodule);
      PyDict_SetItemString(globals, "__builtins__", PyEval_GetBuiltins());
    }

    /** @brief Import a Python module
     * 
     *  @param module The module name to import
     * 
     *  This is more complicated than expected because the import process
     *  might screw up signal handlers resulting in our applications to
     *  not respond to CTRL+C anymore. This solution to the process
     *  is taken from https://stackoverflow.com/a/28788995/2819459
     */
    void import(const std::string& module)
    {
      PyOS_sighandler_t sighandler = PyOS_getsig(SIGINT);
      run("import " + module);
      PyOS_setsig(SIGINT, sighandler);
    }

    /** @brief Run a line of Python code
     * 
     * @param line The Python code line to run
     */
    void run(const std::string& line) const
    {
      PyRun_String(line.c_str(), Py_single_input, globals, locals);
      if (PyErr_Occurred())
      {
        PyErr_Print();
        DUNE_THROW(Dune::Exception, "Python interpreter failed executing '" + line + "'");
      }
    }

    /** @brief Run a bunch of lines of Python code
     * 
     * @param lines An @c std::vector of Python code lines to run
     */
    void run(const std::vector<const char*>& lines) const
    {
      for (const auto& line : lines)
        run(std::string(line));
    }

    protected:
    PyObject* globals;
    PyObject* locals;
  };


  /** @brief Stringify a Python object */
  std::string stringify(PyObject* obj)
  {
    char* result;
    PyObject* repr = PyObject_Str(obj);
    if (PyUnicode_Check(repr)) {
      PyObject* temp_bytes = PyUnicode_AsEncodedString(repr, "UTF-8", "strict");
      if (temp_bytes != NULL)
      {
        result = PyBytes_AS_STRING(temp_bytes);
        Py_DECREF(temp_bytes);
      }
      else
        DUNE_THROW(Dune::Exception, "Encoding error in validation interpreter");
    }
    else
      DUNE_THROW(Dune::Exception, "Unexpected object in validation interpreter");

    return std::string(result);
  }

} // namespace Dune::BlockLab

#endif
