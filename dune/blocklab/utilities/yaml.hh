#ifndef DUNE_BLOCKLAB_UTILITIES_YAML_HH
#define DUNE_BLOCKLAB_UTILITIES_YAML_HH

/** Tools to deal with YAML input */
#include<dune/blocklab/events/base.hh>
#include<dune/common/parametertree.hh>

#include<yaml-cpp/yaml.h>

#include<string>

/** @cond YAML_STUFF 
 * 
 * We are omitting these YAML conversion specialization from the Doxygen Docs as they
 * are not all that helpful and they show up quite prominently due them being in a
 * separate namespace.
*/
namespace YAML {

  template<typename T, int n>
  struct convert<Dune::FieldVector<T, n>>
  {
    static Node encode(const Dune::FieldVector<T, n>& rhs)
    {
      Node node;
      for(int i=0; i<n; ++i)
        node.push_back(rhs[i]);
      return node;
    }

    static bool decode(const Node& node, Dune::FieldVector<T, n>& rhs)
    {
      if(!node.IsSequence() || node.size() < n)
        return false;

      for(int i=0; i<n; ++i)
        rhs[i] = node[i].as<T>();
      return true;
    }
  };

  template<>
  struct convert<Dune::BlockLab::LogLevel>
  {
    static Node encode(const Dune::BlockLab::LogLevel& rhs)
    {
      return Node(static_cast<int>(rhs));
    }

    static bool decode(const Node& node, Dune::BlockLab::LogLevel& rhs)
    {
      if(!node.IsScalar())
        return false;
      rhs = Dune::BlockLab::LogLevel(node.as<int>());
      return true;
    }
  };
    
} // namespace YAML
/** @endcond */


namespace Dune::BlockLab {

  namespace impl {
 
    void ymlParse(Dune::ParameterTree& config, const YAML::Node& node, std::string prefix)
    {
      for(const auto n: node)
      {
        try {
          config[prefix + n.first.as<std::string>()] = n.second.as<std::string>();
        }
        catch(YAML::TypedBadConversion<std::string>& e){
          ymlParse(config, n.second, prefix + n.first.as<std::string>() + ".");
        }
      }
    }

  }

  /** @brief Transform a YAML Node into @c Dune::ParameterTree 
   * 
   * This utility can be used if a @c Dune data structure requires
   * an instance of @c Dune::ParameterTree , but we are only having
   * an instance of @c YAML::Node.
   * 
   * This of course assumes that there is no constructs that cannot
   * be expressed by @c Dune::ParameterTree (such as sequences).
  */
  Dune::ParameterTree ymlToParameterTree(const YAML::Node& node)
  {
    Dune::ParameterTree config;
    impl::ymlParse(config, node, "");
    return config;
  }

} // namespace Dune::Blocklab

#endif
