#include"config.h"

#include<dune/blocklab/utilities/yaml.hh>
#include<dune/common/fvector.hh>
#include<dune/common/test/testsuite.hh>

#include<iostream>


int main()
{
  Dune::TestSuite test;
 
  auto yml = YAML::LoadFile("test.yml");

  // Check conversion to Dune::ParameterTree
  // This should cover all the YAML::convert specializations that we added

  return test.exit();
}