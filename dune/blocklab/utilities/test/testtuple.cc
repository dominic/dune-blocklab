#include"config.h"

#include<dune/blocklab/utilities/tuple.hh>
#include<dune/common/test/testsuite.hh>

#include<tuple>

int main()
{
  Dune::TestSuite test;

  // Compile test for tuple_cat_t
  Dune::BlockLab::tuple_cat_t<std::tuple<int, double>, std::tuple<int>> cat{1, 1.0, 1};

  // Test make_tuple_from_indices
  auto data = std::make_tuple("foo", 42);
  auto t = Dune::BlockLab::make_tuple_from_indices<2>([data](auto i){ return std::get<i>(data); });
  test.check(std::get<0>(t) == "foo");
  test.check(std::get<1>(t) == 42);

  return test.exit();
}
