#ifndef DUNE_BLOCKLAB_UTILITIES_LOCATE_HH
#define DUNE_BLOCKLAB_UTILITIES_LOCATE_HH

/** @file Some tools to locate files on the file system */

#include<string>
#include<string.h>
#include<dune/blocklab/utilities/whereami.h>


namespace Dune::BlockLab {

  /** @brief Return the path that the currect executable is located in */
  std::string get_executable_path()
  {
    int dirname_length;
    int length = wai_getExecutablePath(NULL, 0, NULL);
    char* path = (char*)malloc(length + 1);
    wai_getExecutablePath(path, length, &dirname_length);
    path[length] = '\0';
    return std::string(path);
  }

}

#endif