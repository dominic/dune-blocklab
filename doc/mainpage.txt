/** \mainpage The dune-blocklab developer documentation

This is the developers documentation of dune-blocklab. You are in the correct
place if you are interested in customizing dune-blocklab by providing it with
more functionality like e.g. adding new blocks. Note that some familiarity with
the Dune framework and C++ will be necessary to understand this documentation.

The guides listed in the <a href="modules.html">Modules</a> section try to cover the most interesting customization points
dune-blocklab is offering. If your customization use case is not covered,
consider sending a documentation or feature request using the Gitlab issue
tracker at https://gitlab.dune-project.org/dominic/dune-blocklab/issues
*/