/**

@defgroup parameter How to distribute data between blocks

The @c BlockSolver class manages data which is accessible
from all blocks. Any block can inject data into this system
and all other blocks will be notified of changes in the data.

Data is injected by calling the @c introduce_parameter parameter.
This is typically done from the @c setup method of a block.
As the @c BlockSolver class stores parameter data using @c std::variant,
all possible types of data must be known at compile time.
Several ways to introduce data types exist:

- By default the following types are available:
  - @c bool
  - @c double
  - @c int
  - @c std::string
  - @c YAML::Node
  - <tt>Dune::FieldVector<double, 1></tt>
  - <tt>Dune::FieldVector<double, dim></tt> with @c dim being the grid dimension
  - <tt>std::shared_ptr<AbstractLocalOperatorInterface<GFS>></tt> with @c GFS being a used GridFunctionSpace.
- Application may register an arbitrary amount of types by
  passing an @c std::tuple to @c BlockLabApp as outlined in
  the @ref app guide.

Blocks can access data throught the @c param method of @c BlockSolver,
like e.g. in this snippet:

@code
double param = this->solver->template param<double>("mydouble");
@endcode

Blocks may update data at any time during @c setup or @c apply
by calling the @c update_parameter method of the @c BlockSolver class.
This will notify all blocks about the parameter change by calling
their @c update_parameter method.

Parameters from the block solver's parameter system are available
in @c MuParserCallable expressions through a custom @c param function as well. This is e.g. useful if
a function expression should depend on a parameter, like in the following
snippet:

@code
solver:
  blocks:
    discretevariation:
      name: varied
      datatype: float
      values: 1.0, 2.0
      blocks:
        interpolation:
          functions: param(varied) * 5.0
@endcode

*/