/**

@defgroup grid How to add a grid manager

This guide is not yet written as there is no well-documented interface for
adding a grid. In the future such interface will be added either using
concepts or CRTP.

In the meanwhile, you can have a look at @ref dune/blocklab/grids/structured.hh
and @ref dune/blocklab/grids/gmsh.hh to see two working implementations of
a grid provider and imitate their behaviour.

*/