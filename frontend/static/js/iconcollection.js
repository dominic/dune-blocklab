// A collection of SVG Icons that we use.

var Blocklab = Blocklab || {};
Blocklab.Icons = Blocklab.Icons || {};

Blocklab.Icons['alarm'] = function(group) {
  Blockly.utils.dom.createSvgElement('path',
    {
      'class': 'blocklyIconSymbol',
      'd': 'M0.349,49h49.302L25,1.842L0.349,49z M3.651,47L25,6.159L46.349,47H3.651z',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '24',
      'y': '18',
      'width': '2',
      'height': '18',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '24',
      'y': '39',
      'width': '2',
      'height': '3',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);
};

Blocklab.Icons['compile'] = function(group) {
  Blockly.utils.dom.createSvgElement('path',
    {
      'class': 'blocklyIconSymbol',
      'd': 'M1.118,27.411l4.271,1.412c0.176,0.909,0.415,1.804,0.714,2.673l-2.998,3.363l0.292,0.602 \
        c0.506,1.044,1.094,2.059,1.746,3.017l0.376,0.553l4.391-0.914c0.609,0.7,1.268,1.358,1.968,1.968l-0.914,4.39l0.552,0.376 \
        c0.959,0.653,1.975,1.241,3.018,1.747l0.602,0.292l3.363-2.998c0.869,0.299,1.764,0.538,2.673,0.714l1.413,4.271l0.667,0.048 \
        c0.578,0.043,1.16,0.071,1.748,0.071s1.17-0.028,1.748-0.07l0.667-0.048l1.412-4.271c0.909-0.176,1.804-0.415,2.673-0.714 \
        l3.363,2.998l0.602-0.292c1.044-0.506,2.059-1.094,3.017-1.746l0.553-0.376l-0.914-4.391c0.7-0.609,1.358-1.268,1.968-1.968 \
        l4.39,0.914l0.376-0.552c0.652-0.958,1.24-1.974,1.747-3.018l0.292-0.602l-2.998-3.364c0.299-0.869,0.538-1.764,0.714-2.673 \
        l4.271-1.413l0.048-0.667C48.972,26.166,49,25.584,49,24.996s-0.028-1.17-0.07-1.748l-0.048-0.667l-4.271-1.412 \
        c-0.176-0.909-0.415-1.804-0.714-2.673l2.998-3.363l-0.292-0.602c-0.506-1.044-1.094-2.059-1.746-3.017l-0.376-0.553l-4.391,0.914 \
        c-0.609-0.7-1.268-1.358-1.968-1.968l0.914-4.39l-0.552-0.376c-0.959-0.653-1.975-1.241-3.018-1.747l-0.602-0.292L31.501,6.1 \
        c-0.869-0.299-1.764-0.538-2.673-0.714l-1.413-4.271l-0.667-0.048c-1.155-0.083-2.34-0.083-3.495,0l-0.667,0.048l-1.412,4.271 \
        C20.265,5.562,19.37,5.801,18.501,6.1l-3.363-2.999l-0.602,0.292c-1.042,0.506-2.058,1.093-3.017,1.747l-0.553,0.376l0.914,4.391 \
        c-0.7,0.609-1.358,1.268-1.968,1.968l-4.39-0.914l-0.376,0.552c-0.653,0.959-1.241,1.975-1.747,3.018l-0.292,0.602l2.998,3.363 \
        c-0.299,0.869-0.538,1.764-0.714,2.673L1.12,22.582l-0.048,0.667C1.028,23.826,1,24.408,1,24.996s0.028,1.17,0.07,1.748 \
        L1.118,27.411z M3.024,24.058l4.135-1.368l0.098-0.599c0.192-1.187,0.503-2.352,0.924-3.461l0.215-0.567l-2.899-3.252 \
        c0.289-0.551,0.602-1.092,0.936-1.616l4.247,0.884l0.384-0.469c0.762-0.93,1.619-1.788,2.549-2.548l0.469-0.384L13.198,6.43 \
        c0.525-0.334,1.065-0.647,1.616-0.936l3.252,2.9l0.567-0.215c1.11-0.421,2.275-0.732,3.462-0.924l0.599-0.098l1.367-4.135 \
        c0.622-0.03,1.255-0.03,1.877,0l1.368,4.135l0.599,0.098c1.187,0.192,2.352,0.503,3.461,0.924l0.567,0.215l3.252-2.899 \
        c0.551,0.289,1.092,0.602,1.616,0.936l-0.884,4.247l0.469,0.384c0.93,0.762,1.788,1.619,2.548,2.549l0.384,0.469l4.248-0.884 \
        c0.334,0.524,0.647,1.064,0.936,1.616l-2.899,3.252l0.215,0.567c0.421,1.111,0.732,2.276,0.924,3.462l0.098,0.599l4.135,1.367 \
        C46.991,24.369,47,24.681,47,24.996s-0.009,0.627-0.024,0.938l-4.135,1.368l-0.098,0.599c-0.192,1.187-0.503,2.352-0.924,3.461 \
        l-0.215,0.567l2.899,3.252c-0.289,0.552-0.602,1.092-0.936,1.616l-4.247-0.884l-0.384,0.469c-0.762,0.93-1.619,1.788-2.549,2.548 \
        l-0.469,0.384l0.884,4.248c-0.524,0.334-1.064,0.647-1.616,0.936l-3.252-2.899l-0.567,0.215c-1.111,0.421-2.276,0.732-3.462,0.924 \
        l-0.599,0.098l-1.367,4.135c-0.622,0.03-1.255,0.03-1.877,0l-1.368-4.135l-0.599-0.098c-1.187-0.192-2.352-0.503-3.461-0.924 \
        l-0.567-0.215l-3.252,2.899c-0.551-0.289-1.092-0.602-1.616-0.936l0.884-4.247l-0.469-0.384c-0.93-0.762-1.788-1.619-2.548-2.549 \
        l-0.384-0.469l-4.248,0.884c-0.334-0.524-0.647-1.064-0.936-1.616l2.899-3.252l-0.215-0.567C7.761,30.251,7.45,29.086,7.258,27.9 \
        L7.16,27.301l-4.135-1.367C3.009,25.623,3,25.311,3,24.996S3.009,24.369,3.024,24.058z',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('path',
    {
      'class': 'blocklyIconSymbol',
      'd': 'M25,38.996c7.72,0,14-6.28,14-14s-6.28-14-14-14s-14,6.28-14,14S17.28,38.996,25,38.996z M25,12.996 \
        c6.617,0,12,5.383,12,12s-5.383,12-12,12s-12-5.383-12-12S18.383,12.996,25,12.996z',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);
};

Blocklab.Icons['done'] = function(group) {
  Blockly.utils.dom.createSvgElement('polygon',
    {
      'class': 'blocklyIconSymbol',
      'points': '47.293,6.94 14,40.232 2.707,28.94 1.293,30.353 14,43.06 48.707,8.353',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);
};

Blocklab.Icons['download'] = function(group) {
  Blockly.utils.dom.createSvgElement('polygon',
    {
      'class': 'blocklyIconSymbol',
      'points': '24,25 24,44.586 19.707,40.293 18.293,41.707 25,48.414 31.707,41.707 30.293,40.293 26,44.586 26,25',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('path',
    {
      'class': 'blocklyIconSymbol',
      'd': 'M40.996,17.2C40.999,17.133,41,17.066,41,17c0-3.09-2.349-5.643-5.354-5.965C34.286,5.172,29.095,1,23,1 \
        c-7.168,0-13,5.832-13,13c0,0.384,0.02,0.775,0.06,1.18C4.902,16.106,1,20.669,1,26c0,6.065,4.935,11,11,11h8v-2h-8 \
        c-4.963,0-9-4.038-9-9c0-4.651,3.631-8.588,8.267-8.962l1.091-0.088l-0.186-1.078C12.057,15.198,12,14.586,12,14 \
        c0-6.065,4.935-11,11-11c5.393,0,9.95,3.862,10.836,9.182l0.145,0.871l0.882-0.036C34.925,13.015,34.986,13.008,35,13 \
        c2.206,0,4,1.794,4,4c0,0.272-0.03,0.553-0.091,0.835l-0.235,1.096l1.115,0.109C43.9,19.442,47,22.864,47,27c0,4.411-3.589,8-8,8 \
        h-9v2h9c5.514,0,10-4.486,10-10C49,22.175,45.624,18.128,40.996,17.2z',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);
};

Blocklab.Icons['help'] = function(group) {
  Blockly.utils.dom.createSvgElement('path',
    {
      'class': 'blocklyIconSymbol',
      'd': 'M25,1C11.767,1,1,11.767,1,25s10.767,24,24,24s24-10.767,24-24S38.233,1,25,1z M25,47C12.869,47,3,37.131,3,25 \
        S12.869,3,25,3s22,9.869,22,22S37.131,47,25,47z',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('path',
    {
      'class': 'blocklyIconSymbol',
      'd': 'M27.929,9h-4.857C18.621,9,15,12.621,15,17.071V20h2v-2.929C17,13.724,19.724,11,23.071,11h4.857 \
        C31.276,11,34,13.724,34,17.071c0,1.622-0.632,3.146-1.778,4.293L24,29.586V35h2v-4.586l7.636-7.636 \
        C35.16,21.254,36,19.227,36,17.071C36,12.621,32.379,9,27.929,9z',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '24',
      'y': '38',
      'width': '2',
      'height': '3',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);
};

Blocklab.Icons['log'] = function(group) {
  Blockly.utils.dom.createSvgElement('path',
    {
      'class': 'blocklyIconSymbol',
      'd': 'M39,7c0-3.309-2.691-6-6-6H7C3.691,1,1,3.691,1,7c0,2.967,2.167,5.431,5,5.91V43c0,3.309,2.691,6,6,6h31 \
        c3.309,0,6-2.691,6-6v-1H39V7z M3,7c0-2.206,1.794-4,4-4h21.54C27.586,4.063,27,5.462,27,7s0.586,2.937,1.54,4H7 \
        C4.794,11,3,9.206,3,7z M8,43V13h25v-2c-2.206,0-4-1.794-4-4s1.794-4,4-4s4,1.794,4,4v35v1c0,1.538,0.586,2.937,1.54,4H12 \
        C9.794,47,8,45.206,8,43z M46.873,44c-0.444,1.723-2.013,3-3.873,3s-3.429-1.277-3.873-3H46.873z',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '12',
      'y': '17',
      'width': '4',
      'height': '2',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '19',
      'y': '17',
      'width': '14',
      'height': '2',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '12',
      'y': '22',
      'width': '4',
      'height': '2',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '19',
      'y': '22',
      'width': '14',
      'height': '2',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '12',
      'y': '27',
      'width': '4',
      'height': '2',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '19',
      'y': '27',
      'width': '14',
      'height': '2',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '12',
      'y': '32',
      'width': '4',
      'height': '2',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '19',
      'y': '32',
      'width': '14',
      'height': '2',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '12',
      'y': '37',
      'width': '4',
      'height': '2',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '19',
      'y': '37',
      'width': '14',
      'height': '2',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '12',
      'y': '42',
      'width': '4',
      'height': '2',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '19',
      'y': '42',
      'width': '14',
      'height': '2',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);
};

Blocklab.Icons['play'] = function(group) {
  Blockly.utils.dom.createSvgElement('path',
    {
      'class': 'blocklyIconSymbol',
      'd': 'M7,1.217v47.566L45.918,25L7,1.217z M9,4.783L42.082,25L9,45.217V4.783z',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);
};

Blocklab.Icons['puzzle'] = function(group) {
  Blockly.utils.dom.createSvgElement('path',
    {
      'class': 'blocklyIconSymbol',
      'd': 'M43,24c-0.688,0-1.361,0.119-2,0.351V9H26.649C26.881,8.361,27,7.688,27,7c0-3.309-2.691-6-6-6s-6,2.691-6,6 \
        c0,0.688,0.119,1.361,0.351,2H1v19.031l1.603-1.21C3.313,26.284,4.143,26,5,26c2.206,0,4,1.794,4,4s-1.794,4-4,4 \
        c-0.857,0-1.687-0.284-2.397-0.82L1,31.969V49h40V35.649C41.639,35.881,42.312,36,43,36c3.309,0,6-2.691,6-6S46.309,24,43,24z \
        M43,34c-0.857,0-1.687-0.284-2.397-0.82L39,31.969V47H3V35.649C3.639,35.881,4.312,36,5,36c3.309,0,6-2.691,6-6s-2.691-6-6-6 \
        c-0.688,0-1.361,0.119-2,0.351V11h16.031L17.82,9.397C17.284,8.687,17,7.857,17,7c0-2.206,1.794-4,4-4s4,1.794,4,4 \
        c0,0.857-0.284,1.687-0.82,2.397L22.969,11H39v17.031l1.603-1.21C41.313,26.284,42.143,26,43,26c2.206,0,4,1.794,4,4 \
        S45.206,34,43,34z',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);
};

Blocklab.Icons['refresh'] = function(group) {
  Blockly.utils.dom.createSvgElement('path',
    {
      'class': 'blocklyIconSymbol',
      'd': 'M45.633,17.348C46.54,19.792,47,22.367,47,25c0,12.131-9.869,22-22,22c-6.595,0-12.795-2.963-16.958-8H15v-2H5v10h2v-6.126 \
        C11.53,46.007,18.068,49,25,49c13.234,0,24-10.767,24-24c0-2.872-0.502-5.68-1.492-8.348L45.633,17.348z',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('path',
    {
      'class': 'blocklyIconSymbol',
      'd': 'M35,13h10V3h-2v6.125C38.47,3.992,31.934,1,25,1C11.767,1,1,11.767,1,25c0,2.872,0.502,5.68,1.492,8.348l1.875-0.696 \
        C3.46,30.208,3,27.633,3,25C3,12.869,12.869,3,25,3c6.598,0,12.795,2.962,16.958,8H35V13z',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);
};

Blocklab.Icons['save'] = function(group) {
  Blockly.utils.dom.createSvgElement('path',
    {
      'class': 'blocklyIconSymbol',
      'd': 'M36,1H6H1v48h48V11.586L38.414,1H36z M34,3v14H8V3H34z M47,47H3V3h3v16h30V3h1.586L47,12.414V47z',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('path',
    {
      'class': 'blocklyIconSymbol',
      'd': 'M11,44h28V27H11V44z M13,29h24v13H13V29z',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '29',
      'y': '6',
      'width': '2',
      'height': '8',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '42',
      'y': '42',
      'width': '2',
      'height': '2',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '6',
      'y': '42',
      'width': '2',
      'height': '2',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);
};

Blocklab.Icons['skip'] = function(group) {
  Blockly.utils.dom.createSvgElement('polygon',
    {
      'class': 'blocklyIconSymbol',
      'points': '17.939,47.293 19.354,48.707 43.061,25 19.354,1.293 17.939,2.707 40.232,25 	',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('polygon',
    {
      'class': 'blocklyIconSymbol',
      'points': '8.354,48.707 32.061,25 8.354,1.293 6.939,2.707 29.232,25 6.939,47.293 	',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);
};

Blocklab.Icons['stop'] = function(group) {
  Blockly.utils.dom.createSvgElement('path',
    {
      'class': 'blocklyIconSymbol',
      'd': 'M1,49h48V1H1V49z M3,3h44v44H3V3z',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);
};

Blocklab.Icons['transfer'] = function(group) {
  Blockly.utils.dom.createSvgElement('polygon',
    {
      'class': 'blocklyIconSymbol',
      'points': '39.707,7.293 38.293,8.707 44.586,15 19,15 19,17 44.586,17 38.293,23.293 39.707,24.707 48.414,16',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '1',
      'y': '15',
      'width': '3',
      'height': '2',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '7',
      'y': '15',
      'width': '3',
      'height': '2',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '13',
      'y': '15',
      'width': '3',
      'height': '2',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('polygon',
    {
      'class': 'blocklyIconSymbol',
      'points': '11.707,41.293 5.414,35 31,35 31,33 5.414,33 11.707,26.707 10.293,25.293 1.586,34 10.293,42.707 ',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '46',
      'y': '33',
      'width': '3',
      'height': '2',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '40',
      'y': '33',
      'width': '3',
      'height': '2',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('rect',
    {
      'class': 'blocklyIconSymbol',
      'x': '34',
      'y': '33',
      'width': '3',
      'height': '2',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);
};

Blocklab.Icons['upload'] = function(group) {
  Blockly.utils.dom.createSvgElement('polygon',
    {
      'class': 'blocklyIconSymbol',
      'points': '26,49 26,27.414 30.293,31.707 31.707,30.293 25,23.586 18.293,30.293 19.707,31.707 24,27.414 24,49',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);

  Blockly.utils.dom.createSvgElement('path',
    {
      'class': 'blocklyIconSymbol',
      'd': 'M40.996,17.2C40.999,17.133,41,17.066,41,17c0-3.09-2.349-5.643-5.354-5.965C34.286,5.172,29.095,1,23,1 \
        c-7.168,0-13,5.832-13,13c0,0.384,0.02,0.775,0.06,1.18C4.902,16.106,1,20.669,1,26c0,6.065,4.935,11,11,11h8v-2h-8 \
        c-4.963,0-9-4.038-9-9c0-4.651,3.631-8.588,8.267-8.962l1.091-0.088l-0.186-1.078C12.057,15.198,12,14.586,12,14 \
        c0-6.065,4.935-11,11-11c5.393,0,9.95,3.862,10.836,9.182l0.145,0.871l0.882-0.036C34.925,13.015,34.986,13.008,35,13 \
        c2.206,0,4,1.794,4,4c0,0.272-0.03,0.553-0.091,0.835l-0.235,1.096l1.115,0.109C43.9,19.442,47,22.864,47,27c0,4.411-3.589,8-8,8 \
        h-9v2h9c5.514,0,10-4.486,10-10C49,22.175,45.624,18.128,40.996,17.2z',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);
};

Blocklab.Icons['wait'] = function(group) {
  Blockly.utils.dom.createSvgElement('path',
    {
      'class': 'blocklyIconSymbol',
      'd': 'M11,40.485V47H7v2h36v-2h-4v-6.515c0-1.871-0.729-3.628-2.051-4.95L26.414,25l10.535-10.535 \
        C38.271,13.143,39,11.385,39,9.515V3h4V1H7v2h4v6.515c0,1.871,0.729,3.628,2.051,4.95L23.586,25L13.051,35.535 \
        C11.729,36.857,11,38.615,11,40.485z M14.465,13.05C13.521,12.106,13,10.851,13,9.515V3h24v6.515c0,1.336-0.521,2.592-1.465,3.536 \
        L25,23.586L14.465,13.05z M25,26.414L35.535,36.95C36.479,37.894,37,39.149,37,40.485V47H13v-6.515 \
        c0-1.336,0.521-2.592,1.465-3.536L25,26.414z',
      'transform': 'matrix(0.5,0,0,0.5,0.5,0)'
    },
    group);
};

