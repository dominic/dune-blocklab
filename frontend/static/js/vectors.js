blocklabBlockCounter['composite'] = 0;

Blockly.Blocks['composite'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Composite Element");

    this.appendStatementInput("elements");
 
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setOutput(false);
    this.setInputsInline(false);
    this.setColour(225);
    this.setTooltip("A composite element");
    this.setStatusIcon('puzzle');

    this.childrenInheritStatus = true;
  }
};
  

Blockly.Python['composite'] = function(block) {
  compileData['data'][readableName(block)] = {};
  compileData['data'][readableName(block)]['type'] = 'composite';

  var vectors = [];
  var currentBlock = this.getInputTargetBlock('elements');
  while (currentBlock) {
    vectors.push(readableName(currentBlock));
    currentBlock = currentBlock.getNextBlock();
  }
  compileData['data'][readableName(block)]['vectors'] = vectors;

  Blockly.Python.statementToCode(block, "elements");

  return "";
};

blocklabBlockCounter['power'] = 0;

Blockly.Blocks['power'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Power Element");

    this.appendStatementInput("element")
        .appendField("Element:")

    this.appendDummyInput()
        .appendField("Number:")
        .appendField(new Blockly.FieldNumber(2, 2, null, 1), "number");
    
    this.appendDummyInput()
        .appendField("Treat as Vector Field")
        .appendField(new Blockly.FieldCheckbox(false), "vectorfield");
 
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setOutput(false);
    this.setInputsInline(false);
    this.setColour(225);
    this.setTooltip("A power element");
    this.setStatusIcon('puzzle');

    this.childrenInheritStatus = true;
  }
};


Blockly.Python['power'] = function(block) {
  compileData['data'][readableName(block)] = {};
  compileData['data'][readableName(block)]['type'] = 'power';
  compileData['data'][readableName(block)]['vector'] = readableName(this.getInputTargetBlock('element'));
  compileData['data'][readableName(block)]['number'] = block.getFieldValue("number");
  compileData['data'][readableName(block)]['vectorfield'] = block.getFieldValue("vectorfield");

  Blockly.Python.statementToCode(block, "element");

  return "";
};
