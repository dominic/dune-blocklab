'use strict';

goog.provide('Blockly.StatusIcon');

goog.require('Blockly.Bubble');
goog.require('Blockly.Events');
goog.require('Blockly.Events.Ui');
goog.require('Blockly.Icon');
goog.require('Blockly.utils.dom');
goog.require('Blockly.utils.object');

/**
 * Class for a status icon.
 * @param {!Blockly.Block} block The block associated with this status icon.
 * @extends {Blockly.Icon}
 * @constructor
 */
Blockly.StatusIcon = function(block, status) {
  Blockly.StatusIcon.superClass_.constructor.call(this, block);
  this.status_ = status;
};
Blockly.utils.object.inherits(Blockly.StatusIcon, Blockly.Icon);

Blockly.Icon.prototype.SIZE = 25;

/**
 * Does this icon get hidden when the block is collapsed.
 */
Blockly.StatusIcon.prototype.collapseHidden = false;

/**
 * Set this block's warning text.
 * @param {?string} _text The text, or null to delete.
 * @param {string=} _opt_id An optional ID for the warning text to be able to
 *     maintain multiple warnings.
 */
Blockly.Block.prototype.setStatusIcon = function(_status) {
  // NOP.
};
  
/**
 * Set this block's warning text.
 * @param {?string} text The text, or null to delete.
 * @param {string=} opt_id An optional ID for the warning text to be able to
 *     maintain multiple warnings.
 */
Blockly.BlockSvg.prototype.setStatusIcon = function(status) {
  var changed = false;
  if(status != this.status_) {
    if(!this.statusIcon_) {
      this.statusIcon_ = new Blockly.StatusIcon(this, status);
    }
    this.statusIcon_.drawIcon_ = Blocklab.Icons[status];
    this.statusIcon_.dispose();
    this.statusIcon_.createIcon();
    this.statusIcon_.status_ = status;
    changed = true;  
  }

  if(changed && this.rendered) {
    this.render();
  }
};

Blockly.BlockSvg.prototype.getIcons = function() {
  // How do I call the function that I override?
  // Is this really the way to go:
  // https://stackoverflow.com/a/10427741/2819459
  var icons = [];
  if (this.mutator) {
    icons.push(this.mutator);
  }
  if (this.commentIcon_) {
    icons.push(this.commentIcon_);
  }
  if (this.warning) {
    icons.push(this.warning);
  }
  if (this.statusIcon_) {
    icons.push(this.statusIcon_)
  }
  return icons;
};

Blockly.BlockSvg.prototype.getStatusIcon = function() {
    return this.statusIcon_;
};

/**
 * Dispose of this icon.
 */
Blockly.StatusIcon.prototype.dispose = function() {
  Blockly.utils.dom.removeNode(this.iconGroup_);
  this.iconGroup_ = null;
};
