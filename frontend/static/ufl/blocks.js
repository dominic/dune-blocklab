Blockly.Blocks['ufl_form'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("UFL Form");
    
    this.appendStatementInput("integrals");
    this.setDeletable(false);
  }  
};

Blockly.Python['ufl_form'] = function(block) {
  var integrals = [];
  var currentBlock = this.getInputTargetBlock('integrals');
  while (currentBlock) {
    integrals.push(Blockly.Python.blockToCode(currentBlock, true));
    currentBlock = currentBlock.getNextBlock();
  }
  return integrals.join(" + ");
}

Blockly.Blocks['ufl_integral'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Integral");

    this.appendDummyInput()
        .appendField("Type:")
        .appendField(new Blockly.FieldDropdown([
            ["Volume", "cell"],
            ["Interior Facet", "interior_facet"],
            ["Boundary Facet", "exterior_facet"]
        ]), "measure");
    
    this.appendValueInput("integrand")
        .appendField("Integrand:");
        
    this.setPreviousStatement(true, "ufl_integral");
    this.setNextStatement(true, "ufl_integral");
  }
};

Blockly.Python['ufl_integral'] = function(block) {
  var type_to_measure = {
    "cell": "dx",
    "interior_facet": "dS",
    "exterior_facet": "ds"
  };

  var measure = type_to_measure[block.getFieldValue("measure")];
  var integrand = Blockly.Python.valueToCode(block, "integrand", Blockly.Python.ORDER_NONE);

  var code = "(" + integrand + ") * " + measure
  return code;
}

function predefined_symbol_codegen(symbol) {
  return function(block) {
    return [symbol, Blockly.Python.ORDER_NONE]
  };
}

var terminals = [
  ["ufl_testfunc", "TestFunction", "The test function from the finite element space", predefined_symbol_codegen("v")],
  ["ufl_trialfunc", "TrialFunction", "The trial function from the finite element space", predefined_symbol_codegen("u")]
];

terminals.forEach(t => {
  Blockly.Blocks[t[0]] = {
    init: function() {
      this.appendDummyInput()
          .appendField(t[1]);
      this.setOutput(true);
      this.setTooltip(t[2]);
    }
  };

  Blockly.Python[t[0]] = t[3];
});



var unary_operators = [
  ["ufl_uminus", "-", "Unary Minus Operator"]
]

unary_operators.forEach(op => {
  Blockly.Blocks[op[0]] = {
    init: function() {
      this.appendValueInput("op")
          .appendField(op[1]);
      this.setOutput(true);
      this.setTooltip(op[2]);
    }
  };

  Blockly.Python[op[0]] = function(block) {
    return [op[1] + " " + Blockly.Python.valueToCode(block, "op", Blockly.Python.ORDER_NONE), Blockly.Python.ORDER_NONE];
  }
})

var binary_operators = [
  // Arithmetic binary operators
  ["ufl_plus", "+", "The addition operator"],
  ["ufl_minus", "-", "The subtraction operator"],
  ["ufl_multiply", "*", "The multiplication operator"],
  ["ufl_divide", "/", "The division operator"]
];

binary_operators.forEach(op => {
  Blockly.Blocks[op[0]] = {
    init: function() {
      this.appendValueInput("op1")
          .appendField(op[1]);
      this.appendValueInput("op2");
      this.setOutput(true);
      this.setTooltip(op[2]);
    }
  };

  Blockly.Python[op[0]] = function(block) {
    var op1 = Blockly.Python.valueToCode(block, "op1", Blockly.Python.ORDER_NONE);
    var op2 = Blockly.Python.valueToCode(block, "op2", Blockly.Python.ORDER_NONE);
    return [op1 + " " + op[1] + " " + op2, Blockly.Python.ORDER_NONE];
  }
});

var unary_functions = [
  // Elementary math functions
  ["ufl_exp", "exp", "The exponential function"],
  ["ufl_ln", "ln", "The natural logarithm function"],
  ["ufl_sqrt", "sqrt", "The square root function"],
  ["ufl_max", "Max", "The maximum function"],
  ["ufl_min", "Min", "The minimum function"],
  ["ufl_sin", "sin", "The sine function"],
  ["ufl_cos", "cos", "The cosine function"],
  ["ufl_tan", "tan", "The tangens function"],

  // Differential Operators
  ["ufl_grad", "grad", "The gradient operator"],
  ["ufl_refgrad", "reference_grad", "The gradient operator (on the reference element)"],

  // Matrix Operators
  ["ufl_det", "det", "Determinant of the given matrix"],
  ["ufl_transpose", "transpose", "Transposition of the given matrix"],
  ["ufl_diag", "diag", "Extraction of the diagonal part of the given matrix (as a matrix)"],
  ["ufl_diag_vector", "diag_vector", "Extraction of the diagonal part of the given matrix (as a vector)"],
  ["ufl_trace", "tr", "Trace of a given matrix"],
  ["ufl_inverse", "inv", "Inverse of the given matrix"],
  ["ufl_cofactor", "cofac", "Cofactor matrix of the given matrix"],
  ["ufl_sym", "sym", "Symmetric part of the given matrix"],
  ["ufl_skew", "skew", "Skew-symmetric part of the given matrix"],
  ["ufl_dev", "dev", "Deviatoric part of the given matrix"],

  // Logical function
  ["ufl_not", "Not", "Logical Negation"]
];

unary_functions.forEach(f => {
  Blockly.Blocks[f[0]] = {
    init: function() {
      this.appendValueInput("op")
          .appendField(f[1].toLowerCase());
      this.setOutput(true);
      this.setTooltip(f[2]);
    }
  };
      
  Blockly.Python[f[0]] = function(block) {
    var op = Blockly.Python.valueToCode(block, "op", Blockly.Python.ORDER_NONE);
    return [f[1] + "(" + op + ")", Blockly.Python.ORDER_NONE];
  }
});

var binary_functions = [
  // Tensor Algebra Functions
  ["ufl_cross", "cross", "The cross-product of two vectors"],
  ["ufl_dot", "dot", "The dot product of two tensors"],
  ["ufl_inner", "inner", "The inner product of two tensors"],
  ["ufl_outer", "outer", "The outer product of two tensors"],

  // Logical Functions
  ["ufl_eq", "eq", "Equality comparison"],
  ["ufl_ne", "ne", "Inequality comparison"],
  ["ufl_le", "le", "Lesser or equal comparison"],
  ["ufl_ge", "ge", "Greater or equal comparison"],
  ["ufl_lt", "lt", "Lower than comparison"],
  ["ufl_gt", "gt", "Greater than comparison"],
  ["ufl_and", "And", "Logical conjunction"],
  ["ufl_or", "Or", "Logical Disjunction"],
];

binary_functions.forEach(f => {
  Blockly.Blocks[f[0]] = {
    init: function() {
      this.appendValueInput("op1")
          .appendField(f[1].toLowerCase());
      this.appendValueInput("op2");
      this.setOutput(true);
      this.setTooltip(f[2]);
    }
  };
    
  Blockly.Python[f[0]] = function(block) {
    var op1 = Blockly.Python.valueToCode(block, "op1", Blockly.Python.ORDER_NONE);
    var op2 = Blockly.Python.valueToCode(block, "op2", Blockly.Python.ORDER_NONE);
    return [f[1] + "(" + op1 + ", " + op2 + ")", Blockly.Python.ORDER_NONE];
  }
});

Blockly.Blocks["ufl_cond"] = {
    init: function() {
      this.appendDummyInput()
          .appendField("Conditional");

      this.appendValueInput("cond")
          .appendField("If:");

      this.appendValueInput("op1")
          .appendField("Then:");

      this.appendValueInput("op2")
          .appendField("Else:");

      this.setOutput(true);
      this.setTooltip("A Conditional");
    }
  };
    
  Blockly.Python["ufl_cond"] = function(block) {
    var cond = Blockly.Python.valueToCode(block, "cond", Blockly.Python.ORDER_NONE);
    var op1 = Blockly.Python.valueToCode(block, "op1", Blockly.Python.ORDER_NONE);
    var op2 = Blockly.Python.valueToCode(block, "op2", Blockly.Python.ORDER_NONE);
    return ["conditional(" + cond + ", " + op1 + ", " + op2 + ")", Blockly.Python.ORDER_NONE];
  }
