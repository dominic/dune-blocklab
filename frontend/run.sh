#!/bin/bash

gunicorn frontend:app --limit-request-line 0 --worker-class gevent --bind 0.0.0.0:5000
